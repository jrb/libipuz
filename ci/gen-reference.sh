#!/bin/sh

set -eu

mkdir -p public/devel-docs

echo "Copying in the reference"

mv _build/docs/reference/libipuz-1.0 public/devel-docs/
