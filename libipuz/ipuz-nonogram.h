/* ipuz-nonogram.h
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <libipuz/ipuz-grid.h>
#include <libipuz/ipuz-cell-coord-array.h>

G_BEGIN_DECLS


#define IPUZ_TYPE_NONOGRAM (ipuz_nonogram_get_type ())
G_DECLARE_DERIVABLE_TYPE (IpuzNonogram, ipuz_nonogram, IPUZ, NONOGRAM, IpuzGrid);


typedef struct _IpuzNonogramClass
{
  IpuzGridClass parent_class;
} IpuzNonogramClass;


IpuzPuzzle         *ipuz_nonogram_new                   (void);
const gchar        *ipuz_nonogram_get_space             (IpuzNonogram      *self);
void                ipuz_nonogram_set_space             (IpuzNonogram      *self,
                                                         const gchar       *space);
GArray             *ipuz_nonogram_get_clues             (IpuzNonogram      *self,
                                                         IpuzClueDirection  direction,
                                                         guint              index);
void                ipuz_nonogram_fix_clues             (IpuzNonogram      *self);
guint               ipuz_nonogram_get_n_groups          (IpuzNonogram      *self);
const gchar        *ipuz_nonogram_get_group             (IpuzNonogram      *self,
                                                         guint              index);
IpuzCellCoordArray *ipuz_nonogram_get_cells_by_group    (IpuzNonogram      *self,
                                                         const gchar       *group);
void                ipuz_nonogram_set_count_crossed_off (IpuzNonogram      *self,
                                                         IpuzClueDirection  direction,
                                                         guint              index,
                                                         guint              count_index,
                                                         gboolean           crossed_off);
gboolean            ipuz_nonogram_get_count_crossed_off (IpuzNonogram      *self,
                                                         IpuzClueDirection  direction,
                                                         guint              index,
                                                         guint              count_index);
void                ipuz_nonogram_print                 (IpuzNonogram      *self);


G_END_DECLS
