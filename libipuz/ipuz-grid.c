/* ipuz-grid.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#include "ipuz-crossword.h"
#include "ipuz-grid.h"
#include "ipuz-private.h"

/**
 * IpuzGrid:
 *
 * [class@Ipuz.Grid] is the base class for all grid-like puzzles. It
 * is used to access the [struct@Ipuz.Cell] of a given puzzle kind,
 * and is indexed by [struct@Ipuz.CellCoord].
 *
 * ## Guesses
 *
 * One can associate an [struct@Ipuz.Guesses] with a grid through
 * [method@Ipuz.Grid.set_guesses]. The guesses struct is mainly
 * intended to represent a user play-through of a game, though can be
 * used to store other information per-cell as well.
 *
 * Here's an example of how you would create and set the guesses on a
 * grid.
 *
 * ```C
 * g_autoptr (IpuzGuesses) guesses;
 *
 * guesses = ipuz_grid_create_guesses (grid);
 * ipuz_grid_set_guesses (grid, guesses);
 * ```
 *
 * If you're using guesses with a grid that's being edited, you should
 * use [method@Ipuz.Grid.fix_guesses] to keep them in sync. As an
 * example:
 *
 * ```C
 * g_autoptr (IpuzGuesses) guesses;
 *
 * guesses = ipuz_grid_create_guesses (grid);
 * ipuz_grid_set_guesses (grid, guesses);
 *
 * ...
 *
 * ipuz_grid_resize (grid, new_width, new_height);
 * ipuz_grid_fix_guesses (grid);
 * // guesses is updated to match grid
 *
 * g_assert_cmpint (ipuz_guesses_get_width (guesses), ==, new_width);
 * ```
 */

/**
 * IpuzGridClass:
 * @parent_class: The puzzle class structure representing the parent.
 * @check_cell: Vfunc that invokes the operation determined by
 *   @check_type on @cell and @guesses at @coord.
 * @check_stride: Vfunc that invokes the operation determined by
 *   @check_type on the stride guess determined by @direction and
 *   @index.
 *
 * The class structure for [class@Ipuz.Grid]s.
 */

enum
{
  PROP_0,
  PROP_WIDTH,
  PROP_HEIGHT,
  PROP_GUESSES,
  N_PROPS
};
static GParamSpec *obj_props[N_PROPS] = { NULL, };


typedef struct
{
  IpuzGridCheckType type;
  gboolean retval;
  IpuzGuesses *guesses;
} IpuzGridCheckTuple;

typedef struct
{
  GArray *cells;
  guint width;
  guint height;
  gboolean has_solution;

  IpuzGuesses *guesses;
} IpuzGridPrivate;


static void            ipuz_grid_init               (IpuzGrid            *self);
static void            ipuz_grid_class_init         (IpuzGridClass       *klass);
static void            ipuz_grid_set_property       (GObject             *object,
                                                     guint                prop_id,
                                                     const GValue        *value,
                                                     GParamSpec          *pspec);
static void            ipuz_grid_get_property       (GObject             *object,
                                                     guint                prop_id,
                                                     GValue              *value,
                                                     GParamSpec          *pspec);
static void            ipuz_grid_dispose            (GObject             *object);
static void            ipuz_grid_finalize           (GObject             *object);
static void            ipuz_grid_load_node          (IpuzPuzzle          *puzzle,
                                                     const char          *member_name,
                                                     JsonNode            *node);
static void            ipuz_grid_post_load_node     (IpuzPuzzle          *puzzle,
                                                     const char          *member_name,
                                                     JsonNode            *node);
static void            ipuz_grid_fixup              (IpuzPuzzle          *puzzle);
static void            ipuz_grid_set_style          (IpuzPuzzle          *puzzle,
                                                     const gchar         *style_name,
                                                     IpuzStyle           *style);
static gboolean        ipuz_grid_equal              (IpuzPuzzle          *puzzle1,
                                                     IpuzPuzzle          *puzzle2);
static void            ipuz_grid_build              (IpuzPuzzle          *puzzle,
                                                     JsonBuilder         *builder);
static IpuzPuzzleFlags ipuz_grid_get_flags          (IpuzPuzzle          *self);
static void            ipuz_grid_clone              (IpuzPuzzle          *src,
                                                     IpuzPuzzle          *dest);
static gboolean        ipuz_grid_game_won           (IpuzPuzzle          *self);
static gboolean        ipuz_grid_real_check_cell    (IpuzGrid            *grid,
                                                     IpuzCell            *cell,
                                                     const IpuzCellCoord *coord,
                                                     IpuzGuesses         *guesses,
                                                     IpuzGridCheckType    check_type);
static gboolean        ipuz_grid_real_check_stride  (IpuzGrid            *grid,
                                                     IpuzClueDirection    direction,
                                                     guint                index,
                                                     IpuzGuesses         *guesses,
                                                     IpuzGridCheckType    check_type);
static void            ipuz_grid_check_cell_foreach (IpuzGrid            *grid,
                                                     IpuzCell            *cell,
                                                     const IpuzCellCoord *coord,
                                                     IpuzGuesses         *guesses,
                                                     gpointer             user_data);


G_DEFINE_TYPE_WITH_CODE (IpuzGrid, ipuz_grid, IPUZ_TYPE_PUZZLE,
                         G_ADD_PRIVATE (IpuzGrid));


/*
 * Class Methods
 */

static void
cell_array_clear_func (GArray **cell_row)
{
  g_array_free (*cell_row, TRUE);
  *cell_row = NULL;
}

static void
cell_clear_func (IpuzCell **cell)
{
  ipuz_cell_unref (*cell);
  *cell = NULL;
}

static void
ipuz_grid_init (IpuzGrid *self)
{
  IpuzGridPrivate *priv;

  priv = ipuz_grid_get_instance_private (self);

  priv->cells = g_array_new (FALSE, TRUE, sizeof (GArray*));
  g_array_set_clear_func (priv->cells, (GDestroyNotify) cell_array_clear_func);
  priv->width = 0;
  priv->height = 0;
}

static void
ipuz_grid_class_init (IpuzGridClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  IpuzPuzzleClass *puzzle_class = IPUZ_PUZZLE_CLASS (klass);
  IpuzGridClass *grid_class = IPUZ_GRID_CLASS (klass);

  object_class->set_property = ipuz_grid_set_property;
  object_class->get_property = ipuz_grid_get_property;
  object_class->dispose = ipuz_grid_dispose;
  object_class->finalize = ipuz_grid_finalize;
  puzzle_class->load_node = ipuz_grid_load_node;
  puzzle_class->post_load_node = ipuz_grid_post_load_node;
  puzzle_class->fixup = ipuz_grid_fixup;
  puzzle_class->set_style = ipuz_grid_set_style;
  puzzle_class->equal = ipuz_grid_equal;
  puzzle_class->build = ipuz_grid_build;
  puzzle_class->get_flags = ipuz_grid_get_flags;
  puzzle_class->clone = ipuz_grid_clone;
  puzzle_class->game_won = ipuz_grid_game_won;
  grid_class->check_cell = ipuz_grid_real_check_cell;
  grid_class->check_stride = ipuz_grid_real_check_stride;

  /**
   * IpuzGrid:width: (attributes org.gtk.Property.get=ipuz_grid_get_width org.gtk.Property.set=ipuz_grid_resize)
   *
   * Number of columns in the grid.
   **/
  obj_props[PROP_WIDTH] = g_param_spec_uint ("width",
                                             NULL, NULL,
                                             0, 65536, 0,
                                             G_PARAM_READWRITE);

  /**
   * IpuzGrid:height: (attributes org.gtk.Property.get=ipuz_grid_get_height org.gtk.Property.set=ipuz_grid_resize)
   *
   * Number of rows in the grid.
   **/
  obj_props[PROP_HEIGHT] = g_param_spec_uint ("height",
                                              NULL, NULL,
                                              0, 65536, 0,
                                              G_PARAM_READWRITE);

  /**
   * IpuzGrid:guesses: (attributes org.gtk.Property.get=ipuz_grid_get_guesses org.gtk.Property.set=ipuz_grid_set_guesses)
   *
   * The [struct@Ipuz.Guesses] associated with the grid.
   **/
  obj_props[PROP_GUESSES] = g_param_spec_boxed ("guesses",
                                                NULL, NULL,
                                                IPUZ_TYPE_GUESSES,
                                                G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);
}

static void
ipuz_grid_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  IpuzGridPrivate *priv;

  priv = ipuz_grid_get_instance_private (IPUZ_GRID (object));

  switch (prop_id)
    {
    case PROP_WIDTH:
      ipuz_grid_resize (IPUZ_GRID (object),
                        g_value_get_uint (value),
                        priv->height);
      break;
    case PROP_HEIGHT:
      ipuz_grid_resize (IPUZ_GRID (object),
                        priv->width,
                        g_value_get_uint (value));
      break;
    case PROP_GUESSES:
      ipuz_grid_set_guesses (IPUZ_GRID (object),
                             g_value_get_object (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
ipuz_grid_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  IpuzGridPrivate *priv;

  priv = ipuz_grid_get_instance_private (IPUZ_GRID (object));

  switch (prop_id)
    {
    case PROP_WIDTH:
      g_value_set_uint (value, priv->width);
      break;
    case PROP_HEIGHT:
      g_value_set_uint (value, priv->height);
      break;
    case PROP_GUESSES:
      g_value_set_object (value, priv->guesses);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

/* free all memory */
static void
ipuz_grid_dispose (GObject *object)
{
  guint width, height;

  width = ipuz_grid_get_width (IPUZ_GRID (object));
  height = ipuz_grid_get_height (IPUZ_GRID (object));

  /* We need to drop all references to clues in the cells before we
   * free them. This is because of the loop where clues have refs to
   * cells, and cells have refs back to clues. Since cells are bound
   * to the grid, we can proxy this dispose to break the cycle in one
   * direction. */
  for (guint row = 0; row < height; row++)
    {
      for (guint column = 0; column < width; column++)
        {
          IpuzCellCoord coord = { .row = row, .column = column };

          IpuzCell *cell = ipuz_grid_get_cell (IPUZ_GRID (object), &coord);
          ipuz_cell_clear_clues (cell);
        }
    }

  G_OBJECT_CLASS (ipuz_grid_parent_class)->dispose (object);
}

static void
ipuz_grid_finalize (GObject *object)
{
  IpuzGridPrivate *priv;

  g_return_if_fail (object != NULL);

  priv = ipuz_grid_get_instance_private (IPUZ_GRID (object));

  g_clear_pointer (&priv->cells, g_array_unref);
  g_clear_pointer (&priv->guesses, ipuz_guesses_unref);

  G_OBJECT_CLASS (ipuz_grid_parent_class)->finalize (object);
}


static void
load_dimensions (IpuzGrid *self,
                 JsonNode *node)
{
  gint width = -1;
  gint height = -1;

  if (!JSON_NODE_HOLDS_OBJECT (node))
    return;

  g_autoptr(JsonReader) reader = json_reader_new (node);
  if (json_reader_read_member (reader, "width"))
    width = json_reader_get_int_value (reader);
  json_reader_end_member (reader);

  if (json_reader_read_member (reader, "height"))
    height = json_reader_get_int_value (reader);
  json_reader_end_member (reader);

  if (width > 0 && height > 0)
    ipuz_grid_resize (self, width, height);
}

static void
ipuz_grid_load_node (IpuzPuzzle *puzzle,
                         const char *member_name,
                         JsonNode   *node)
{
  g_assert (member_name);
  g_assert (node);

  if (strcmp (member_name, "dimensions") == 0)
    {
      load_dimensions (IPUZ_GRID (puzzle), node);
      return;
    }

  IPUZ_PUZZLE_CLASS (ipuz_grid_parent_class)->load_node (puzzle, member_name, node);
}


static void
ipuz_grid_parse_puzzle_row (GArray         *row,
                            JsonArray      *array,
                            IpuzPuzzleKind  kind,
                            const gchar    *block,
                            const gchar    *empty)
{
  guint n_rows, array_len;

  g_return_if_fail (row != NULL);
  g_return_if_fail (array != NULL);
  g_return_if_fail (block != NULL);

  array_len = json_array_get_length (array);
  n_rows = row->len;

  for (guint i = 0; i < MIN (n_rows, array_len); i++)
    {
      JsonNode *node;
      IpuzCell *cell;


      node = json_array_get_element (array, i);
      cell = g_array_index (row, IpuzCell *, i);

      ipuz_cell_parse_puzzle (cell, node, kind, block, empty);
    }
}

static void
ipuz_grid_parse_puzzle (IpuzGrid    *self,
                        JsonNode    *node,
                        const gchar *block,
                        const gchar *empty)
{
  IpuzGridPrivate *priv;
  JsonArray *array;
  guint array_len;
  IpuzPuzzleKind kind;

  g_return_if_fail (IPUZ_IS_GRID (self));
  g_return_if_fail (node != NULL);
  g_return_if_fail (block != NULL);
  g_return_if_fail (empty != NULL);

  priv = ipuz_grid_get_instance_private (self);
  kind = ipuz_puzzle_get_puzzle_kind (IPUZ_PUZZLE (self));

  /* bail out on anything other than an JsonArray */
  if (! JSON_NODE_HOLDS_ARRAY (node))
    return;

  /* bail out on anything other than an JsonArray */
  if (! JSON_NODE_HOLDS_ARRAY (node))
    return;

  array = json_node_get_array (node);
  array_len = json_array_get_length (array);

  for (guint i = 0; i < MIN (priv->height, array_len); i++)
    {
      JsonNode *row_node;
      row_node = json_array_get_element (array, i);
      if (JSON_NODE_HOLDS_ARRAY (row_node))
        ipuz_grid_parse_puzzle_row (g_array_index (priv->cells, GArray *, i),
                                    json_node_get_array (row_node),
                                    kind,
                                    block,
                                    empty);
    }
}

/*
 * We look for valid strings that aren't in the block element, and are in the
 * charset if it exists.
 */
static void
ipuz_grid_parse_solution_row (GArray         *row,
                              guint           columns,
                              JsonArray      *array,
                              IpuzPuzzleKind  kind,
                              const gchar    *block,
                              const gchar    *charset)
{
  guint array_len;

  g_return_if_fail (row != NULL);
  g_return_if_fail (array != NULL);
  g_return_if_fail (block != NULL);

  array_len = json_array_get_length (array);

  for (guint i = 0; i < MIN (columns, array_len); i++)
    {
      JsonNode *node;
      IpuzCell *cell;

      cell =  g_array_index (row, IpuzCell *, i);
      node = json_array_get_element (array, i);
      ipuz_cell_parse_solution (cell, node, kind, block, charset);
    }
}

static void
ipuz_grid_parse_solution (IpuzGrid    *self,
                          JsonNode    *node,
                          const gchar *block,
                          const gchar *charset)
{
  IpuzGridPrivate *priv;
  JsonArray *array;
  guint array_len;
  IpuzPuzzleKind kind;

  g_return_if_fail (IPUZ_IS_GRID (self));
  g_return_if_fail (node != NULL);
  g_return_if_fail (block != NULL);

  priv = ipuz_grid_get_instance_private (self);
  kind = ipuz_puzzle_get_puzzle_kind (IPUZ_PUZZLE (self));

  /* bail out on anything other than an JsonArray */
  if (! JSON_NODE_HOLDS_ARRAY (node))
    return;

  array = json_node_get_array (node);
  array_len = json_array_get_length (array);

  for (guint i = 0; i < MIN (priv->height, array_len); i++)
    {
      JsonNode *row_node;
      row_node = json_array_get_element (array, i);
      if (JSON_NODE_HOLDS_ARRAY (row_node))
        ipuz_grid_parse_solution_row (g_array_index (priv->cells, GArray *, i),
                                      priv->width,
                                      json_node_get_array (row_node),
                                      kind,
                                      block,
                                      charset);
    }
}

static void
ipuz_grid_post_load_node (IpuzPuzzle *puzzle,
                          const char *member_name,
                          JsonNode   *node)
{
  IpuzGridPrivate *priv;

  g_return_if_fail (node != NULL);

  priv = ipuz_grid_get_instance_private (IPUZ_GRID (puzzle));

  if (g_strcmp0 (member_name, "puzzle") == 0)
    {
      ipuz_grid_parse_puzzle (IPUZ_GRID (puzzle), node,
                              ipuz_puzzle_get_block (puzzle),
                              ipuz_puzzle_get_empty (puzzle));
      return;
    }
  /* FIXME(refactor): Move to crosswords */
  else if (g_strcmp0 (member_name, "solution") == 0
           && IPUZ_IS_CROSSWORD (puzzle))
    {
      ipuz_grid_parse_solution (IPUZ_GRID (puzzle), node,
                                ipuz_puzzle_get_block (puzzle),
                                ipuz_puzzle_get_charset_str (puzzle));
      priv->has_solution = TRUE;
      return;
    }
  if (strcmp (member_name, "saved") == 0)
    {
      /* FIXME(saved): Load the saved guesses from disk */
    }
}

static void
ipuz_grid_fixup (IpuzPuzzle *puzzle)
{
  g_autoptr(GHashTable) styles = NULL;

  g_object_get (G_OBJECT (puzzle),
                "styles", &styles,
                NULL);

  /* short-circuit this fixup */
  /* FIXME: should have a has_style flag */
  if (styles == NULL)
    return;

/* Makes sure cells have their style associated with them */
  for (guint row = 0; row < ipuz_grid_get_height (IPUZ_GRID (puzzle)); row++)
    {
      for (guint column = 0; column < ipuz_grid_get_width (IPUZ_GRID (puzzle)); column++)
        {
          IpuzCell *cell;
          IpuzCellCoord coord = { .row = row, .column = column };

          cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
          if (ipuz_cell_get_style_name (cell))
            {
              IpuzStyle *style;

              style = ipuz_puzzle_get_style (puzzle, ipuz_cell_get_style_name (cell));
              ipuz_cell_set_style (cell, style, ipuz_cell_get_style_name (cell));
            }
        }
    }
}

typedef struct
{
  gpointer old_style;
  gpointer new_style;
} StyleSwapTuple;

static void
foreach_fixup_style (IpuzGrid            *grid,
                     IpuzCell            *cell,
                     const IpuzCellCoord *coord,
                     IpuzGuesses         *guesses,
                     gpointer             user_data)
{
  StyleSwapTuple *tuple;

  tuple = (StyleSwapTuple *) user_data;

  if (ipuz_cell_get_style (cell) == tuple->old_style)
    ipuz_cell_set_style (cell, tuple->new_style, NULL);
}

static void
ipuz_grid_set_style (IpuzPuzzle  *puzzle,
                     const gchar *style_name,
                     IpuzStyle   *style)
{
  StyleSwapTuple tuple;

  tuple.old_style = ipuz_puzzle_get_style (puzzle, style_name);
  tuple.new_style = style;

  /* old_style may be unreffed after this call. Do not do anything
   * with it */
  IPUZ_PUZZLE_CLASS (ipuz_grid_parent_class)->set_style (puzzle, style_name, style);

  if (tuple.old_style != NULL)
    ipuz_grid_foreach_cell (IPUZ_GRID (puzzle),
                            foreach_fixup_style, &tuple);
}

static gboolean
ipuz_grid_equal (IpuzPuzzle *puzzle1,
                 IpuzPuzzle *puzzle2)
{
  IpuzGridPrivate *priv1, *priv2;
  guint r, c;

  priv1 = ipuz_grid_get_instance_private (IPUZ_GRID (puzzle1));
  priv2 = ipuz_grid_get_instance_private (IPUZ_GRID (puzzle2));

  if (puzzle1 == NULL || puzzle2 == NULL)
    return (puzzle1 == puzzle2);

  if (priv1->height != priv2->height || priv1->width != priv2->width)
    return FALSE;

  for (r = 0; r < priv1->height; r++)
    {
      for (c = 0; c < priv1->width; c++)
        {
          IpuzCellCoord coord = { .row = r, .column = c };
          IpuzCell *cell1 = ipuz_grid_get_cell (IPUZ_GRID (puzzle1), &coord);
          IpuzCell *cell2 = ipuz_grid_get_cell (IPUZ_GRID (puzzle2), &coord);

          if (!ipuz_cell_equal (cell1, cell2))
            return FALSE;
        }
    }

  return IPUZ_PUZZLE_CLASS (ipuz_grid_parent_class)->equal (puzzle1, puzzle2);
}

static void
build_dimensions (IpuzGrid    *self,
                  JsonBuilder *builder)
{
  IpuzGridPrivate *priv;

  priv = ipuz_grid_get_instance_private (self);

  json_builder_set_member_name (builder, "dimensions");
  json_builder_begin_object (builder);
  json_builder_set_member_name (builder, "width");
  json_builder_add_int_value (builder, priv->width);
  json_builder_set_member_name (builder, "height");
  json_builder_add_int_value (builder, priv->height);
  json_builder_end_object (builder);
}

static void
ipuz_grid_build_puzzle (IpuzGrid    *self,
                        JsonBuilder *builder,
                        const char  *block,
                        const char  *empty)
{
  IpuzGridPrivate *priv;
  guint r, c;

  priv = ipuz_grid_get_instance_private (self);

  if (priv->height == 0 || priv->width == 0)
    return;

  json_builder_set_member_name (builder, "puzzle");
  json_builder_begin_array (builder);
  for (r = 0; r < priv->height; r++)
    {
      json_builder_begin_array (builder);
      for (c = 0; c < priv->width; c++)
        {
          IpuzCellCoord coord = { .row = r, .column = c };
          IpuzCell *cell = ipuz_grid_get_cell (self, &coord);

          ipuz_cell_build (cell, builder, FALSE, block, empty);
        }
      json_builder_end_array (builder);
    }
  json_builder_end_array (builder);
}


void
ipuz_grid_build_solution (IpuzGrid    *self,
                          JsonBuilder *builder,
                          const char  *block)
{
  IpuzGridPrivate *priv;
  guint r, c;

  priv = ipuz_grid_get_instance_private (self);

  if (priv->height == 0 || priv->width == 0)
    return;

  json_builder_set_member_name (builder, "solution");
  json_builder_begin_array (builder);
  for (r = 0; r < priv->height; r++)
    {
      json_builder_begin_array (builder);
      for (c = 0; c < priv->width; c++)
        {
          IpuzCellCoord coord = { .row = r, .column = c };
          IpuzCell *cell = ipuz_grid_get_cell (self, &coord);

          ipuz_cell_build (cell, builder, TRUE, block, NULL);
        }
      json_builder_end_array (builder);
    }
  json_builder_end_array (builder);
}

static void
ipuz_grid_build (IpuzPuzzle  *puzzle,
                 JsonBuilder *builder)
{
  IpuzGrid *self = IPUZ_GRID (puzzle);;

  /* We chain to the parent class first to get meta-information. Not
   * every parsing section can handle this coming at the end. */
  IPUZ_PUZZLE_CLASS (ipuz_grid_parent_class)->build (puzzle, builder);

  build_dimensions (self, builder);

  ipuz_grid_build_puzzle (self, builder,
                           ipuz_puzzle_get_block (puzzle),
                           ipuz_puzzle_get_empty (puzzle));

  /* FIXME(refactor):  move to IpuzCrossword */
  if (IPUZ_IS_CROSSWORD (puzzle))
    ipuz_grid_build_solution (self, builder,
                              ipuz_puzzle_get_block (puzzle));
}

static IpuzPuzzleFlags
ipuz_grid_get_flags (IpuzPuzzle *puzzle)
{
  IpuzGridPrivate *priv;
  guint flags;

  priv = ipuz_grid_get_instance_private (IPUZ_GRID (puzzle));

  flags = IPUZ_PUZZLE_CLASS (ipuz_grid_parent_class)->get_flags (puzzle);

  if (priv->has_solution)
    flags |= IPUZ_PUZZLE_FLAG_HAS_SOLUTION;

  return flags;
}

/*
 * Copying grids is painful. They refer to other portions of the
 * puzzle that we don't really know about. There's not really a good
 * way to just make fresh copies of cells, as they refer to styles and
 * clues within the puzzle. We can copy the board and cells on the
 * surface, and count on our subclasses to fix them up afterwards.
 */
static void
ipuz_grid_clone (IpuzPuzzle *src,
                 IpuzPuzzle *dest)
{
  IpuzGridPrivate *src_priv;
  IpuzGridPrivate *dest_priv;
  GHashTable *styles = NULL;
  guint row, column;

  src_priv = ipuz_grid_get_instance_private (IPUZ_GRID (src));
  dest_priv = ipuz_grid_get_instance_private (IPUZ_GRID (dest));

  /* Copy any fields */
  dest_priv->has_solution = src_priv->has_solution;

  /* Chain up to start to get the meta information and styles*/
  IPUZ_PUZZLE_CLASS (ipuz_grid_parent_class)->clone (src, dest);
  g_object_get (G_OBJECT (dest),
                "styles", &styles,
                NULL);

  /* handle grid data */
  ipuz_grid_resize (IPUZ_GRID (dest), src_priv->width, src_priv->height);
  if (src_priv->guesses)
    dest_priv->guesses = ipuz_guesses_copy (src_priv->guesses);

  /* clone the cells */
  for (row = 0; row < src_priv->height; row++)
    {
      for (column = 0; column < src_priv->width; column++)
        {
          IpuzCellCoord coord = { .row = row, .column = column };
          IpuzCell *src_cell, *dest_cell;
          IpuzStyle *src_style;
          const gchar *src_style_name;

          src_cell = ipuz_grid_get_cell (IPUZ_GRID (src), &coord);
          dest_cell = ipuz_grid_get_cell (IPUZ_GRID (dest), &coord);

          ipuz_cell_set_number (dest_cell, ipuz_cell_get_number (src_cell));
          ipuz_cell_set_label (dest_cell, ipuz_cell_get_label (src_cell));
          ipuz_cell_set_solution (dest_cell, ipuz_cell_get_solution (src_cell));
          ipuz_cell_set_initial_val (dest_cell, ipuz_cell_get_initial_val (src_cell));
          ipuz_cell_set_saved_guess (dest_cell, ipuz_cell_get_saved_guess (src_cell));
          ipuz_cell_set_cell_type (dest_cell, ipuz_cell_get_cell_type (src_cell));

          src_style = ipuz_cell_get_style (src_cell);
          src_style_name = ipuz_cell_get_style_name (src_cell);

          /* Set the style and the name at the same time */
          if (src_style)
            {
              g_autoptr (IpuzStyle) dest_style = NULL;

              if (src_style_name && styles)
                dest_style = g_hash_table_lookup (styles, src_style_name);

              if (dest_style != NULL)
                ipuz_style_ref (dest_style);
              else
                dest_style = ipuz_style_copy (src_style);
              ipuz_cell_set_style (dest_cell, dest_style, src_style_name);
            }
          /* just set the style_name */
          else
            {
              ipuz_cell_set_style_name (dest_cell, ipuz_cell_get_style_name (src_cell));
            }
        }
    }
}

static gboolean
game_won_solution (IpuzGrid *self)
{
  IpuzGuesses *guesses;
  IpuzGridPrivate *priv;

  priv = ipuz_grid_get_instance_private (self);
  guesses = ipuz_grid_get_guesses (self);

  for (guint row = 0; row < priv->height; row++)
    {
      for (guint column = 0; column < priv->width; column++)
        {
          IpuzCell *cell;
          IpuzCellCoord coord = { .row = row, .column = column };

          cell = ipuz_grid_get_cell (self, &coord);
          if (ipuz_grid_check_cell (self,
                                    cell, &coord, guesses,
                                    IPUZ_GRID_CHECK_GUESSABLE))
            {
              if (! ipuz_grid_check_cell (self,
                                          cell, &coord, guesses,
                                          IPUZ_GRID_CHECK_GUESS_CORRECT))
                return FALSE;
            }
        }
    }

  return TRUE;
}

static gboolean
ipuz_grid_game_won (IpuzPuzzle *self)
{
  guint flags;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), FALSE);

  /* You can't win without playing! */
  if (ipuz_grid_get_guesses (IPUZ_GRID (self)) == NULL)
    return FALSE;

  flags = ipuz_puzzle_get_flags (self);

  if (flags & IPUZ_PUZZLE_FLAG_HAS_SOLUTION)
    return game_won_solution (IPUZ_GRID (self));

  if (flags & IPUZ_PUZZLE_FLAG_HAS_CHECKSUM)
    {
      /* FIXME(checksum): need to support this! */
    }
  return FALSE;
}

/* We can't say anything authoritative about a cell without knowing
 * what type it is. Just return false */
static gboolean
ipuz_grid_real_check_cell (IpuzGrid            *grid,
                           IpuzCell            *cell,
                           const IpuzCellCoord *coord,
                           IpuzGuesses         *guesses,
                           IpuzGridCheckType    check_type)
{
  g_assert (IPUZ_IS_GRID (grid));

  return FALSE;
}

/* We can't say anything authoritative about a stride without knowing
 * what type it is. Just return false */
static gboolean
ipuz_grid_real_check_stride (IpuzGrid          *grid,
                             IpuzClueDirection  direction,
                             guint              index,
                             IpuzGuesses       *guesses,
                             IpuzGridCheckType  check_type)
{
  /* NOTE: we don't consider stride guess when checking for
   * guess_won. We may have to go back and put in a default handler
   * here if that ever changes. */
  g_assert (IPUZ_IS_GRID (grid));

  return FALSE;
}

static void
ipuz_grid_check_cell_foreach (IpuzGrid            *grid,
                              IpuzCell            *cell,
                              const IpuzCellCoord *coord,
                              IpuzGuesses         *unused,
                              gpointer             user_data)
{
  IpuzGridCheckTuple *tuple = user_data;

  /* We use tuple->guesses instead of unused */
  tuple->retval |= ipuz_grid_check_cell (grid, cell, coord, tuple->guesses, tuple->type);
}


/**
 * ipuz_grid_get_width:
 * @self: An `IpuzGrid`
 *
 * Returns the number of columns in @self.
 *
 * Returns: The width of @self
 **/
guint
ipuz_grid_get_width (IpuzGrid *self)
{
  IpuzGridPrivate *priv;

  g_return_val_if_fail (IPUZ_IS_GRID (self), 0);

  priv = ipuz_grid_get_instance_private (self);

  return priv->width;
}

/**
 * ipuz_grid_get_height:
 * @self: An `IpuzGrid`
 *
 * Returns the number of rows in @self.
 *
 * Returns: The height of @self
 **/
guint
ipuz_grid_get_height (IpuzGrid *self)
{
  IpuzGridPrivate *priv;

  g_return_val_if_fail (IPUZ_IS_GRID (self), 0);

  priv = ipuz_grid_get_instance_private (self);

  return priv->height;
}

/**
 * ipuz_grid_get_cell:
 * @self: An `IpuzGrid`
 * @coord: Coordinates for the cell.
 *
 * Retrieves the cell at @coord. If the coordinates are
 * outside the bounds of the grid then %NULL will be returned.
 *
 * The coordinate system of the @self is similar to that of a spreadsheet. The
 * origin is the upper left corner. Row's increase vertically downward, and
 * columns increase horizontally.
 *
 * Returns: (nullable) (transfer none): The cell at @coord.
 **/
IpuzCell *
ipuz_grid_get_cell (IpuzGrid            *self,
                    const IpuzCellCoord *coord)
{
  IpuzGridPrivate *priv;
  GArray *row_array;

  g_return_val_if_fail (IPUZ_IS_GRID (self), NULL);
  g_return_val_if_fail (coord != NULL, NULL);

  priv = ipuz_grid_get_instance_private (self);

  if (coord->row >= priv->height || coord->column >= priv->width)
    return NULL;

  row_array = g_array_index (priv->cells, GArray *, coord->row);
  g_assert (row_array);

  return g_array_index (row_array, IpuzCell *, coord->column);
}

/**
 * ipuz_grid_foreach_cell:
 * @func: (scope call): The function to call for each cell
 * @user_data: User data to pass to @func
 *
 * Calls @func for each #IpuzCell in @self.
 **/
void
ipuz_grid_foreach_cell (IpuzGrid                *self,
                        IpuzGridForeachCellFunc  func,
                        gpointer                 user_data)
{
  IpuzGridPrivate *priv;

  g_return_if_fail (IPUZ_IS_GRID (self));

  priv = ipuz_grid_get_instance_private (self);

  for (guint row = 0; row < priv->height; row++)
    {
      for (guint column = 0; column < priv->width; column++)
        {
          IpuzCell *cell;
          IpuzCellCoord coord = { .row = row, .column = column };

          cell = ipuz_grid_get_cell (self, &coord);
          (func) (self, cell, &coord, priv->guesses, user_data);
        }
    }
}

/**
 * ipuz_grid_create_guesses:
 *
 * Creates a fresh [struct@Ipuz.Guesses]. It will be initialized to
 * the current state of the grid.
 *
 * The guessses can be used to solve the puzzle, and as such, its
 * format is puzzle-type dependent. See each puzzle's *Guesses*
 * section for more information on the format.
 *
 * Note that this method won't change the internal guesses of self. If
 * that is desired, you should pair this with
 * [method@Ipuz.Grid.set_guesses]. As an example:
 *
 * ```C
 * g_autoptr (IpuzGuesses) guesses;
 *
 * guesses = ipuz_grid_create_guesses (grid);
 * ipuz_grid_set_guesses (grid, guesses);
 * ```
 *
 * Returns: (transfer full): A new [struct@Ipuz.Guesses] matching
 * @self
 **/
IpuzGuesses *
ipuz_grid_create_guesses (IpuzGrid *self)
{
  IpuzGridPrivate *priv;
  IpuzGridCheckTuple tuple = {
    .type = IPUZ_GRID_CHECK_INITIALIZE_GUESS,
    .retval = FALSE,
  };

  g_return_val_if_fail (IPUZ_IS_GRID (self), NULL);

  priv = ipuz_grid_get_instance_private (self);

  tuple.guesses = ipuz_guesses_new ();
  ipuz_guesses_resize (tuple.guesses, priv->width, priv->height);
  ipuz_grid_foreach_cell (self, ipuz_grid_check_cell_foreach, &tuple);

  /* Initialize the strides. Does nothing on most puzzles */
  for (guint i = 0; i < priv->height; i++)
    ipuz_grid_check_stride (self, IPUZ_CLUE_DIRECTION_ACROSS, i,
                            tuple.guesses,
                            IPUZ_GRID_CHECK_INITIALIZE_GUESS);

  for (guint i = 0; i < priv->width; i++)
    ipuz_grid_check_stride (self, IPUZ_CLUE_DIRECTION_DOWN, i,
                            tuple.guesses,
                            IPUZ_GRID_CHECK_INITIALIZE_GUESS);

  return tuple.guesses;
}

/**
 * ipuz_grid_resize:
 * @new_width: The new width
 * @new_height: The new height
 *
 * Resizes @self to the new size.
 *
 * If one of the dimensions is larger than before, the new cells will
 * be filled in with puzzle-appropriate default values.
 *
 * This function will not adjust its internal [struct@Ipuz.Guesses]
 * object to match the new size. That can be done by a separate call
 * to [method@Ipuz.Grid.fix_guesses].
 **/
void
ipuz_grid_resize (IpuzGrid *self,
                  guint     new_width,
                  guint     new_height)
{
  IpuzGridPrivate *priv;
  guint old_width, old_height;

  g_return_if_fail (IPUZ_IS_GRID (self));

  priv = ipuz_grid_get_instance_private (self);

  /* Calculate the old dimensions from the existing array.
   */
  old_height = priv->height;
  old_width = priv->width;

  if (new_width == old_width && new_height == old_height)
    return;

  /* If we are growing vertically, add more rows. Otherwise, if we're shrinking,
   * we count on g_array_set_size to do the right thing for us. */
  if (new_height > old_height)
    {
      for (guint i = 0; i < new_height - old_height; i++)
        {
          GArray *new_row;
          new_row = g_array_new (FALSE, TRUE, sizeof (IpuzCell *));
          g_array_set_clear_func (new_row, (GDestroyNotify) cell_clear_func);
          g_array_append_val (priv->cells, new_row);
        }
    }
  else
    {
      g_array_set_size (priv->cells, new_height);
    }

  /* Resize all the rows to be the right size. This will clear memory if rows
   * shrink or fill it with empty cells if they grow.
   */
  for (guint i = 0; i < priv->cells->len; i++)
    {
      GArray *row;
      row = g_array_index (priv->cells, GArray *, i);
      g_array_set_size (row, new_width);

      for (guint j = 0; j < new_width; j++)
        {
          IpuzCell **cell_ptr = &(g_array_index (row, IpuzCell *, j));
          if (*cell_ptr == NULL)
            *cell_ptr = _ipuz_cell_new ();
        }
    }

  priv->height = new_height;
  priv->width = new_width;
}

static gboolean
ipuz_grid_validate_guesses (IpuzGrid    *self,
                            IpuzGuesses *guesses)
{
  guint width, height;

  width = ipuz_grid_get_width (self);
  height = ipuz_grid_get_height (self);

  if (width != ipuz_guesses_get_width (guesses) ||
      height != ipuz_guesses_get_height (guesses))
    return FALSE;

  for (guint row = 0; row < height; row++)
    {
      for (guint column = 0; column < width; column++)
        {
          IpuzCell *cell;
          IpuzCellCoord coord = { .row = row, .column = column };

          cell = ipuz_grid_get_cell (self, &coord);
          if (! ipuz_grid_check_cell (self, cell, &coord, guesses,
                                      IPUZ_GRID_CHECK_GUESSES_VALID))
            return FALSE;
        }
    }

  return TRUE;
}

/**
 * ipuz_grid_set_guesses:
 * @guesses: (nullable): The [struct@Ipuz.Guesses] to set on self
 *
 * Sets @guesses for @self. If there's a mismatch in the cell types
 * between @guesses and @self then %FALSE is returned. @guesses will
 * be set regardless of the return value.
 *
 * Mismatches can be fixed by calling [method@Ipuz.Grid.fix_guesses].
 *
 * Returns: %TRUE, if guesses matches @self
 **/
gboolean
ipuz_grid_set_guesses (IpuzGrid    *self,
                       IpuzGuesses *guesses)
{
  IpuzGridPrivate *priv;
  gboolean is_valid = TRUE;

  g_return_val_if_fail (IPUZ_IS_GRID (self), FALSE);

  priv = ipuz_grid_get_instance_private (self);

  if (guesses)
    {
      if (! ipuz_grid_validate_guesses (self, guesses))
        is_valid = FALSE;
      ipuz_guesses_ref (guesses);
    }

  g_clear_pointer (&priv->guesses, ipuz_guesses_unref);
  priv->guesses = guesses;
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_GUESSES]);

  return is_valid;
}

/**
 * ipuz_grid_get_guesses:
 *
 * Returns the [struct@Ipuz.Guesses] associated with @self.
 *
 * Returns: (transfer none): The [struct@Ipuz.Guesses] associated with
 * @self
 **/
IpuzGuesses *
ipuz_grid_get_guesses (IpuzGrid *self)
{
  IpuzGridPrivate *priv;

  g_return_val_if_fail (IPUZ_IS_GRID (self), FALSE);

  priv = ipuz_grid_get_instance_private (self);

  return priv->guesses;
}

/**
 * ipuz_grid_fix_guesses:
 *
 * Fixes the guesses associated with @self. The result will be a
 * playable [struct@Ipuz.Guesses] struct.
 *
 * ::: note
 *     This method will try to keep any guesses on cells intact. If a
 *     fresh playing grid is desired, then it's recommended to call
 *     [method@Ipuz.Grid.create_guesses], and then attach it to @self
 *     with [method@IpuzGrid.set_guesses].
 **/
void
ipuz_grid_fix_guesses (IpuzGrid *self)
{
  IpuzGridPrivate *priv;
  IpuzGridCheckTuple tuple = {
    .type = IPUZ_GRID_CHECK_INITIALIZE_GUESS,
    .retval = FALSE,
  };

  g_return_if_fail (IPUZ_IS_GRID (self));

  priv = ipuz_grid_get_instance_private (self);

  if (priv->guesses == NULL)
    return;

  tuple.guesses = priv->guesses;

  /* this will be a noop if the sizes haven't changed */
  ipuz_guesses_resize (priv->guesses, priv->width, priv->height);
  ipuz_grid_foreach_cell (self, ipuz_grid_check_cell_foreach, &tuple);
}

/**
 * ipuz_grid_check_cell:
 * @cell: A cell to check
 * @coord: The coordinate of @cell
 * @guesses: The guesses to check
 * @check_type: The operation to perform at @coord
 *
 * Invokes the operation determined by @check_type on @cell and
 * @guesses at @coord.
 *
 * This function allows subclasses to specify puzzle-specific
 * behavior. For example, if @check_type is %IPUZ_GRID_GUESSABLE, then
 * it will return %TRUE if the cell can accept a users's guess (eg. is
 * not a block or null with crosswords, etc).
 *
 * This function is used by [class@Ipuz.Grid] to maintain consistency
 * of the puzzle in its operations. Subclasses should implement this
 * method in order to define their behavior. It is not expected that
 * most users will ever need to call this function.
 *
 * See [enum@Ipuz.GridCheckType] for more information on the specific
 * operations.
 *
 * ::: note
 *     Some of the operation types can modify either guesses or cell.
 *
 * Returns: %TRUE, if the condition determined by %check_type is met
 **/
gboolean
ipuz_grid_check_cell (IpuzGrid            *self,
                      IpuzCell            *cell,
                      const IpuzCellCoord *coord,
                      IpuzGuesses         *guesses,
                      IpuzGridCheckType    check_type)
{
  IpuzGridClass *klass;

  g_return_val_if_fail (IPUZ_IS_GRID (self), FALSE);
  g_return_val_if_fail (coord != NULL, FALSE);

  klass = IPUZ_GRID_GET_CLASS (self);

  return klass->check_cell (self, cell, coord, guesses, check_type);
}

/**
 * ipuz_grid_check_stride:
 * @direction:
 * @index:
 * @guesses: The guesses to check
 * @check_type: The operation to perform at @coord
 *
 * Invokes the operation determined by @check_type on the stride guess
 * determined by @direction and @index.
 *
 * This function allows subclasses to specify puzzle-specific
 * behavior. Unlike [method@Ipuz.Grid.check_cell], this function is
 * optional for puzzles as most puzzles don't use stride guesses. A
 * default handler is provided that just returns %FALSE.
 *
 * This function is used by [class@Ipuz.Grid] to maintain consistency
 * of the puzzle in its operations. Subclasses should implement this
 * method in order to define their behavior. It is not expected that
 * most users will ever need to call this function.
 *
 * See [enum@Ipuz.GridCheckType] for more information on the specific
 * operations.
 *
 * ::: note
 *     Some of the operation types can modify either guesses or cell.
 *
 * Returns: %TRUE, if the condition determined by %check_type is met
 **/
gboolean
ipuz_grid_check_stride (IpuzGrid          *self,
                        IpuzClueDirection  direction,
                        guint              index,
                        IpuzGuesses       *guesses,
                        IpuzGridCheckType  check_type)
{
  IpuzGridClass *klass;

  g_return_val_if_fail (IPUZ_IS_GRID (self), FALSE);

  klass = IPUZ_GRID_GET_CLASS (self);

  return klass->check_stride (self, direction, index, guesses, check_type);

}
