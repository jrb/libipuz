/* ipuz-clue-docs.c - Documentation and annotations
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

/* This does not compile. It's just for gi-docgen and
 * gobject-introspection */


/**
 * IPUZ_CLUE_DIRECTION_HEADING:
 * @direction: The [enum@Ipuz.ClueDirection] to evaluate
 *
 * Evaluates to %TRUE if @direction has a physical direction in the puzzle.
 */

/**
 * ipuz_clue_direction_to_string:
 * @direction: An [enum@Ipuz.ClueDirection]
 *
 * Returns a string representation of @direction.
 *
 * Returns: (transfer none): a string representation of @direction.
 */

/**
 * ipuz_clue_direction_from_string:
 * @str: A string
 *
 * Returns the direction represented by @str
 *
 * Returns: the direction returns: by @str
 */

/**
 * ipuz_clue_direction_switch:
 * @direction: The [enum@Ipuz.ClueDirection] to evaluate
 *
 * Returns the direction orthoganol to @direction if it is directional.
 *
 * For example, the orthoganol direction to
 * [enum@Ipuz.ClueDirection.DOWN] is [enum@Ipuz.ClueDirection.ACROSS]
 * .
 *
 * Returns: The direction orthoganol to @direction
 */

/**
 * IpuzClueId:
 * @direction: direction of the clue
 * @index: index of the clue
 *
 * A data type that represents a relative identifier for an
 * [struct@Ipuz.Clue].
 *
 * Within a puzzle, clues can be identified by a
 * [struct@Ipuz.ClueId]. For example, this id:
 *
 * ```C
 * IpuzClueId clue_id = {
 *    .direction = IPUZ_CLUE_DIRECTION_DOWN,
 *    .index = 5
 * };
 * ```
 *
 * would refer to the 6th clue in the down direction.
 *
 * Note that @index is an offset into the list of clues, and has
 * nothing to do with the numbering the user sees. As an example, the
 * clue at *1 Across* would have an index of 0.
 *
 * If `direction == IPUZ_CLUE_DIRECTION_NONE`, then the number
 * is meaningless, and this means that there is no clue to be
 * identified (e.g. equivalent to a %NULL pointer for an [struct@Ipuz.Clue]).
 *
 * ::: note
 *     IpuzClueIds aren't guaranteed to be stable when editing
 *     puzzles. Changing the grid my lead to a change in index for a
 *     given clue.
 */

/**
 * ipuz_clue_id_copy:
 *
 * Returns a newly allocated copy of @clue_id.
 *
 * Returns: (transfer full): A newly allocated copy of @clue_id
 */

/**
 * ipuz_clue_id_free:
 *
 * Frees @clue_id.
 */

/**
 * ipuz_clue_id_equal:
 * @clue_id1: (nullable): An [struct@Ipuz.ClueId]
 * @clue_id2: (nullable): An [struct@Ipuz.ClueId]
 *
 * Returns %TRUE if @clue_id1 and @clue_id2 are identical.
 *
 * Returns: %TRUE, if @clue_id1 and @clue_id2 are identical
 */


/**
 * IpuzClue:
 *
 * An opaque data type representing an individual clue in a
 * puzzle. Clues are used by both [class@Ipuz.Crossword] and
 * [class@Ipuz.Acrostic] puzzles.
 */

/**
 * ipuz_clue_new:
 *
 * Returns a new [struct@Ipuz.Clue].
 *
 * Returns: A new [struct@Ipuz.Clue]
 */

/**
 * ipuz_clue_ref:
 *
 * Refs the clue.
 *
 * Returns: @clue. This can be used to chain calls or ref on return.
 **/

/**
 * ipuz_clue_unref:
 *
 * Unrefs @clue, which will be freed when the reference count reaches 0.
 **/

/**
 * ipuz_clue_dup:
 *
 * Makes a copy of @clue.
 *
 * Returns: (transfer full): A newly-allocated copy of @clue
 **/


/**
 * ipuz_clue_equal:
 * @clue1: An [struct@Ipuz.Clue]
 * @clue2: An [struct@Ipuz.Clue] to compare with @clue1
 *
 * Compares two clues and returns %TRUE if they are identical.
 *
 * Returns: %TRUE if the two clues match
 **/

/**
 * ipuz_clue_get_number:
 *
 * Returns the clue number.
 *
 * The clue number matches the number in [struct@Ipuz.Cell].
 *
 * Returns: the clue number
 **/

/**
 * ipuz_clue_set_number:
 * @number: The number to set
 *
 * Sets the clue number of @clue to @number.
 **/

/**
 * ipuz_clue_get_label:
 *
 * Gets the text to display in the list of clues instead of the clue
 * number.
 *
 * Returns: (transfer none): the label of @clue
 **/

/**
 * ipuz_clue_set_label:
 * @label: The label of @clue
 *
 * Sets the text to display in the list of clues instead of the clue
 * number.
 **/

/**
 * ipuz_clue_get_clue_text:
 *
 * Returns the clue text of @clue.
 *
 * Returns: (transfer none): The clue text of @clue.
 **/

/**
 * ipuz_clue_set_clue_text:
 * @clue_text: the clue text of @clue.
 *
 * Sets the clue text of @clue to be @clue_text.
 **/

/**
 * ipuz_clue_get_enumeration:
 *
 * Returns the enumeration of @clue.
 *
 * Returns: (transfer none): The enumeration of @clue
 **/

/**
 * ipuz_clue_set_enumeration:
 * @enumeration: (transfer none) (nullable): The enumeration of @clue
 *
 * Sets the enumeration of @clue to be @enumeration.
 **/

/**
 * ipuz_clue_ensure_enumeration:
 *
 * Ensures that an enumeration exists for @clue.
 *
 * If one hasn't been set yet, then a new one is generated based on
 * the length of the coords. The geometry of the clue is not
 * considered when generating this new enumeration.
 **/

/**
 * ipuz_clue_get_direction:
 *
 * Returns the direction of @clue.
 *
 * Returns: the direction of @clue
 **/

/**
 * ipuz_clue_set_direction:
 * @direction: the direction of clue
 *
 * Sets the direction of @clue to @direction.
 **/

/**
 * ipuz_clue_get_location:
 * @location: (out) (not nullable): Location to set the coord of the clue
 *
 * Sets @location to be the coord of @clue.
 *
 * This is used by [class@Ipuz.Arrowword], which display the clue
 * within a given cell. If a location has not been set for @clue, then
 * @location is left untouched and %FALSE is returned.
 *
 * Returns: %TRUE, if location is set
 **/

/**
 * ipuz_clue_set_location:
 * @location: (nullable): Location to set the coord of the clue to
 *
 * Sets the location of @clue to be @location. This is used for
 * [class@Ipuz.Arrowword].
 *
 * The location can be unset by passing in %NULL to @location.
 **/

/**
 * ipuz_clue_get_n_coords:
 *
 * Returns the number of cells associated with @clue.
 *
 * Returns: The number of cells associated with @clue.
 **/

/**
 * ipuz_clue_get_coord:
 * @index: The index of the coord to get
 * @coord: (not nullable): Location to set the coord at @index
 *
 * Sets @coord to be the coord at @index.
 *
 * If @index is longer than the number of cells associated with @clue,
 * then %FALSE is returned.
 *
 * Returns: %TRUE, if @coord is set
 **/

/**
 * ipuz_clue_get_first_coord:
 * @coord: (not nullable) (out): Location to set the first coord of @clue
 *
 * Sets @coord to be the first coord of @clue. If no coords have been
 * set to clue, then @coord is untouched and %FALSE is returned.
 *
 * Returns: %TRUE, if @coord was set to the first coord of @clue.
 **/

/**
 * ipuz_clue_get_coords:
 *
 * Returns the raw coords that @clue represents, in order.
 *
 * This method is less convenient than [method@Ipuz.Clue.get_n_coords]
 * and [method@Ipuz.Clue.get_coord]. You probably want to use those
 * methods instead.
 *
 * ::: note
 *     This returns the internal [struct@Ipuz.CellCoordArray] even if
 *     there aren't any cells associated with @clue. In that case, the
 *     length of the returned array will be 0.
 *
 * ::: warning
 *     It is possible to modify the returned array. If the caller does
 *     that, care must be taken to keep consistency with the rest of
 *     the puzzle.
 *
 * Returns: (transfer none) (not nullable): The raw
 * [struct@Ipuz.CellCoordArray] of the coords for @clue
 **/

/**
 * ipuz_clue_set_coords:
 * @coords: (not nullable): Location to set the first coord of @clue
 *
 * Sets the raw coordinates of @clue, overwriting any current values.
 **/

/**
 * ipuz_clue_append_coord:
 * @coord: A coord to append to @clue
 *
 * Appends @coord to the internal list of coords of @clue.
 **/

/**
 * ipuz_clue_clear_coords:
 *
 * Resets all the coords associated with @clue. 
 **/

/**
 * ipuz_clue_contains_coord:
 * @coord: (not nullable): A coord to search for
 *
 * Returns %TRUE if @clue contains coord in it's list of coords.
 *
 * Returns: %TRUE if @clue contains coord
 **/
