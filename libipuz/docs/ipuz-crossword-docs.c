/* ipuz-crossword-docs.c - Documentation and annotations
 *
 * Copyright 2025 Jonathan Blandford <jrb@gnome.org>
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

/* Does not compile. Just for gi-docgen and gobject-introspection */

/**
 * IpuzCrossword:
 *
 * Crosswords are a word puzzle consisting of a grid of open cells and
 * blocks into which answers are written. It has clues in both the
 * *Across* and *Down* directions to lead the solver to the
 * answer. Crosswords are frequently symmetric, and use numbers to
 * identify clues.
 *
 * The [class@Ipuz.Crossword] class is intended to model **Standard
 * Crosswords**. These are also sometimes known as **American
 * Crosswords** or **Traditional Crosswords**.
 *
 * <picture>
 * <source srcset="crossword-dark.png" media="(prefers-color-scheme: dark)" />
 * <img src="crossword.png" alt="crossword" />
 * </picture>
 * _Example of a crossword puzzle_
 *
 * As a matter of convention, standard crosswords tend to have shorter
 * clues and denser grids. The example above is a good representative
 * example of what a dense grid could look like. It also features
 * cells with [struct@Ipuz.Style]s to set circles around some of the
 * letters.
 *
 * # Representation
 *
 * ## Grid
 *
 * Crosswords are built with a grid of [struct@Ipuz.Cell]s. These can
 * be accessed through the parent class of [class@IpuzGrid]. The
 * `cell_type` of the cell determines how the cell is interpreted:
 *
 * * <img style="vertical-align:middle; display: inline" src="cell-normal.png"/> [enum@Ipuz.CellType.NORMAL] — A _normal_ light cell. This contains a solution for the player to figure out and fill in.
 * * <img style="vertical-align:middle; display: inline" src="cell-block.png"/>  [enum@Ipuz.CellType.BLOCK] — A _block_. This will separate clues and contains no letters.
 * * <img style="vertical-align:middle; display: inline" src="cell-null.png"/>   [enum@Ipuz.CellType.NULL] — An _null_ cell. Similar to blocks, it also separates clues and contains no letters. However, it is omitted during rendering
 *
 * ::: note
 *     A null cell can be used to make a shaped puzzle. This is less
 *     common than regular crosswords. But as an interesting sidenote,
 *     that the very [first
 *     crossword](https://gitlab.gnome.org/jrb/libipuz/-/blob/master/libipuz/tests/first-crossword.ipuz?ref_type=heads)
 *     was shaped and uses nulls to render correctly.
 *
 * ## Clues
 *
 * Clues can be accessed through the [iface@Ipuz.Clues] interface. In
 * general, [struct@Ipuz.Enumeration]s are not used with these
 * clues. As a result, [property@Ipuz.Crossword:show_enumerations] is
 * %FALSE by default.
 *
 * While not required, it's generally a good idea to store the
 * coordinates associated with a clue. If it doesn't exist, they can
 * be inferred using [method@Ipuz.Crossword.fix_clues].
 *
 * ## Guesses
 *
 * When used with an [struct@Ipuz.Guesses] object, guesses are tested
 * against the solution of the cell to indicate a block was guessed
 * correctly. Block and null cells are mirrored in the guesses but are
 * generally ignored.
 *
 * ## Variants
 *
 * The [class@Ipuz.Crossword] class was written to model standard
 * crosswords and its `_fix()` functions enforce those
 * conventions. However, it's possible to use this class to represent
 * variants of the standard crossword. Common ones (such as
 * [class@Ipuz.Arrowword] and [class@Ipuz.Barred]) have dedicated
 * subclasses for them. Others can be done by manually modifying the
 * puzzle.
 *
 * As some examples:
 *
 *  * **Cross figures:** Use strings of numbers instead of letters
 *  * **Rebus crosswords:**  multi-character words in the solution field of cells
 *
 * # Editing
 *
 * The [class@Ipuz.Crossword] class has five different fix functions
 * to help maintain conventions.
 *
 * * [method@Ipuz.Crossword.fix_symmetry]: Makes the grid symmetric with respect to `cell_type`.
 * * [method@Ipuz.Crossword.fix_numbering]: Updates the numbering of cells to match clue numbers
 * * [method@Ipuz.Crossword.fix_clues]: Makes sure a clue exists for each number
 * * [method@Ipuz.Crossword.fix_enumerations]: Makes sure an enumeration for each clue exists
 * * [method@Ipuz.Crossword.fix_styles]: Removes unused styles
 *
 * In addition, there is a [method@Ipuz.Crossword.fix_all] method that
 * will call these all sequentially. It is not available to language
 * bindings.
 */

/**
 * IpuzCrosswordClass:
 * @parent_class: The grid class structure representing the parent.
 * @fix_symmetry: Vfunc that enforces the board symmetry of the cells
 *   in @summetry_coords in the direction of @symmetry.
 * @fix_numbering: Vfunc that goes through all the normal cells in the
 *   grid and calculates the numbers for clues. This will clear any
 *   existing numbers or labels associated with cells.
 * @fix_clues: Vfunc that ensures that there are [struct@Ipuz.Clue]s
 *   for every sequenctial run of across or down normal cells of two
 *   or more. This will potentially add or remove existing clues as
 *   necessary.
 * @fix_enumerations: Vfunc that ensures that every clue has an
 *   enumeration if [property@Ipuz.Crossword:show-enumerations] is set
 *   to %TRUE. If not set already set, an enumeration the apparent
 *   length of the clue will be created.
 * @fix_styles: Vfunc that clears empty [struct@Ipuz.Style]s from all
 *   the cells.
 * @fix_all_valist: Convenience vfunc for calling all the `_fix()`
 *   functions in crosswords sequentially. This is not available for
 *   language bindings.
 * @clue_continues_up: Vfunc that returns %TRUE if the down clue
 *   crossing @coord can be continued up.
 * @clue_continues_down: Vfunc that returns %TRUE if the down clue
 *   crossing @coord can be continued down.
 * @clue_continues_left: Vfunc that returns %TRUE if the down clue
 *   crossing @coord can be continued left.
 * @clue_continues_right: Vfunc that returns %TRUE if the down clue
 *   crossing @coord can be continued right.
 * @mirror_cell: Vfunc that makes the cell at @dest_coord match the
 *   appearance of the cell at @src_coord while maintaining
 *   symmetry.
 * @check_mirror: Vfunc that checks the cell at @dest_coord to see if
 *   it's the symmetric opposite of the cell at @src_coord.
 *
 * The class structure for [class@Ipuz.Crossword]s.
 **/

/**
 * ipuz_crossword_new:
 *
 * Returns a newly created crossword puzzle.
 *
 * Returns: a newly allocated [class@Ipuz.Crossword]
 **/

/**
 * ipuz_crossword_get_show_enumerations: (attributes org.gtk.Method.get_property=show-enumerations)
 *
 * Returns %TRUE if @self should be displayed with enumerations.
 *
 * Returns: %TRUE if @self should be displayed with enumerations.
 **/

/**
 * ipuz_crossword_set_show_enumerations: (attributes org.gtk.Method.set_property=show-enumerations)
 * @show_enumerations: whether @self should be displayed with enumerations
 *
 * Sets whether @self should be displayed with enumerations.
 **/

/**
 * ipuz_crossword_get_clue_placement: (attributes org.gtk.Method.get_property=clue-placement)
 *
 * Returns an indication of how to display the clues relative to the grid.
 *
 * Returns: An indication of how to display the clues relative to the grid
 **/

/**
 * ipuz_crossword_set_clue_placement: (attributes org.gtk.Method.set_property=clue-placement)
 * @clue_placement: The placement of the clues
 *
 * Sets an indication of how to display the clues relative to the grid.
 **/

/**
 * ipuz_crossword_get_solution_chars:
 *
 * Returns an [struct@Ipuz.Charset] consisting of all the characters
 * in the puzzle.
 *
 * Returns: An [struct@Ipuz.Charset] consisting of all the characters
 * in the puzzle
 **/

/**
 * ipuz_crossword_get_symmetry:
 *
 * Calculates the apparent symmetry of @self based on the grid.
 *
 * Note, there can be multiple valid calculations for a given grid. As
 * a simple example, we can't say anything at all about the symmetry
 * for a blank, square board. This function returns the most specific
 * symmetry that matches.
 *
 * Returns: The apparent symmetry of the puzzle
 **/

/**
 * ipuz_crossword_fix_symmetry:
 * @symmetry: the symmetry condition to enforce
 * @symmetry_coords: (element-type IpuzCellCoord): A #GArray of #IpuzCellCoords
 *
 * Enforce the board symmetry of the cells in @summetry_coords in the
 * direction of @symmetry. That is to say, go through each cell in
 * @symmetry_coords and make sure that the appropriate cell at the
 * point(s) of symmetry have the same [enum@Ipuz.CellType].
 *
 * In addition, subclasses of [class@Ipuz.Crossword] may do further
 * fixups.
 **/

/**
 * ipuz_crossword_fix_numbering:
 *
 * Goes through all the normal cells in the grid and calculates the
 * numbers for clues. This will clear any existing numbers or labels
 * associated with cells.
 *
 * In addition, subclasses of [class@Ipuz.Crossword] may do further
 * fixups.
 **/

/**
 * ipuz_crossword_fix_clues:
 *
 * Ensures that there are [struct@Ipuz.Clue]s for every sequenctial
 * run of across or down normal cells of two or more. This will
 * potentially add or remove existing clues as necessary.
 *
 * This method will do its best to persist clues where possible. It
 * does that by keeping track of the cells that the prior clues used,
 * and copying over their answers to any new cells occupying the same
 * location. The oldclues will be unreffed by @self, and should not be
 * considered useful after this has been run.
 *
 * [method@Ipuz.Crossword.fix_numbering] should be called prior to
 * calling this function, as this uses the existing numbering to
 * annotate the created clues.
 *
 * In addition, subclasses of [class@Ipuz.Crossword] may do further
 * fixups.
 **/

/**
 * ipuz_crossword_fix_enumerations:
 *
 * Ensures that every clue has an enumeration if
 * [property@Ipuz.Crossword:show-enumerations] is set to %TRUE. If not
 * set already set, an enumeration the apparent length of the clue
 * will be created.
 *
 * In addition, subclasses of [class@Ipuz.Crossword] may do further
 * fixups.
 **/

/**
 * ipuz_crossword_fix_styles:
 *
 * Clears empty [struct@Ipuz.Style]s from all the cells in @self. See
 * [method@Ipuz.Style.is_empty] for more information on empty styles.
 *
 * In addition, subclasses of [class@Ipuz.Crossword] may do further
 * fixups.
 **/

/**
 * ipuz_crossword_fix_all: (skip)
 * @first_attribute_name: The first attribute to be fixed
 * @...: The value of the attribute that's being fixed, terminated by `NULL`
 *
 * Convenience function for calling all the `_fix()` functions in
 * crosswords sequentially. This is not available for language
 * bindings.
 *
 * Crosswords support the following rguments:
 *  * `"symmetry-coords"`: The coords to pass to [method@Ipuz.Crossword.fix_symmetry]
 *  * `"symmetry"`: The symmetry to pass to [method@Ipuz.Crossword.fix_symmetry]
 *
 * For example, the following call will result in a well formed puzzle
 * with all the coords at coord.
 *
 * ```C
 * static void
 * make_a_cell_a_block (IpuzPuzzle    *puzzle,
 *                      IpuzCellCoord *coord)
 * {
 *   g_autoptr (IpuzCellCoordArray) coords = NULL;
 *   IpuzCell *cell;
 *
 *   cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), coord);
 *   ipuz_cell_set_cell_type (cell, IPUZ_CELL_BLOCK);
 *
 *   coords = ipuz_cell_coord_array_new ();
 *   ipuz_cell_coord_array_append (coords, coord);
 *
 *   ipuz_crosswords_fix_all (IPUZ_CROSSWORDS (puzzle),
 *                            "symmetry-coords", coords,
 *                            "symmetry", IPUZ_SYMMETRY_ROTATIONAL_QUARTER,
 *                            NULL);
 * }
 * ```
 *
 * In addition, subclasses of [class@Ipuz.Crossword] may do further
 * fixups.
 **/

/**
 * ipuz_crossword_clue_continues_up:
 * @coord: A coord to test
 *
 * Returns %TRUE if the down clue crossing @coord can be continued up.
 *
 * This can be used to navigate a crossword. Sub-classes can provide
 * their own implementation to help indicate the extents of clues.
 *
 * Returns: %TRUE, if the down clue crossing with @coord can be
 * continued up
 **/

/**
 * ipuz_crossword_clue_continues_down:
 * @coord: A coord to test
 *
 * Returns %TRUE if the down clue crossing @coord can be continued
 * down.
 *
 * This can be used to navigate a crossword. Sub-classes can provide
 * their own implementation to help indicate the extents of clues.
 *
 * Returns: %TRUE, if the down clue crossing with @coord can be
 * continued down
 **/

/**
 * ipuz_crossword_clue_continues_left:
 * @coord: A coord to test
 *
 * Returns %TRUE if the down clue crossing @coord can be continued left.
 *
 * This can be used to navigate a crossword. Sub-classes can provide
 * their own implementation to help indicate the extents of clues.
 *
 * Returns: %TRUE, if the down clue crossing with @coord can be
 * continued left
 **/

/**
 * ipuz_crossword_clue_continues_right:
 * @coord: A coord to test
 *
 * Returns %TRUE if the down clue crossing @coord can be continued
 * right.
 *
 * This can be used to navigate a crossword. Sub-classes can provide
 * their own implementation to help indicate the extents of clues.
 *
 * Returns: %TRUE, if the down clue crossing with @coord can be
 * continued right.
 **/

/**
 * ipuz_crossword_mirror_cell:
 * @src_coord: The source coord to mirror
 * @dest_coord: The dest coord to mirrored
 * @symmetry: The symmetry to be observed
 * @symmetry_offset: The symmetry offset to be observed
 *
 * Makes the cell at @dest_coord match the appearance of the cell at
 * @src_coord while maintaining symmetry. For normal crosswords, this
 * just matches their `cell_type` and symmetry is largely
 * ignored. However, for barred crosswords this implementation is
 * quite complex as it needs to rotate and adjust the bars to keep
 * symmetry.
 **/

/**
 * ipuz_crossword_check_mirror:
 * @src_coord: The source coord to mirror
 * @dest_coord: The dest coord to mirrored
 * @symmetry: The symmetry to be observed
 * @symmetry_offset: The symmetry offset to be observed
 *
 * Checks the cell at @dest_coord to see if it's the symmetric
 * opposite of the cell at @src_coord. For normal crosswords, this
 * just checks that the `cell_type` is the same, and symmetry is
 * largely ignored. However, for barred crosswords this implementation
 * is quite complex as it needs to rotate and adjust the bars.
 *
 * Returns: %TRUE, if the cell at @dest_coord is the mirror of that at @src_coord
 **/

/**
 * ipuz_crossword_print:
 *
 * Prints out a text version of @self to the terminal for debugging
 * purposes.
 **/

/*
 * VFUNCS
 */

/**
 * IpuzCrossword:fix_all_valist:
 * @first_attribute_name: The first attribute to be fixed
 * @var_args: The va_list of the arguments
 *
 * Convenience function for calling all the `_fix()` functions in
 * crosswords sequentially. This is not available for language
 * bindings.
 *
 * See [method@Ipuz.Crossword.fix_all] fo fore information.
 **/
