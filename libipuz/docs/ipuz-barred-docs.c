/* ipuz-barred-docs.c - Documentation and annotations
 *
 * Copyright 2025 Jonathan Blandford <jrb@gnome.org>
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

/* Does not compile. Just for gi-docgen and gobject-introspection */

/**
 * IpuzBarred:
 *
 * A crossword puzzle with lines between words instead of
 * blocks. Their clues tend to be cryptic in nature and often exhibit
 * symmetry.
 *
 * <picture>
 * <source srcset="barred-dark.png" media="(prefers-color-scheme: dark)" />
 * <img src="barred.png" alt="barred" />
 * </picture>
 * _Example of a barred puzzle_
 *
 * Barred puzzles inherit from [class@Ipuz.Crossword] and can use all
 * of the methods associated with that class. In particular, the
 * `_fix()` methods will take bars under consideration when working
 * with the grid.
 *
 * # Representation
 *
 * Barred puzzles are not explicitly defined in the ipuz spec. As a
 * result, libipuz has an additional
 * [property@Ipuz.Puzzle:puzzle-kind] field available as a libipuz
 * extension.
 *
 * When saving, libipuz adds the `https://libipuz.org/barred#1` tag to
 * the kind field, as well as the `http://ipuz.org/crossword#1`
 * tag. This will keep compatibility with other implementations, but
 * allow libipuz to identify these puzzles. In practice, barred
 * puzzles act like crosswords when being solved and can be treated as
 * such. However the additional _kind_ tag lets libipuz load those
 * puzzles into a [class@Ipuz.Barred] object which provides additional
 * editing capabilities.
 *
 * ## Grid
 *
 * Barred puzzles use an [struct@Ipuz.Style] with
 * [flags@Ipuz.StyleSides] set to indicate lines in the puzzle. As a
 * result, many cells in the puzzle will have a style attached to
 * them. Typically, only [enum@Ipuz.CellType.NORMAL] cells are found
 * within a barred crossword. Blocks and null cells are possible, but
 * non-traditional.
 *
 * A barred puzzle will create three named styles that can be used to
 * modify the grid. These styles are empty other than a side set. They
 * can be looked up using [method@Ipuz.Puzzle.get_style] with the
 * following constants:
 *
 *  * [const@Ipuz.BARRED_STYLE_L]
 *  * [const@Ipuz.BARRED_STYLE_T]
 *  * [const@Ipuz.BARRED_STYLE_TL]
 *
 * These styles set a side on the left, top, and top-left of a cell
 * respectively. When building the grid's shape, these are used to
 * fill out the overall shape of the grid. There's no need for right,
 * bottom, and bottom-right borders as that effect can be had by
 * setting the above styles on a neighbooring cell. See
 * [method@Ipuz.Barred.set_cell_bars] for more information.
 *
 * It's possible for users of the cell to reuse these styles — or
 * provide their own. For example, you could set a style with a border
 * on the top and with the answer circled. The `_fix()` functions will
 * recognize and preserve that.
 *
 * ## Clues
 *
 * Clues with [class@Ipuz.Barred] objects are the same as with
 * [class@Ipuz.Crossword], and can be accessed through the
 * [iface@Ipuz.Clues] interface.
 *
 * ## Guesses
 *
 * Guesses with [class@Ipuz.Barred] objects are identical to
 * [class@Ipuz.Crossword].
 *
 * # Editing
 *
 * Editing a barred puzzle can be done with a combination of
 * [method@Ipuz.Barred.set_cell_bars] and the [class@Ipuz.Crossword]
 * `_fix()` functions. One thing to call out is that
 * [method@Ipuz.Crossword.fix_styles] will convert all styles that
 * only contain bars to the internal styles.
 *
 * ::: note
 *     It's also worth pointing out that filling a barred grid with
 *     letters is even harder then filling a standard
 *     crossword. Solving this is outside the scope of libipuz.
 */

/**
 * IPUZ_BARRED_STYLE_T
 *
 * String to pass to [method@Ipuz.Puzzle.get_style] to acquire the top
 * style of an [class@Ipuz.Barred] instance.
 */

/**
 * IPUZ_BARRED_STYLE_L
 *
 * String to pass to [method@Ipuz.Puzzle.get_style] to acquire the
 * left style of an [class@Ipuz.Barred] instance.
 */

/**
 * IPUZ_BARRED_STYLE_TL
 *
 * String to pass to [method@Ipuz.Puzzle.get_style] to acquire the
 * top-left style of an [class@Ipuz.Barred] instance.
 */

/**
 * ipuz_barred_new:
 *
 * Returns a newly created barred puzzle.
 *
 * Returns: a newly allocated [class@Ipuz.Barred]
 **/

/**
 * ipuz_barred_calculate_side_toggle:
 * @coord: The cell to be toggled
 * @side: The side to be toggled. This must be a single value
 * @symmetry: The symmetry to respect
 *
 * Calculate the sides of a cell after toggling one of its sides while
 * taking symmetry into account. For most of the cells on the grid
 * this just changes the @side. However, the center line of symmetry
 * and the center square need special handling as toggling those cells
 * will modify multiple sides.
 *
 * Returns: The new sides of the cell at @coord
 **/

/**
 * ipuz_barred_set_cell_bars:
 * @coord: Coordinate of the cell to change
 * @sides: The location of the bars to be set. May contain multiple values ORed together.
 *
 * Sets the cell at @coord to have bars as determined by @sides. Note
 * that if @sides contains [flags@Ipuz.StyleSides.RIGHT], then this
 * will be implemented by setting the left side of the cell to the
 * right of @coord. A similar pattern occurs when @sides contains
 * [flags@Ipuz.StyleSides.BOTTOM].
 *
 * Returns %TRUE if the the cell at @coord has new sides.
 *
 * Returns: %TRUE, if the cell at @coord has new sides
 */

/**
 * ipuz_barred_get_cell_bars:
 * @coord: Coordinate of the cell to inspect
 *
 * Returns the orientation of any bars set around the cell at
 * @coord. This function will inspect the style at @coord — as well as
 * any neighboring cells — in order to calculate this value.
 *
 * Returns: The orientation of any bars set around the cell at @coord
 **/
