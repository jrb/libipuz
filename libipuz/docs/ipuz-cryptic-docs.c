/* ipuz-cryptic-docs.c - Documentation and annotations
 *
 * Copyright 2025 Jonathan Blandford <jrb@gnome.org>
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

/* Does not compile. Just for gi-docgen and gobject-introspection */

/**
 * IpuzCryptic:
 *
 * Cryptics are a crossword variant featuring cryptic clues.
 *
 * <picture>
 * <source srcset="cryptic-dark.png" media="(prefers-color-scheme: dark)" />
 * <img src="cryptic.png" alt="cryptic" />
 * </picture>
 * _Example of a cryptic puzzle_
 *
 * Cryptic crosswords traditionally have lattice-like grids with fewer
 * — but longer — answers. It uses [cryptic
 * clues](https://en.wikipedia.org/wiki/Cryptic_crossword), which use
 * wordplay to give additional information when solving them. As a
 * result, the dense grid of words are not neccessary. These
 * crosswords are sometimes known as **British Crosswords**.
 *
 * Similar to [class@Ipuz.Crossword]s, cryptics are frequently
 * symmetric and use numbered clues.
 *
 * # Representation
 *
 * When saving, libipuz adds the tag
 * `http://ipuz.org/crossword/crypticcrossword#1` to the kind field,
 * as well as `http://ipuz.org/crossword#1`. This allows libipuz to
 * identify these puzzle types. In practice, cryptic puzzles act like
 * crosswords when being solved and can be treated as such. However
 * the additional _kind_ tag lets libipuz load those puzzles into a
 * [class@Ipuz.Cryptic] object, which lets developers differentiate
 * them.
 *
 * ## Grid
 *
 * The grid with [class@Ipuz.Cryptic] objects is identical to
 * [class@Ipuz.Crossword].
 *
 * ## Clues
 *
 * Clues with [class@Ipuz.Cryptic] objects are mostly the same as with
 * [class@Ipuz.Crossword], and can be accessed through the
 * [iface@Ipuz.Clues] interface.
 *
 * One minor difference between cryptics and regular crosswords is the
 * common usage of [struct@Ipuz.Enumeration] to give additional hints
 * to the answer. As a result, [ctor@Ipuz.Cryptic.new] will enable
 * [property@Ipuz.Crossword:show_enumerations] by default.
 *
 * ## Guesses
 *
 * Guesses with [class@Ipuz.Cryptic] objects are identical to
 * [class@Ipuz.Crossword].
 *
 * # Editing
 *
 * Editing [class@Ipuz.Cryptic] objects is the identical to
 * [class@Ipuz.Crossword].
 */

/**
 * ipuz_cryptic_new:
 *
 * Returns a newly created cryptic puzzle. The new puzzle will have
 * [property@Ipuz.Crossword:show_enumerations] enabled by default.
 *
 * Returns: a newly allocated [class@Ipuz.Cryptic]
 **/
