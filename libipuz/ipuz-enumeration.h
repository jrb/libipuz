/* ipuz-enumeration.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS


#define IPUZ_DELIM_IS_MODIFIER(d)  (d==IPUZ_DELIMINATOR_ALLCAPS||d==IPUZ_DELIMINATOR_CAPITALIZED||d ==IPUZ_DELIMINATOR_FOREIGN)
#define IPUZ_DELIM_IS_SEPARATOR(d) ((guint)d<=IPUZ_DELIMINATOR_APOSTROPHE)

/**
 * IpuzDeliminator:
 * @IPUZ_DELIMINATOR_WORD_BREAK: A word break. Represented by the space character
 * @IPUZ_DELIMINATOR_PERIOD: A period. Represented by `.`
 * @IPUZ_DELIMINATOR_DASH: A dash. Represented by `-`
 * @IPUZ_DELIMINATOR_APOSTROPHE: An apostrophe. Represented by `'`
 * @IPUZ_DELIMINATOR_ALLCAPS: The following word is capitalized. Represented by `*`
 * @IPUZ_DELIMINATOR_CAPITALIZED: The letter is capitalized. Represented by `^`
 * @IPUZ_DELIMINATOR_FOREIGN: The following word is foreign, slang, dialect, obsolete, archaic. Represented by `+`
 *
 * A deliminator is a hint in a puzzle grid about the nature of a cell
 * or word. This enum indicates the type of deliminator that should be
 * used for a given location in the grid.
 */
typedef enum _IpuzDeliminator
{
  IPUZ_DELIMINATOR_WORD_BREAK,
  IPUZ_DELIMINATOR_PERIOD,
  IPUZ_DELIMINATOR_DASH,
  IPUZ_DELIMINATOR_APOSTROPHE,

  /* Word Modifiers */
  IPUZ_DELIMINATOR_ALLCAPS,
  IPUZ_DELIMINATOR_CAPITALIZED,
  IPUZ_DELIMINATOR_FOREIGN,
} IpuzDeliminator;

typedef enum _IpuzVerbosity
{
  IPUZ_VERBOSITY_STANDARD, /* "4,4" */
  IPUZ_VERBOSITY_SPARSE,   /* "Two words" */
} IpuzVerbosity;


typedef struct _IpuzEnumeration IpuzEnumeration;


/**
 * IpuzEnumerationForeachDelimFunc:
 * @enumeration: The #IpuzEnumeration to iterate through
 * @delim: The next #IpuzDeliminator being iterated
 * @grid_offset: The offset of @delim within the grid
 * @final_word: Whether the deliminator marks the end of the final word
 * @user_data: data to be passed to the function
 *
 * The function to be passed to [method@Ipuz.Enumeration.foreach_delim].
 *
 * @grid_offset indicates what position within the clue the
 * deliminator can be found. It includes both the borders and the
 * cells in its count, with the inital left border starting at 0. For
 * example, consider the enumeration `"^4-4 3"`:
 *
 * ![Enumeration "^4-4 3"](delims.png)
 *
 * This function will be called in the following places:
 *
 * - At `grid_offset=1` with `delim=IPUZ_DELIMINATOR_CAPITALIZED` on the "C" character
 * - At `grid_offset=8` with  `delim=IPUZ_DELIMINATOR_DASH` between cells 4 and 5
 * - At `grid_offset=16` with `delim=IPUZ_DELIMINATOR_WORD_BREAK` betwen cells 8 and 9
 * - At `grid_offset=22` with `delim=IPUZ_DELIMINATOR_WORD_BREAK` and @final_word set to %TRUE
 *
 * ::: note
 *     The final %IPUZ_DELIMINATOR_WORD_BREAK is implicit and
 *     shouldn't be rendered in the puzzle. It is used to indicate the
 *     length of the final word, and is useful for continuations.
 *
 */
typedef void (*IpuzEnumerationForeachDelimFunc) (IpuzEnumeration *enumeration,
                                                 IpuzDeliminator  delim,
                                                 guint            grid_offset,
                                                 gboolean         final_word,
                                                 gpointer         user_data);


#define IPUZ_TYPE_ENUMERATION (ipuz_enumeration_get_type ())


GType            ipuz_enumeration_get_type      (void);
IpuzEnumeration *ipuz_enumeration_new           (const gchar                     *src,
                                                 IpuzVerbosity                    verbosity);
IpuzEnumeration *ipuz_enumeration_ref           (IpuzEnumeration                 *self);
void             ipuz_enumeration_unref         (IpuzEnumeration                 *self);
gboolean         ipuz_enumeration_equal         (const IpuzEnumeration           *enumeration1,
                                                 const IpuzEnumeration           *enumeration2);
gchar           *ipuz_enumeration_get_src       (const IpuzEnumeration           *self);
gchar           *ipuz_enumeration_get_display   (const IpuzEnumeration           *self);
gboolean         ipuz_enumeration_get_has_delim (const IpuzEnumeration           *self);
gint             ipuz_enumeration_delim_length  (const IpuzEnumeration           *self);
void             ipuz_enumeration_foreach_delim (const IpuzEnumeration           *self,
                                                 IpuzEnumerationForeachDelimFunc  func,
                                                 gpointer                         user_data);
gboolean         ipuz_enumeration_valid_char    (gchar                            c);


G_DEFINE_AUTOPTR_CLEANUP_FUNC(IpuzEnumeration, ipuz_enumeration_unref)


G_END_DECLS
