use glib::subclass::prelude::*;
use gio::prelude::*;
use glib::translate::*;
use glib::types::StaticType;

use std::cmp;
use std::ffi::{c_char, c_uint, CStr, CString};
use std::sync::{Arc, Mutex};
use glib::Quark;
use glib::ffi::{gboolean, g_compute_checksum_for_string, G_CHECKSUM_SHA1, GError, GType};
use gio::ffi::{GInputStream, GCancellable};
use gio::InputStream;
// For some reason, InputStream::from_glib_none doesn't work without this import
use gio::glib::translate::FromGlibPtrNone;

use serde_json::{json, Value};
use std::fs;
use crate::error::set_error_literal;
use crate::cell::*;

// Keep this in sync with ipuz-clue.h:IpuzClueDirection
#[repr(C)]
#[derive(PartialEq)]
pub enum IpuzClueDirection {
    None,
    Across,
    Down,
    Diagonal,
    DiagonalUp,
    DiagonalDownLeft,
    DiagonalUpLeft,
    Zones,
    Clues,
    Hidden,
    Custom,
}

#[derive(Clone, glib::SharedBoxed)]
#[shared_boxed_type(name = "IpuzGuesses")]
pub struct Wrapper(Arc<Mutex<Guesses>>);

pub type IpuzGuesses = <<Wrapper as SharedType>::RefCountedType as RefCounted>::InnerType;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Guesses {
    cells: Vec<Vec<IpuzGuessCell>>,
    rows: usize,
    columns: usize,
    puzzle_id: String,
    per_column_stride_guess: Vec<Option<CString>>,
    per_row_stride_guess: Vec<Option<CString>>,
}

#[derive(Clone, Debug, Default, PartialEq)]
struct IpuzGuessCell {
    cell_type: IpuzCellType,
    guess: Option<CString>,
}

#[repr(C)]
#[derive(Clone, Copy)]
pub struct IpuzCellCoord {
    row: c_uint,
    column: c_uint,
}

/// Rust-size version of IpuzCellCoord, with `usize` instead of ints for convenience
struct CellCoord {
    row: usize,
    column: usize,
}

impl From<IpuzCellCoord> for CellCoord {
    fn from(c: IpuzCellCoord) -> CellCoord {
        CellCoord {
            row: c.row as usize,
            column: c.column as usize,
        }
    }
}

impl From<CellCoord> for IpuzCellCoord {
    fn from(c: CellCoord) -> IpuzCellCoord {
        IpuzCellCoord {
            row: c.row as c_uint,
            column: c.column as c_uint,
        }
    }
}

/// Reads a vector of optional CStrings, per the Guesses conventions.
///
/// By the time we read the "per_row_stride_guess" / "per_column_stride_guess" keys from the
/// JSON, we know the size they ought to be (the number of rows or columns in the saved
/// solution, respectively).  So, we check that their respective values are JSON arrays
/// of the appropriate size.
///
/// Note that it is allowed for those keys not to exist.  In this case, the incoming `value`
/// will be `serde_json::Value::Null`; this is valid and the returned vector will simply be
/// full of `None` values.
///
/// FIXME: this should return a Result with a meaningful error in case the `value` is not
/// an array of the appropriate length, or if it does not contain only `null` or strings.
fn validated_stride_guess(value: &Value, expected_num: usize) -> Vec<Option<CString>> {
    if let Some(array) = value.as_array() {
        if array.len() == expected_num && array.iter().all(|v| v.is_string() || v.is_null()) {
            let mut cstrings = Vec::with_capacity(array.len());
            for v in array {
                match v {
                    Value::String(st) => {
                        cstrings.push(Some(CString::new(st.clone()).unwrap()));
                    }
                    _ => {
                        cstrings.push(None);
                    }
                }
            }
            return cstrings;
        }
    }

    vec![None; expected_num]
}

impl Guesses {
    fn get_cell(&self, coord: CellCoord) -> &IpuzGuessCell {
        return &self.cells[coord.row][coord.column];
    }

    fn get_cell_mut(&mut self, coord: CellCoord) -> &mut IpuzGuessCell {
        return &mut self.cells[coord.row][coord.column];
    }

    fn new_from_value(v: Value) -> Self {
        let arr: Vec<Vec<Value>> = serde_json::from_value(v["saved"].to_owned()).unwrap();
        let mut guesses = Guesses::default();
        for i in 0..arr.len() {
            guesses.cells.push(Vec::new());
            for j in 0..arr[i].len() {
                match &arr[i][j] {
                    Value::Null => {
                        guesses.cells[i].push(IpuzGuessCell {
                            cell_type: IpuzCellType::IpuzCellNull,
                            guess: None
                        });
                    },
                    Value::String(st) => {
                        match st.as_str() {
                            "#" =>  {
                                guesses.cells[i].push(IpuzGuessCell {
                                    cell_type: IpuzCellType::IpuzCellBlock,
                                    guess: None
                                });
                            }

                            "" => {
                                guesses.cells[i].push(IpuzGuessCell {
                                    cell_type: IpuzCellType::IpuzCellNormal,
                                    guess: None
                                });
                            }

                            _ => {
                                guesses.cells[i].push(IpuzGuessCell {
                                    cell_type: IpuzCellType::IpuzCellNormal,
                                    guess: CString::new(st.clone()).ok()
                                });
                            }
                        }
                    },
                    _ => (),
                }
            }
            guesses.columns = std::cmp::max(guesses.columns, guesses.cells[i].len());
        }
        guesses.rows = guesses.cells.len();

        guesses.per_column_stride_guess = validated_stride_guess(&v["per_column_stride_guess"], guesses.columns);
        guesses.per_row_stride_guess = validated_stride_guess(&v["per_row_stride_guess"], guesses.rows);

        guesses
    }

    fn error_quark() -> Quark {
        Quark::from_str("ipuz-guesses-error-quark")
    }

    fn resize(&mut self, new_width: usize, new_height: usize) {
        let old_width = self.columns;
        let old_height = self.rows;

        if old_width == new_width && old_height == new_height {
            return;
        }

        let empty_cell = IpuzGuessCell {
            cell_type: IpuzCellType::IpuzCellNull,
            guess: None
        };

        self.cells.resize_with (new_height, || { vec![empty_cell.clone(); new_width] });

        for i in 0..cmp::min (old_height, new_height) {
            self.cells[i].resize (new_width, empty_cell.clone());
        }

        self.per_column_stride_guess.resize(new_width, None);
        self.per_row_stride_guess.resize(new_height, None);

        self.rows = new_height;
        self.columns = new_width;
    }

    /// Associate a data string with a certain column.
    ///
    /// Use `None` for `data` if you want to clear its data.
    fn set_column_stride_guess(&mut self, data: Option<CString>, index: usize) {
        self.per_column_stride_guess[index] = data
    }

    /// Get the string data that was associated to a column with [`Guesses::set_column_stride_guess()`]
    fn get_column_stride_guess(&self, index: usize) -> Option<&CString> {
        self.per_column_stride_guess[index].as_ref()
    }

    /// Associate a data string with a certain row.
    ///
    /// Use `None` for `data` if you want to clear its data.
    fn set_row_stride_guess(&mut self, data: Option<CString>, index: usize) {
        self.per_row_stride_guess[index] = data
    }

    /// Get the string data that was associated to a row with [`Guesses::set_row_stride_guess()`]
    fn get_row_stride_guess(&self, index: usize) -> Option<&CString> {
        self.per_row_stride_guess[index].as_ref()
    }

    /// Serialize the `Guesses` to a JSON object value.
    ///
    /// The `per_column_stride_guess` and `per_row_stride_guess` fields are only output
    /// if they have any data in them; otherwise the resulting object dictionary will
    /// not contain those properties.
    fn to_json_value(&self) -> Value {
        let mut saved: Vec<Vec<Value>> = Vec::new();

        const IPUZ_DEFAULT_BLOCK: &str = "#";
        for row in 0..self.cells.len() {
            saved.push(Vec::new());
            for column in 0..self.cells[row].len() {
                match self.cells[row][column].cell_type {
                    IpuzCellType::IpuzCellNull => saved[row].push(Value::Null),
                    IpuzCellType::IpuzCellBlock => saved[row].push(Value::String(IPUZ_DEFAULT_BLOCK.to_owned())),
                    IpuzCellType::IpuzCellNormal => saved[row].push({
                        if let Some (guess) = &self.cells[row][column].guess {
                            Value::String(guess.to_str().unwrap().to_string())
                        } else {
                            Value::Null
                        }
                    }),
                }
            }
        }

        let mut json_content = json!({
            "puzzle-id": self.puzzle_id,
            "saved": saved,
        });

        add_array_if_not_full_of_none(&mut json_content, "per_column_stride_guess", &self.per_column_stride_guess);
        add_array_if_not_full_of_none(&mut json_content, "per_row_stride_guess", &self.per_row_stride_guess);

        json_content
    }

    /// Serialize the `Guesses` to JSON, as with `to_json_value()`, and convert it to a
    /// string.
    fn to_string(&self) -> String {
        self.to_json_value().to_string()
    }
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_new() -> *const IpuzGuesses {
    let guesses = Guesses::default();

    Wrapper(Arc::new(Mutex::new(guesses))).into_refcounted().into_raw()
}


#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_new_from_file(
    filename: *const c_char,
    error: *mut *mut GError
) -> *const IpuzGuesses {
    ipuz_return_val_if_fail! {
        ipuz_guesses_new_from_file => std::ptr::null();

        !filename.is_null(),
    }

    match fs::read_to_string(CStr::from_ptr(filename).to_str().unwrap()) {
        Err(err) => {
            set_error_literal(error, Guesses::error_quark(), err);
            std::ptr::null()
        },
        Ok(s) => {
            let v: Value = serde_json::from_str(&s).unwrap();

            Wrapper(Arc::new(Mutex::new(Guesses::new_from_value(v)))).into_refcounted().into_raw()
        }
    }

}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_new_from_stream(
    stream: *mut GInputStream,
    cancellable: *mut GCancellable,
    error: *mut *mut GError
) -> *const IpuzGuesses {
    ipuz_return_val_if_fail! {
        ipuz_guesses_new_from_stream => std::ptr::null();

        !stream.is_null(),
    }
    let input_stream = InputStream::from_glib_none(stream);
    match serde_json::from_reader(input_stream.into_read()){
        Err(err) => {
            set_error_literal(error, Guesses::error_quark(), err.into());
            gio::ffi::g_cancellable_cancel(cancellable);
            std::ptr::null()
        },
        Ok(v) => {
            Wrapper(Arc::new(Mutex::new(Guesses::new_from_value(v)))).into_refcounted().into_raw()
        }
    }

}

fn is_full_of_none(slice: &[Option<CString>]) -> bool {
    slice.iter().all(|v| v.is_none())
}

fn add_array_if_not_full_of_none(json: &mut Value, key: &str, array: &[Option<CString>]) {
    if !is_full_of_none(array) {
        let string_array: Vec<Option<String>> = array
            .iter()
            .map(|opt_cstring| {
                opt_cstring.as_ref().map(|cstring| {
                    cstring.clone().into_string().expect("")
                })
            })
            .collect();
        let v = Value::from(string_array);
        json.as_object_mut().unwrap().insert(key.to_string(), v);
    }
}

// TODO: add tests for this function
#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_save_to_file(
    guesses: *const IpuzGuesses,
    filename: *const c_char,
    error: *mut *mut GError
) -> gboolean {
    ipuz_return_val_if_fail! {
        ipuz_guesses_save_to_file => false.into_glib();

        !guesses.is_null(),
        !filename.is_null(),
    }

    let guesses = (*guesses).lock().unwrap();
    let filename = CStr::from_ptr(filename).to_str().unwrap();

    let json_string = guesses.to_string();

    return match fs::write(filename, json_string) {
        Err(err) => {
            // FIXME: refactor this to a separate file
            set_error_literal(error, Guesses::error_quark(), err);
            false.into_glib()
        },
        Ok(()) => true.into_glib(),
    };
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_ref(guesses: *const IpuzGuesses) -> *const IpuzGuesses {
    ipuz_return_val_if_fail! {
        ipuz_guesses_ref => std::ptr::null();

        !guesses.is_null(),
    }

    Arc::increment_strong_count(guesses);
    guesses
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_unref(guesses: *const IpuzGuesses) {
    // TODO: These are giving warnings
    /*ipuz_return_if_fail! {
        ipuz_guesses_unref;

        !guesses.is_null(),
    }*/

    if !guesses.is_null() {
        Arc::decrement_strong_count(guesses);
    }
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_copy (src: *const IpuzGuesses) -> *const IpuzGuesses {
    // TODO: These are giving warnings
    /*ipuz_return_val_if_fail! {
        ipuz_guesses_copy => std::ptr::null();

        !src.is_null(),
    }*/

    if src.is_null() {
        return std::ptr::null();
    }

    let src = (*src).lock().unwrap();
    Wrapper(Arc::new(Mutex::new(src.clone()))).into_refcounted().into_raw()
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_equal(a: *const IpuzGuesses, b: *const IpuzGuesses) -> gboolean {
    if a.is_null() && b.is_null() {
        return true.into_glib();
    }

    if a.is_null() || b.is_null() {
        return false.into_glib();
    }

    let a = (*a).lock().unwrap();
    let b = (*b).lock().unwrap();
    (*a == *b).into_glib()
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_resize(guesses: *const IpuzGuesses,
                                             new_width: c_uint,
                                             new_height: c_uint)
{
    ipuz_return_if_fail! {
        ipuz_guesses_resize;

        !guesses.is_null(),
    }

    let mut guesses = (*guesses).lock().unwrap();
    let new_width = new_width as usize;
    let new_height = new_height as usize;
    guesses.resize(new_width, new_height);
}


#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_get_width(guesses: *const IpuzGuesses) -> c_uint
{
    ipuz_return_val_if_fail! {
        ipuz_guesses_get_width => 0;

        !guesses.is_null(),
    }

    let guesses = (*guesses).lock().unwrap();
    return guesses.columns as c_uint;
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_get_height(guesses: *const IpuzGuesses) -> c_uint
{
    ipuz_return_val_if_fail! {
        ipuz_guesses_get_height => 0;

        !guesses.is_null(),
    }

    let guesses = (*guesses).lock().unwrap();
    return guesses.rows as c_uint;
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_get_guess(guesses: *const IpuzGuesses,
                        coord: *const IpuzCellCoord) -> *const c_char
{
    ipuz_return_val_if_fail! {
        ipuz_guesses_get_guess => std::ptr::null();

        !guesses.is_null(),
        !coord.is_null(),
    }

    let guesses = (*guesses).lock().unwrap();
    let coord = CellCoord::from(*coord);

    ipuz_return_val_if_fail! {
        ipuz_guesses_get_guess => std::ptr::null();

        coord.row < guesses.rows,
        coord.column < guesses.columns,
    }

    let cell = guesses.get_cell(coord);
    if let Some(guess) = &cell.guess {
        return guess.as_ptr();
    } else {
        return std::ptr::null();
    }
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_set_guess(
    guesses: *mut IpuzGuesses,
    coord: *const IpuzCellCoord,
    guess: *const c_char
) {
    ipuz_return_if_fail! {
        ipuz_guesses_set_guess;

        !guesses.is_null(),
        !coord.is_null(),
    }

    let mut guesses = (*guesses).lock().unwrap();
    let coord = CellCoord::from(*coord);

    ipuz_return_if_fail! {
        ipuz_guesses_set_guess;

        coord.row < guesses.rows,
        coord.column < guesses.columns,
    }

    let cell = guesses.get_cell_mut(coord);

    if guess.is_null() {
        cell.guess = None;
    } else {
        cell.guess = Some(CStr::from_ptr(guess).to_owned());
    }
}


#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_set_cell_type(
    guesses: *const IpuzGuesses,
    coord: *const IpuzCellCoord,
    cell_type: IpuzCellType) {
    ipuz_return_if_fail! {
        ipuz_guesses_set_cell_type;

        !guesses.is_null(),
        !coord.is_null(),
    }

    let mut guesses = (*guesses).lock().unwrap();
    let coord = CellCoord::from(*coord);

    ipuz_return_if_fail! {
        ipuz_guesses_set_cell_type;

        coord.row < guesses.rows,
        coord.column < guesses.columns,
    }

    let cell = guesses.get_cell_mut(coord);
    cell.cell_type = cell_type;
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_get_cell_type(
    guesses: *const IpuzGuesses,
    coord: *const IpuzCellCoord
) -> IpuzCellType {
    ipuz_return_val_if_fail! {
        ipuz_guesses_get_cell_type => IpuzCellType::IpuzCellNormal;

        !guesses.is_null(),
        !coord.is_null(),
    }

    let guesses = (*guesses).lock().unwrap();
    let coord = CellCoord::from(*coord);

    ipuz_return_val_if_fail! {
        ipuz_guesses_get_cell_type => IpuzCellType::IpuzCellNormal;

        coord.row < guesses.rows,
        coord.column < guesses.columns,
    }

    let cell = guesses.get_cell(coord);
    return cell.cell_type;
}

/* Returns the percentage filled out. Not the percentage correct
 */
#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_get_percent(guesses: *const IpuzGuesses) -> f32
{
    ipuz_return_val_if_fail! {
        ipuz_guesses_get_percent => 0.0;

        !guesses.is_null(),
    }

    let guesses = (*guesses).lock().unwrap();
    let mut guessed = 0;
    let mut total = 0;

    for row in 0..guesses.rows {
        for column in 0..guesses.columns {
            let cell = &guesses.cells[row][column];
            if cell.cell_type == IpuzCellType::IpuzCellNormal {
                total += 1;
                if ! cell.guess.is_none () {
                    guessed += 1;
                }
            }
        }
    }

    if total == 0 {
        return 0.0;
    }

    return (guessed as f32) / (total as f32);
}


/**
 * ipuz_guesses_get_checksum:
 * @guesses: An `IpuzGuess`
 * @salt: used to seed the checksum, or NULL
 *
 * Returns a SHA1 HASH representing the current state of the
 * puzzle. Used to
 *
 * Returns (transfer full): a newly allocated checksum of the solution
 **/
#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_get_checksum(guesses: *const IpuzGuesses,
                           salt: *const c_char) -> *mut c_char
{
    ipuz_return_val_if_fail! {
        ipuz_guesses_get_checksum => std::ptr::null_mut();

        !guesses.is_null(),
    }
    let guesses = (*guesses).lock().unwrap();
    let mut str = String::default();
    for row in 0..guesses.rows {
        for column in 0..guesses.columns {
            let cell = &guesses.cells[row][column];
            if cell.cell_type == IpuzCellType::IpuzCellNormal {
                if let Some (guess) = &cell.guess {
                    str += &guess.to_str().unwrap();
                } else {
                    let _ipuz_default_empty = "0";
                    str += _ipuz_default_empty;
                }
            }
        }
    }

    if !salt.is_null() {
        str += CStr::from_ptr(salt).to_str().unwrap();
    }

    return g_compute_checksum_for_string(G_CHECKSUM_SHA1, str.to_glib_none().0, str.len() as _);
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_print(guesses: *const IpuzGuesses) {
    ipuz_return_if_fail! {
        ipuz_guesses_print;

        !guesses.is_null(),
    }

    let guesses = (*guesses).lock().unwrap();

    for _i in 0..=guesses.columns {
        print!("██");
    }

    print!("\n");
    for row in 0..guesses.rows {
        print!("█");
        for column in 0..guesses.columns {
            let coord = CellCoord { row, column };

            match guesses.get_cell(coord).cell_type {
                IpuzCellType::IpuzCellBlock => print!("▓▓"),
                IpuzCellType::IpuzCellNull => print!("▞▚"),
                IpuzCellType::IpuzCellNormal => print!("  "),
            }
        }
        print!("█\n█");
        for column in 0..guesses.columns {
            let coord = CellCoord { row, column };
            let cell = guesses.get_cell(coord);
            let cell_type = cell.cell_type;
            match cell_type {
                IpuzCellType::IpuzCellBlock => print!("▓▓"),
                IpuzCellType::IpuzCellNull => print!("▚▞"),
                IpuzCellType::IpuzCellNormal => {
                    if let Some(guess) = &cell.guess {
                        print!(" {}", guess.to_str().unwrap());
                    } else {
                        print!("  ");
                    }
                },
            }
        }
        print!("█\n");
    }
    for _column in 0..=guesses.columns {
        print!("██");
    }
    print!("\n\n");
}


#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_get_type() -> GType {
    Wrapper::static_type().into_glib()
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_set_stride_guess(
    guesses: *const IpuzGuesses,
    direction: IpuzClueDirection,
    index: c_uint,
    guess: *const c_char,
) {
    ipuz_return_if_fail! {
        ipuz_guesses_set_stride_guess;

        !guesses.is_null(),
        direction == IpuzClueDirection::Down || direction == IpuzClueDirection::Across,
    }

    let mut guesses = (*guesses).lock().unwrap();

    let index = index as usize;
    let guess = if guess.is_null() {
        None
    } else {
        Some(CStr::from_ptr(guess).to_owned())
    };

    match direction {
        IpuzClueDirection::Down => {
            ipuz_return_if_fail! {
                ipuz_guesses_get_stride_guess;
                index < guesses.per_column_stride_guess.len(),
            }

            guesses.set_column_stride_guess(guess, index);
        }

        IpuzClueDirection::Across => {
            ipuz_return_if_fail! {
                ipuz_guesses_get_stride_guess;
                index < guesses.per_row_stride_guess.len(),
            }

            guesses.set_row_stride_guess(guess, index);
        }

        _ => unreachable!()
    }
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_get_stride_guess(
    guesses: *const IpuzGuesses,
    direction: IpuzClueDirection,
    index: c_uint,
) -> *const c_char {
    ipuz_return_val_if_fail! {
        ipuz_guesses_get_stride_guess => std::ptr::null();

        !guesses.is_null(),
        direction == IpuzClueDirection::Down || direction == IpuzClueDirection::Across,
    }

    let guesses = (*guesses).lock().unwrap();

    let index = index as usize;

    match direction {
        IpuzClueDirection::Down => {
            ipuz_return_val_if_fail! {
                ipuz_guesses_get_stride_guess => std::ptr::null();
                index < guesses.per_column_stride_guess.len(),
            }

            if let Some(s) = &guesses.get_column_stride_guess(index) {
                s.as_ptr()
            } else {
                std::ptr::null()
            }
        }

        IpuzClueDirection::Across => {
            ipuz_return_val_if_fail! {
                ipuz_guesses_get_stride_guess => std::ptr::null();
                index < guesses.per_row_stride_guess.len(),
            }

            if let Some(s) = &guesses.get_row_stride_guess(index) {
                s.as_ptr()
            } else {
                std::ptr::null()
            }
        }

        _ => std::ptr::null(),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn validates_null_stride_guess() {
        assert_eq!(validated_stride_guess(&Value::Null, 10), vec![None::<CString>; 10]);
    }

    #[test]
    fn non_string_data_does_not_validate() {
        let value = json!([1, 2, 3]);
        assert_eq!(validated_stride_guess(&value, 3), vec![None::<CString>; 3]);
    }

    #[test]
    fn validates_stride_guess() {
        let value = json!([null, "hello", null, "world"]);
        assert_eq!(
            validated_stride_guess(&value, 4),
            vec![
                None::<CString>,
                Some(CString::new("hello").unwrap()),
                None::<CString>,
                Some(CString::new("world").unwrap())
            ]
        );
    }

    #[test]
    fn too_short_stride_guess_does_not_validate() {
        let value = json!(["1", "2", "3"]);
        assert_eq!(
            validated_stride_guess(&value, 4),
            vec![None::<CString>; 4]
        );
    }

    #[test]
    fn too_long_stride_guess_does_not_validate() {
        let value = json!(["1", "2", "3", "4", "5"]);
        assert_eq!(
            validated_stride_guess(&value, 4),
            vec![None::<CString>; 4]
        );
    }

    fn make_guesses_without_stride_guess() -> Guesses {
        let mut guesses = Guesses::default();
        guesses.resize(10, 10);

        let cell = guesses.get_cell_mut(CellCoord { row: 0, column: 0 });
        cell.cell_type = IpuzCellType::IpuzCellNormal;
        cell.guess = Some(CString::new("A").unwrap());

        guesses
    }

    #[test]
    fn guesses_without_stride_guess_dont_store_it() {
        let guesses = make_guesses_without_stride_guess();
        let value = guesses.to_json_value();

        let object = value.as_object().expect("json value from guesses should be an object");
        assert!(!object.contains_key("per_row_stride_guess"));
        assert!(!object.contains_key("per_column_stride_guess"));
    }

    #[test]
    fn guesses_without_stride_guess_roundtrip() {
        let guesses = make_guesses_without_stride_guess();
        let value = guesses.to_json_value();

        let new_guesses = Guesses::new_from_value(value);
        let new_value = new_guesses.to_json_value();

        let object = new_value.as_object().expect("json value from new_guesses should be an object");
        assert!(!object.contains_key("per_row_stride_guess"));
        assert!(!object.contains_key("per_column_stride_guess"));

        assert_eq!(guesses, new_guesses);
    }

    #[test]
    fn guesses_with_stride_guess_roundtrip() {
        let mut guesses = make_guesses_without_stride_guess();

        guesses.set_column_stride_guess(Some(CString::new("5").unwrap()), 5);
        guesses.set_row_stride_guess(Some(CString::new("9").unwrap()), 9);

        let value = guesses.to_json_value();

        let new_guesses = Guesses::new_from_value(value);

        assert_eq!(guesses, new_guesses);
    }
}
