#![allow(non_snake_case)]

use glib::dgettext;
use glib::prelude::*;
use glib::subclass::prelude::*;
use glib::translate::*;
use glib_sys::gboolean;
use glib_sys::gpointer;
use libc::c_char;

use std::ffi::CStr;
use std::sync::Arc;

fn CHAR_IS_SEPARATOR(c: char) -> bool {
    c == '.' || c == '-' || c == ' ' || c == ',' || c == '\''
}

fn CHAR_IS_MODIFIER(c: char) -> bool {
    c == '*' || c == '^' || c == '+'
}

fn DELIM_IS_MODIFIER(d: IpuzDeliminator) -> bool {
    d == IpuzDeliminator::AllCaps
        || d == IpuzDeliminator::Capitalized
        || d == IpuzDeliminator::Foreign
}

#[repr(C)]
#[derive(Clone, Copy, Default, PartialEq, glib::Enum)]
#[enum_type(name = "IpuzDeliminator")]
pub enum IpuzDeliminator {
    #[default]
    WordBreak,
    Period,
    Dash,
    Apostrophe,

    /* Word Modifiers */
    AllCaps,
    Capitalized,
    Foreign,
}

#[repr(C)]
#[derive(Clone, Copy, Default, PartialEq, glib::Enum)]
#[enum_type(name = "IpuzVerbosity")]
pub enum IpuzVerbosity {
    #[default]
    Standard, /* "4,4" */
    Sparse, /* "Two words" */
}

#[derive(Clone, PartialEq)]
struct DelimSegment {
    grid_offset: u32,
    delim: IpuzDeliminator,
}

#[derive(Clone, glib::SharedBoxed)]
#[shared_boxed_type(name = "IpuzEnumeration")]
pub struct Wrapper(Arc<Enumeration>);

pub type IpuzEnumeration = <<Wrapper as SharedType>::RefCountedType as RefCounted>::InnerType;

#[derive(Default, PartialEq, Clone)]
pub struct Enumeration {
    verbosity: IpuzVerbosity,
    src: String,
    display: String,
    delims: Vec<DelimSegment>,
}

#[derive(PartialEq)]
enum DelimState {
    LookForWord,
    LookForNumber,
    LookForSeparator,
}

impl Enumeration {
    fn new(src: &str, verbosity: IpuzVerbosity) -> Self {
        let mut enumeration = Enumeration::default();
        enumeration.src = src.to_owned();
        enumeration.verbosity = verbosity;
        enumeration.create_delim_list();
        enumeration.create_display();
        enumeration
    }

    /* This parses the src string.
     *
     * We go through cycles of looking for (optionally) a modifier, then a
     * word, then a separator
     */
    fn create_delim_list(&mut self) {
        let MAX_ENUMERATION_SIZE = 999;
        let ptr = &self.src;
        let mut grid_offset = 0;
        let mut state;
        if ptr.len() == 0 {
            /* If we hit an error mid-parse it means we don't fit our
             * pattern. We bail out and just go with the src string for the
             * display string */
            self.display = self.src.clone();
            self.delims.clear();
            return;
        }
        let mut chars = ptr.chars().peekable();
        /* peek at the first character to make sure we're looking at t */
        if CHAR_IS_MODIFIER(*chars.peek().unwrap()) || chars.peek().unwrap().is_digit(10) {
            grid_offset += 1;
            state = DelimState::LookForWord;
        } else if CHAR_IS_SEPARATOR(*chars.peek().unwrap()) {
            state = DelimState::LookForSeparator
        } else {
            /* Hit an error mid-parse, bail out */
            self.display = self.src.clone();
            self.delims.clear();
            return;
        }

        loop {
            if chars.peek() == None {
                break;
            }
            let ch = *chars.peek().unwrap();
            /* Check for modifiers */
            if state == DelimState::LookForWord && CHAR_IS_MODIFIER(ch) {
                let delim = match ch {
                    '*' => IpuzDeliminator::AllCaps,
                    '^' => IpuzDeliminator::Capitalized,
                    '+' => IpuzDeliminator::Foreign,
                    _ => unreachable!(),
                };

                self.delims.push(DelimSegment { grid_offset, delim });
                state = DelimState::LookForNumber;
                chars.next();
                continue;
            }
            if (state == DelimState::LookForWord || state == DelimState::LookForNumber)
                && ch.is_digit(10)
            {
                let mut num;
                num = ch.to_digit(10).unwrap();
                chars.next();
                let mut next = chars.peek();
                while next != None && next.unwrap().is_digit(10) {
                    num *= 10;
                    num += next.unwrap().to_digit(10).unwrap();

                    chars.next();
                    next = chars.peek();
                    if num > MAX_ENUMERATION_SIZE {
                        break;
                    }
                }

                /* We limit ourselves to two digits per word */
                /* We don't handle enumerations of 0, right now. */
                if num > MAX_ENUMERATION_SIZE || num == 0 {
                    /* Hit an error mid-parse, bail out */
                    self.display = self.src.clone();
                    self.delims.clear();
                    return;
                }

                grid_offset += num * 2 - 1;
                state = DelimState::LookForSeparator;
                continue;
            }

            if state == DelimState::LookForSeparator && CHAR_IS_SEPARATOR(ch) {
                let delim = match ch {
                    ' ' | ',' => IpuzDeliminator::WordBreak,
                    '-' => IpuzDeliminator::Dash,
                    '.' => IpuzDeliminator::Period,
                    '\'' => IpuzDeliminator::Apostrophe,
                    _ => unreachable!(),
                };

                self.delims.push(DelimSegment { grid_offset, delim });
                state = DelimState::LookForWord;
                grid_offset += 1;
                chars.next();
                continue;
            }
            /* If we don't fit this pattern, we error out and just use the src */
            self.display = self.src.clone();
            self.delims.clear();
            return;
        }
        /* We are okay ending with a separator or a number. We are not okay
        ending with a modifier. So '5-' ("ACETO-") or '-5' are fine, but
        '5+' is not.
        */
        if state != DelimState::LookForNumber {
            /* If we end with a separator we need to wind back the
            grid_offset, as we advanced beyond the end */
            if state == DelimState::LookForWord {
                grid_offset -= 1;
            }
            let segment = DelimSegment {
                grid_offset,
                delim: IpuzDeliminator::WordBreak,
            };
            self.delims.push(segment);
            return;
        }

        /* Hit an error mid-parse, bail out */
        self.display = self.src.clone();
        self.delims.clear();
        return;
    }

    fn create_display(&mut self) {
        let mut grid_offset = 0;
        let mut word_modifier = false;
        let mut word_modifier_type = IpuzDeliminator::default(); /* dummy value; will get initialized later */

        /* Special case when we couldn't parse the enumeration. Just repeat it */
        if self.delims.len() == 0 {
            self.display = self.src.clone();
            return;
        }

        let mut display = String::new();

        let mut i = 0;
        while i < self.delims.len() {
            let segment = &self.delims[i];
            if DELIM_IS_MODIFIER(segment.delim) {
                word_modifier = true;
                word_modifier_type = segment.delim;
                i += 1;
                continue;
            }
            if segment.grid_offset - grid_offset > 0 {
                display.push_str(&(((segment.grid_offset - grid_offset) / 2).to_string()));
            }
            if word_modifier {
                word_modifier = false;
                match word_modifier_type {
                    IpuzDeliminator::AllCaps => {
                        display += dgettext(Some("libipuz-1.0"), " (allcaps)").as_str()
                    }
                    IpuzDeliminator::Capitalized => {
                        display += dgettext(Some("libipuz-1.0"), " (capitalized)").as_str()
                    }
                    IpuzDeliminator::Foreign => {
                        display += dgettext(Some("libipuz-1.0"), " (foreign)").as_str()
                    }
                    _ => unreachable!(),
                }
            }
            grid_offset = segment.grid_offset;
            if i != self.delims.len() - 1 {
                match segment.delim {
                    IpuzDeliminator::WordBreak => display.push(','),
                    IpuzDeliminator::Period => display.push('.'),
                    IpuzDeliminator::Dash => display.push('-'),
                    IpuzDeliminator::Apostrophe => display.push('\''),
                    _ => (),
                }
            }
            i += 1;
        }
        self.display = display;
    }
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_enumeration_get_type() -> glib::ffi::GType {
    Wrapper::static_type().into_glib()
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_enumeration_new(
    src: *const c_char,
    verbosity: IpuzVerbosity,
) -> *const IpuzEnumeration {
    ipuz_return_val_if_fail! {
        ipuz_enumeration_new => std::ptr::null();

        !src.is_null(),
    }

    let src = CStr::from_ptr(src).to_str().unwrap();
    Wrapper(Arc::new(Enumeration::new(src, verbosity))).into_refcounted().into_raw()
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_enumeration_ref(
    enumeration: *const IpuzEnumeration,
) -> *const IpuzEnumeration {
    ipuz_return_val_if_fail! {
        ipuz_enumeration_ref => std::ptr::null();

        !enumeration.is_null(),
    }

    Arc::increment_strong_count(enumeration);
    enumeration
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_enumeration_unref(
    enumeration: *const IpuzEnumeration
) {
    /* NULL is being passed to this function by C */
    if !enumeration.is_null() {
        Arc::decrement_strong_count(enumeration);
    }
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_enumeration_equal(
    enumeration1: *const IpuzEnumeration,
    enumeration2: *const IpuzEnumeration
) -> gboolean {
    ipuz_return_val_if_fail! {
        ipuz_enumeration_equal => false.into_glib();

        !enumeration1.is_null(),
        !enumeration2.is_null(),
    }

    let enumeration1 = &*enumeration1;
    let enumeration2 = &*enumeration2;
    (enumeration1.src == enumeration2.src).into_glib()
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_enumeration_get_src (
    enumeration: *const IpuzEnumeration
) -> *mut c_char {
    ipuz_return_val_if_fail! {
        ipuz_enumeration_get_src => std::ptr::null_mut();

        !enumeration.is_null(),
    }

    let enumeration = &*enumeration;
    enumeration.src.to_glib_full()
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_enumeration_get_display (
    enumeration: *const IpuzEnumeration
) -> *mut c_char {
    ipuz_return_val_if_fail! {
        ipuz_enumeration_get_display => std::ptr::null_mut();

        !enumeration.is_null(),
    }

    let enumeration = &*enumeration;
    enumeration.display.to_glib_full()
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_enumeration_get_has_delim (
    enumeration: *const IpuzEnumeration
) -> gboolean {
    ipuz_return_val_if_fail! {
        ipuz_enumeration_get_has_delim => false.into_glib();

        !enumeration.is_null(),
    }

    let enumeration = &*enumeration;
    (enumeration.delims.len() != 0).into_glib()
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_enumeration_delim_length (
    enumeration: *const IpuzEnumeration
) -> i32 {
    ipuz_return_val_if_fail! {
        ipuz_enumeration_delim_length => -1;

        !enumeration.is_null(),
    }

    let enumeration = &*enumeration;
    match enumeration.delims.len() {
        0 => -1,
        _ => {
            let segment = enumeration.delims.last().unwrap();
            i32::try_from(segment.grid_offset / 2).unwrap()
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_enumeration_foreach_delim (
    enumeration: *const IpuzEnumeration,
    func: unsafe extern "C" fn(&Enumeration, IpuzDeliminator, u32, gboolean, gpointer),
    user_data: gpointer
) {
    ipuz_return_if_fail! {
      ipuz_enumeration_foreach_delim;

      !enumeration.is_null(),
    }

    let enumeration = &*enumeration;
    let mut i = 0;
    while i < enumeration.delims.len() {
        let segment = &enumeration.delims[i];
        func(
            &enumeration,
            segment.delim,
            segment.grid_offset,
            (i == enumeration.delims.len() - 1).into_glib(),
            user_data,
        );
        i += 1;
    }
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_enumeration_valid_char(c: c_char) -> gboolean {
    let c = char::from(u8::try_from(c).unwrap());
    (CHAR_IS_SEPARATOR(c) || CHAR_IS_MODIFIER(c) || c.is_digit(10)).into_glib()
}

/* vi: set et sw=4: */
