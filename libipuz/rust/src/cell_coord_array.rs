use glib::subclass::prelude::*;
use glib::prelude::*;
use glib::translate::*;
use glib_sys::gboolean;

use std::ffi::c_uint;
use std::sync::{Arc, Mutex};

// use std::sync::Arc;

/* Keep in sync with ipuz-clue.h */
#[repr(C)]
#[derive(Copy, Clone, Debug, Default, Eq, Ord, PartialEq, PartialOrd)]
pub struct IpuzCellCoord {
    row: c_uint,
    column: c_uint,
}

#[derive(Clone, glib::SharedBoxed)]
#[shared_boxed_type(name = "IpuzCellCoordArray")]
pub struct Wrapper(Arc<Mutex<CellCoordArray>>);

// This will resolve to the Mutex that is wrapped above.
pub type IpuzCellCoordArray = <<Wrapper as SharedType>::RefCountedType as RefCounted>::InnerType;

#[derive(Clone, PartialEq, Debug)]
pub struct CellCoordArray {
    data: Vec<IpuzCellCoord>,
}

impl CellCoordArray {
    pub fn new() -> Self {
        CellCoordArray { data: Vec::new() }
    }

    pub fn append(&mut self, coord: IpuzCellCoord) {
        if !self.find(&coord) {
            self.data.push(coord);
        }
    }

    pub fn clear (&mut self) {
        self.data.clear ();
    }

    pub fn remove_coord(&mut self, coord: IpuzCellCoord) {
        if let Some(index) = self.data.iter().position(|&c| c == coord) {
            self.data.remove(index);
        }
    }

    pub fn coord_index(&mut self, coord: IpuzCellCoord) -> i32 {
        if let Some(index) = self.data.iter().position(|&c| c == coord) {
            return index as i32;
        }
        -1
    }

    pub fn find(&self, coord: &IpuzCellCoord) -> bool {
        self.data.contains(coord)
    }

    pub fn len(&self) -> usize {
        self.data.len()
    }

    pub fn index(&self, i: usize) -> Option<IpuzCellCoord> {
        self.data.get(i).cloned()
    }

    pub fn shuffle(&mut self) {
        use rand::seq::SliceRandom;
        let mut rng = rand::thread_rng();
        self.data.shuffle(&mut rng);
    }

    pub fn pop_front(&mut self) -> Option<IpuzCellCoord> {
        if !self.data.is_empty() {
            Some(self.data.remove(0))
        } else {
            None
        }
    }

    pub fn set_sorted(&mut self, sorted: bool) {
        if sorted {
            self.data.sort();
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_cell_coord_array_get_type() -> glib::ffi::GType {
    Wrapper::static_type().into_glib()
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_cell_coord_array_new() -> *const IpuzCellCoordArray {
    Wrapper(Arc::new(Mutex::new(CellCoordArray::new())))
        .into_refcounted()
        .into_raw()
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_cell_coord_array_append(
    array: *const IpuzCellCoordArray,
    coord: *const IpuzCellCoord,
) {
    ipuz_return_if_fail! {
        ipuz_cell_coord_array_append;
        !array.is_null(),
        !coord.is_null(),
    };

    let mut inner = (*array).lock().unwrap();

    inner.append(*coord);
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_cell_coord_array_clear(
    array: *mut IpuzCellCoordArray
) {
    ipuz_return_if_fail! {
        ipuz_cell_coord_array_clear;
        !array.is_null(),
    };

    let mut inner = (*array).lock().unwrap();
    inner.clear();
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_cell_coord_array_remove_coord(
    array: *mut IpuzCellCoordArray,
    coord: *const IpuzCellCoord,
) {
    ipuz_return_if_fail! {
        ipuz_cell_coord_array_remove_coord;
        !array.is_null(),
        !coord.is_null(),
    };

    let mut inner = (*array).lock().unwrap();
    inner.remove_coord(*coord);
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_cell_coord_array_coord_index(
    array: *mut IpuzCellCoordArray,
    coord: *const IpuzCellCoord,
) -> i32 {
    ipuz_return_val_if_fail! {
        ipuz_cell_coord_array_coord_index => -1;
        !array.is_null(),
    };

    if coord.is_null() {
        return -1;
    };
    let mut inner = (*array).lock().unwrap();
    inner.coord_index(*coord)
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_cell_coord_array_len(array: *const IpuzCellCoordArray) -> usize {
    ipuz_return_val_if_fail! {
        ipuz_cell_coord_array_len => 0;
        !array.is_null(),
    };

    let inner = (*array).lock().unwrap();
    inner.len()
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_cell_coord_array_index(
    array: *const IpuzCellCoordArray,
    i: usize,
    out_value: *mut IpuzCellCoord,
) -> gboolean {
    ipuz_return_val_if_fail! {
        ipuz_cell_coord_array_index => false.into_glib();
        !array.is_null(),
    };

    let inner = (*array).lock().unwrap();

    match inner.index(i) {
        Some(v) => {
            if !out_value.is_null() {
                *out_value = v;
            }
            true.into_glib()
        }

        None => {
            if !out_value.is_null() {
                *out_value = IpuzCellCoord::default();
            }
            false.into_glib()
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_cell_coord_array_print(array: *const IpuzCellCoordArray) {
    ipuz_return_if_fail! {
        ipuz_cell_coord_array_print;
        !array.is_null(),
    };

    let inner = (*array).lock().unwrap();
    println!("{:?}", inner);
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_cell_coord_array_pop_front(
    array: *const IpuzCellCoordArray,
    out_front: *mut IpuzCellCoord,
) -> gboolean {
    ipuz_return_val_if_fail! {
        ipuz_cell_coord_array_pop_front => false.into_glib();
        !array.is_null(),
    };

    let mut inner = (*array).lock().unwrap();

    match inner.pop_front() {
        Some(coord) => {
            if !out_front.is_null() {
                *out_front = coord;
            }
            true.into_glib()
        }

        None => {
            if !out_front.is_null() {
                *out_front = IpuzCellCoord::default();
            }
            false.into_glib()
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_cell_coord_array_shuffle(array: *const IpuzCellCoordArray) {
    ipuz_return_if_fail! {
        ipuz_cell_coord_array_shuffle;
        !array.is_null(),
    };

    let mut inner = (*array).lock().unwrap();
    inner.shuffle();
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_cell_coord_array_set_sorted(
    array: *const IpuzCellCoordArray,
    sorted: gboolean,
) {
    ipuz_return_if_fail! {
        ipuz_cell_coord_array_set_sorted;
        !array.is_null(),
    };

    let mut inner = (*array).lock().unwrap();
    inner.set_sorted(from_glib(sorted));
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_cell_coord_array_ref(
    array: *const IpuzCellCoordArray,
) -> *const IpuzCellCoordArray {
    ipuz_return_val_if_fail! {
        ipuz_cell_coord_array_ref => std::ptr::null();
        !array.is_null(),
    };

    <Arc<IpuzCellCoordArray> as RefCounted>::ref_(array)
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_cell_coord_array_unref(array: *const IpuzCellCoordArray) {
    if !array.is_null() {
        <Arc<IpuzCellCoordArray> as RefCounted>::from_raw(array);
    }
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_cell_coord_array_dup(
    array: *const IpuzCellCoordArray,
) -> *const IpuzCellCoordArray {
    ipuz_return_val_if_fail! {
        ipuz_cell_coord_array_ref => std::ptr::null();
        !array.is_null(),
    };

    let inner_array = &*(*array).lock().unwrap();

    Wrapper(Arc::new(Mutex::new(inner_array.clone())))
        .into_refcounted()
        .into_raw()
}


#[no_mangle]
pub unsafe extern "C" fn ipuz_cell_coord_array_equal (array1: *const IpuzCellCoordArray,
                                                      array2: *const IpuzCellCoordArray) -> gboolean {
    if array1.is_null() {
        if array2.is_null () {
            return true.into_glib();
        }
        return false.into_glib();
    } else if array2.is_null() {
        return false.into_glib();
    }

    /* avoid the potential deadlock below if arrays are the same */
    if array1 == array2 {
        return true.into_glib();
    }

    let inner_array1 = &*(*array1).lock().unwrap();
    let inner_array2 = &*(*array2).lock().unwrap();

    (*inner_array1 == *inner_array2).into_glib()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn handles_append() {
        let mut coord_array = CellCoordArray::new();
        let coord = IpuzCellCoord { row: 1, column: 1 };
        coord_array.append(coord);
        assert_eq!(coord_array.len(), 1);
    }

    #[test]
    fn handles_remove_coord() {
        let mut coord_array = CellCoordArray::new();
        let coord = IpuzCellCoord { row: 1, column: 1 };
        coord_array.append(coord);
        coord_array.remove_coord(coord);
        assert_eq!(coord_array.len(), 0);
    }

    #[test]
    fn handles_find() {
        let mut coord_array = CellCoordArray::new();
        let coord = IpuzCellCoord { row: 1, column: 1 };
        coord_array.append(coord);
        assert!(coord_array.find(&coord));
    }

    /* Not sure how to test this.
    #[test]
    fn handles_shuffle() {
        let mut coord_array = CellCoordArray::new();
        coord_array.append(IpuzCellCoord { row: 1, column: 1 });
        coord_array.append(IpuzCellCoord { row: 2, column: 2 });
        coord_array.append(IpuzCellCoord { row: 3, column: 3 });

        let mut shuffled_array = coord_array.clone();
        shuffled_array.shuffle();

        assert_ne!(coord_array.index(0), shuffled_array.index(0));
        assert_ne!(coord_array.index(1), shuffled_array.index(1));
        assert_ne!(coord_array.index(2), shuffled_array.index(2));
    }
     */
    #[test]
    fn handles_pop_front() {
        let mut coord_array = CellCoordArray::new();
        coord_array.append(IpuzCellCoord { row: 1, column: 1 });
        coord_array.append(IpuzCellCoord { row: 2, column: 2 });
        coord_array.append(IpuzCellCoord { row: 3, column: 3 });

        let popped_coord = coord_array.pop_front();
        assert_eq!(popped_coord, Some(IpuzCellCoord { row: 1, column: 1 }));
        assert_eq!(coord_array.len(), 2);
    }
}
