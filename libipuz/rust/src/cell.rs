use crate::ffi::grefcount;
use std::ffi::{c_char, c_int};

#[repr(C)]
#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub enum IpuzCellType {
    #[default]
    IpuzCellNormal, /* Regular cell */
    IpuzCellBlock,  /* Blocked out cell */
    IpuzCellNull,   /* omitted cell */
}

/* Keep in sync with libipuz/ipuz-cell.h */
#[repr(C)]
pub struct IpuzCell {
    ref_count: grefcount,

    pub cell_type: IpuzCellType,
    number: c_int,
    label: *mut c_char,
    solution: *mut c_char,
    pub initial_val: *mut c_char,

    /* This is the solved guesses loaded from file and to be saved
    * within the .ipuz file. It shouldn't be used to track the
    * transient state of a puzzle being solved; use IpuzGuesses instead
    * for that. */
    pub saved_guess: *mut c_char,

    //style: *const IpuzStyle,
    
    /* do not free */
    //clues: Vec<IpuzClue>,

    /* Private */
    //style_name: String,
}
