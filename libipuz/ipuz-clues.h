/* ipuz-clues.h
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <libipuz/ipuz-clue.h>
#include <libipuz/ipuz-puzzle.h>

G_BEGIN_DECLS


#define IPUZ_TYPE_CLUES (ipuz_clues_get_type())
G_DECLARE_INTERFACE (IpuzClues, ipuz_clues, IPUZ, CLUES, IpuzPuzzle);


/**
 * IpuzCluesForeachClueFunc:
 * @clues: The iterated [iface@Ipuz.Clues]
 * @direction: The direction of @clue
 * @clue: The next clue in @xword
 * @clue_id: The #IpuzClueId of @clue
 * @user_data: data passed to the function
 *
 * The function to be passed to [method@Ipuz.Clues.foreach_clue].
 *
 * You should not change the number of clues in @xword from within
 * this function. It is safe to change properties of @clue.
 */
typedef void (*IpuzCluesForeachClueFunc) (IpuzClues         *clues,
                                          IpuzClueDirection  direction,
                                          IpuzClue          *clue,
                                          IpuzClueId        *clue_id,
                                          gpointer           user_data);

struct _IpuzCluesInterface
{
  GTypeInterface g_iface;

  IpuzClueDirection  (* add_clue_set)           (IpuzClues                *clues,
                                                 IpuzClueDirection         direction,
                                                 const gchar              *label);
  void               (* remove_clue_set)        (IpuzClues                *clues,
                                                 IpuzClueDirection         direction);
  void               (* clear_clue_sets)        (IpuzClues                *clues);
  guint              (* get_n_clue_sets)        (IpuzClues                *clues);
  IpuzClueDirection  (* clue_set_get_dir)       (IpuzClues                *clues,
                                                 guint                     index);
  const gchar       *(* clue_set_get_label)     (IpuzClues                *clues,
                                                 IpuzClueDirection         direction);
  GArray            *(* get_clues)              (IpuzClues                *clues,
                                                 IpuzClueDirection         direction);
  void               (* foreach_clue)           (IpuzClues                *clues,
                                                 IpuzCluesForeachClueFunc  func,
                                                 gpointer                  user_data);
  guint              (* get_n_clues)            (IpuzClues                *clues,
                                                 IpuzClueDirection         direction);
  IpuzClue          *(* get_clue_by_id)         (IpuzClues                *clues,
                                                 IpuzClueId               *clue_id);
  void               (* remove_clue)            (IpuzClues                *clues,
                                                 IpuzClue                 *clue);
  gboolean           (* get_id_by_clue)         (IpuzClues                *clues,
                                                 const IpuzClue           *clue,
                                                 IpuzClueId               *clue_id);
  gchar             *(* get_clue_string_by_id)  (IpuzClues                *clues,
                                                 IpuzClueId               *clue_id);
  gchar             *(* get_guess_string_by_id) (IpuzClues                *clues,
                                                 IpuzClueId               *clue_id);
  gboolean           (* clue_guessed)           (IpuzClues                *clues,
                                                 IpuzClue                 *clue,
                                                 gboolean                 *correct);
  IpuzClue          *(* find_clue_by_number)    (IpuzClues                *clues,
                                                 IpuzClueDirection         direction,
                                                 gint                      number);
  IpuzClue          *(* find_clue_by_label)     (IpuzClues                *clues,
                                                 IpuzClueDirection         direction,
                                                 const char               *label);
  IpuzClue          *(* find_clue_by_coord)     (IpuzClues                *clues,
                                                 IpuzClueDirection         direction,
                                                 const IpuzCellCoord      *coord);
};


/* Clue Sets */
IpuzClueDirection  ipuz_clues_add_clue_set           (IpuzClues                *clues,
                                                      IpuzClueDirection         direction,
                                                      const gchar              *label);
void               ipuz_clues_remove_clue_set        (IpuzClues                *clues,
                                                      IpuzClueDirection         direction);
void               ipuz_clues_clear_clue_sets        (IpuzClues                *clues);
guint              ipuz_clues_get_n_clue_sets        (IpuzClues                *clues);
IpuzClueDirection  ipuz_clues_clue_set_get_dir       (IpuzClues                *clues,
                                                      guint                     index);
const gchar       *ipuz_clues_clue_set_get_label     (IpuzClues                *clues,
                                                      IpuzClueDirection         direction);


/* Clues */
void               ipuz_clues_remove_clue            (IpuzClues                *clues,
                                                      IpuzClue                 *clue);
GArray            *ipuz_clues_get_clues              (IpuzClues                *clues,
                                                      IpuzClueDirection         direction);
void               ipuz_clues_foreach_clue           (IpuzClues                *clues,
                                                      IpuzCluesForeachClueFunc  func,
                                                      gpointer                  user_data);
guint              ipuz_clues_get_n_clues            (IpuzClues                *clues,
                                                      IpuzClueDirection         direction);
IpuzClue          *ipuz_clues_get_clue_by_id         (IpuzClues                *clues,
                                                      IpuzClueId               *clue_id);
gboolean           ipuz_clues_get_id_by_clue         (IpuzClues                *clues,
                                                      const IpuzClue           *clue,
                                                      IpuzClueId               *clue_id);
gchar             *ipuz_clues_get_clue_string_by_id  (IpuzClues                *clues,
                                                      IpuzClueId               *clue_id);
gchar             *ipuz_clues_get_guess_string_by_id (IpuzClues                *clues,
                                                      IpuzClueId               *clue_id);
gboolean           ipuz_clues_clue_guessed           (IpuzClues                *clues,
                                                      IpuzClue                 *clue,
                                                      gboolean                 *correct);
IpuzClue          *ipuz_clues_find_clue_by_number    (IpuzClues                *clues,
                                                      IpuzClueDirection         direction,
                                                      gint                      number);
IpuzClue          *ipuz_clues_find_clue_by_label     (IpuzClues                *clues,
                                                      IpuzClueDirection         direction,
                                                      const char               *label);
IpuzClue          *ipuz_clues_find_clue_by_coord     (IpuzClues                *clues,
                                                      IpuzClueDirection         direction,
                                                      const IpuzCellCoord      *coord);


G_END_DECLS
