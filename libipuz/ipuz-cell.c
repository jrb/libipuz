/* ipuz-cell.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */


#include "ipuz-cell.h"


struct _IpuzCell
{
  grefcount ref_count;

  IpuzCellType cell_type;
  gint number;
  gchar *label;
  gchar *solution;
  gchar *initial_val;

  /* This is the solved guesses loaded from file and to be saved
   * within the .ipuz file. It shouldn't be used to track the
   * transient state of a puzzle being solved; use IpuzGuesses instead
   * for that. */
  gchar *saved_guess;
  IpuzStyle *style;

  /* do not free */
  GArray *clues;

  /* Private */
  gchar *style_name;
};


G_DEFINE_BOXED_TYPE (IpuzCell, ipuz_cell, ipuz_cell_ref, ipuz_cell_unref);


IpuzCell *
ipuz_cell_ref (IpuzCell *cell)
{
  g_return_val_if_fail (cell != NULL, NULL);

  g_ref_count_inc (&cell->ref_count);

  return cell;
}

void
ipuz_cell_unref (IpuzCell *cell)
{
  g_return_if_fail (cell != NULL);

  if (!g_ref_count_dec (&cell->ref_count))
    return;

  /* Free data */
  g_free (cell->label);
  g_free (cell->solution);
  g_free (cell->saved_guess);
  g_free (cell->initial_val);
  g_free (cell->style_name);
  g_clear_pointer (&cell->style, ipuz_style_unref);
  g_clear_pointer (&cell->clues, g_array_unref);

  g_free (cell);
}

gboolean
ipuz_cell_equal (const IpuzCell *cell1,
                 const IpuzCell *cell2)
{
  if (cell1 == NULL)
    return (cell1 != cell2);
  else if (cell2 == NULL)
    return FALSE;

  return (cell1->cell_type == cell2->cell_type
          && cell1->number == cell2->number
          && g_strcmp0 (cell1->label, cell2->label) == 0
          && g_strcmp0 (cell1->solution, cell2->solution) == 0
          && g_strcmp0 (cell1->saved_guess, cell2->saved_guess) == 0
          && g_strcmp0 (cell1->initial_val, cell2->initial_val) == 0
          && g_strcmp0 (cell1->style_name, cell2->style_name) == 0);
}

void
ipuz_cell_build (IpuzCell    *cell,
                 JsonBuilder *builder,
                 gboolean     solution,
                 const char  *block,
                 const char  *empty)
{
  g_return_if_fail (cell != NULL);

  if (IPUZ_CELL_IS_NULL (cell))
    {
      json_builder_add_null_value (builder);
      return;
    }

  /* We're printing out the "solution" block */
  if (solution)
    {
      if (cell->solution != NULL)
        json_builder_add_string_value (builder, cell->solution);
      else
        json_builder_add_null_value (builder);

      return;
    }

  /* Short-circuit printing out the cell as an object */
  if (cell->style == NULL &&
      cell->initial_val == NULL)
    {
      if (IPUZ_CELL_IS_BLOCK (cell))
        json_builder_add_string_value (builder, block);
      else if (cell->label)
        json_builder_add_string_value (builder, cell->label);
      else
        json_builder_add_int_value (builder, cell->number);
    }
  else /* Print a more complex cell */
    {
      json_builder_begin_object (builder);
      json_builder_set_member_name (builder, "cell");
      if (IPUZ_CELL_IS_BLOCK (cell))
        json_builder_add_string_value (builder, block);
      else if (cell->label)
        json_builder_add_string_value (builder, cell->label);
      else
        json_builder_add_int_value (builder, cell->number);

      if (cell->style)
        {
          json_builder_set_member_name (builder, "style");
          /* If it's a named style, we put a string with the name to
           * refer to the global styles. Otherwise, we do a full copy
           * of the style. */
          if (cell->style_name)
            json_builder_add_string_value (builder, cell->style_name);
          else
            ipuz_style_build (cell->style, builder);
        }

      if (cell->initial_val)
        {
          json_builder_set_member_name (builder, "value");
          json_builder_add_string_value (builder, cell->initial_val);
        }

      json_builder_end_object (builder);
    }
}

IpuzCellType
ipuz_cell_get_cell_type (IpuzCell *cell)
{
  g_return_val_if_fail (cell != NULL, IPUZ_CELL_NORMAL);

  return cell->cell_type;
}

void
ipuz_cell_set_cell_type (IpuzCell     *cell,
                         IpuzCellType  cell_type)
{
  g_return_if_fail (cell != NULL);

  if (cell->cell_type == cell_type)
    return;

  cell->cell_type = cell_type;
  if (cell->cell_type != IPUZ_CELL_NORMAL)
    {
      cell->number = 0;
      g_clear_pointer (&cell->label, g_free);
      g_clear_pointer (&cell->solution, g_free);
      g_clear_pointer (&cell->saved_guess, g_free);
      g_clear_pointer (&cell->initial_val, g_free);
    }

  if (cell->cell_type == IPUZ_CELL_NULL)
    {
      g_clear_pointer (&cell->style, ipuz_style_unref);
      g_clear_pointer (&cell->style_name, g_free);
    }
}

gint
ipuz_cell_get_number (IpuzCell *cell)
{
  g_return_val_if_fail (cell != NULL, 0);

  return cell->number;
}

void
ipuz_cell_set_number (IpuzCell *cell,
                      gint      number)
{
  g_return_if_fail (cell != NULL);

  cell->cell_type = IPUZ_CELL_NORMAL;
  cell->number = number;
}

const gchar *
ipuz_cell_get_label (IpuzCell *cell)
{
  g_return_val_if_fail (cell != NULL, NULL);

  return cell->label;
}

void
ipuz_cell_set_label (IpuzCell *cell,
                     const gchar *label)
{
  g_return_if_fail (cell != NULL);

  g_free (cell->label);

  cell->cell_type = IPUZ_CELL_NORMAL;
  cell->label = g_strdup (label);
}

const gchar *
ipuz_cell_get_solution (IpuzCell *cell)
{
  g_return_val_if_fail (cell != NULL, NULL);

  return cell->solution;
}

void
ipuz_cell_set_solution (IpuzCell *cell,
                        const gchar *solution)
{
  g_return_if_fail (cell != NULL);

  g_free (cell->solution);

  cell->cell_type = IPUZ_CELL_NORMAL;
  cell->solution = g_strdup (solution);
}

const gchar *
ipuz_cell_get_saved_guess (IpuzCell *cell)
{
  g_return_val_if_fail (cell != NULL, NULL);

  return cell->saved_guess;
}

void
ipuz_cell_set_saved_guess (IpuzCell    *cell,
                           const gchar *saved_guess)
{
  g_return_if_fail (cell != NULL);

  g_free (cell->saved_guess);

  cell->cell_type = IPUZ_CELL_NORMAL;
  cell->saved_guess = g_strdup (saved_guess);
}

const gchar *
ipuz_cell_get_initial_val (IpuzCell *cell)
{
  g_return_val_if_fail (cell != NULL, NULL);

  return cell->initial_val;
}

void
ipuz_cell_set_initial_val (IpuzCell *cell,
                           const gchar *initial_val)
{
  g_return_if_fail (cell != NULL);

  g_free (cell->initial_val);

  cell->initial_val = g_strdup (initial_val);
}

IpuzStyle *
ipuz_cell_get_style (IpuzCell *cell)
{
  g_return_val_if_fail (cell != NULL, NULL);

  return cell->style;
}

void
ipuz_cell_set_style (IpuzCell    *cell,
                     IpuzStyle   *style,
                     const gchar *style_name)
{
  gchar *new_style_name = NULL;

  g_return_if_fail (cell != NULL);

  if (style)
    {
      ipuz_style_ref (style);
      new_style_name = g_strdup (style_name);
    }

  g_clear_pointer (&cell->style, ipuz_style_unref);
  g_clear_pointer (&cell->style_name, g_free);

  cell->style = style;
  cell->style_name = new_style_name;
}

const gchar *
ipuz_cell_get_style_name (IpuzCell *cell)
{
  g_return_val_if_fail (cell != NULL, NULL);

  return cell->style_name;
}

void
ipuz_cell_set_style_name (IpuzCell    *cell,
                          const gchar *style_name)
{
  gchar *new_style_name;

  g_return_if_fail (cell != NULL);

  new_style_name = g_strdup (style_name);

  g_clear_pointer (&cell->style_name, g_free);

  cell->style_name = new_style_name;
}


void
ipuz_cell_set_clue (IpuzCell *cell,
                    IpuzClue *clue)
{
  g_return_if_fail (cell != NULL);
  g_return_if_fail (clue != NULL);

  if (cell->clues == NULL)
    cell->clues = g_array_new (FALSE, TRUE, sizeof (IpuzClue *));

  for (guint i = 0; i < cell->clues->len; i++)
    {
      IpuzClue *old_clue = g_array_index (cell->clues, IpuzClue *, i);
      if (ipuz_clue_get_direction (old_clue) == ipuz_clue_get_direction (clue))
        {
	  g_array_remove_index_fast (cell->clues, i);
	  break;
	}
    }

  g_array_append_val (cell->clues, clue);
}

void
ipuz_cell_clear_clues (IpuzCell *cell)
{
  g_return_if_fail (cell != NULL);

  if (cell->clues)
    g_array_set_size (cell->clues, 0);
}

void
ipuz_cell_clear_clue_direction (IpuzCell          *cell,
                                IpuzClueDirection  direction)
{
  g_return_if_fail (cell != NULL);

  if (cell->clues == NULL)
    return;

  for (guint i = 0; i < cell->clues->len; i++)
    {
      IpuzClue *clue = g_array_index (cell->clues, IpuzClue *, i);
      g_assert (clue != NULL);

      if (ipuz_clue_get_direction (clue) == direction)
        {
          g_array_remove_index_fast (cell->clues, i);
          return;
        }
    }
}

const IpuzClue *
ipuz_cell_get_clue (IpuzCell          *cell,
                    IpuzClueDirection  direction)
{
  g_return_val_if_fail (cell != NULL, NULL);

  if (cell->clues)
    {
      for (guint i = 0; i < cell->clues->len; i++)
        {
          IpuzClue *clue = g_array_index (cell->clues, IpuzClue *, i);
          g_assert (clue != NULL);

          if (ipuz_clue_get_direction (clue) == direction)
            return clue;
        }
    }

  return NULL;
}


/* Basically, every node type is a valid puzzle cell, but not every value is
 * valid.
 * Some notes:
 * Null -> NULL cell type
 * Number > 0 -> number
 * Number == 0 and empty = "0" -> empty
 * string == empty -> empty
 * string == block -> block
 * string -> label, otherwise
 * Object -> "it's complicated"
 */
static void
ipuz_cell_parse_puzzle_value (IpuzCell       *cell,
                              JsonNode       *node,
                              IpuzPuzzleKind  kind,
                              const gchar    *block,
                              const gchar    *empty)
{
  GType value_type = json_node_get_value_type (node);

  if (value_type == G_TYPE_INT64)
    {
      int number = json_node_get_int (node);

      ipuz_cell_set_cell_type (cell, IPUZ_CELL_NORMAL);

      if ((number == 0) && (g_strcmp0 (empty, "0") == 0))
        return;

      ipuz_cell_set_number (cell, number);
      }
  else if (value_type == G_TYPE_STRING)
    {
      const gchar *str = json_node_get_string (node);
      if (g_strcmp0 (str, empty) == 0)
        ipuz_cell_set_cell_type (cell, IPUZ_CELL_NORMAL);
      else if (g_strcmp0 (str, block) == 0)
        ipuz_cell_set_cell_type (cell, IPUZ_CELL_BLOCK);
      else
        {
          if (kind == IPUZ_PUZZLE_NONOGRAM ||
              kind == IPUZ_PUZZLE_NONOGRAM_COLOR)
            {
              /* FIXME(nonogram): ipuz_cell_set_solution/cell_type
               * will change the cell type to NORMAL or clear the
               * solution. We don't want that for nonograms. I'll
               * remove those checks, but need to go through and see
               * if anything will break. */
              ipuz_cell_set_solution (cell, str);
              cell->cell_type = IPUZ_CELL_BLOCK;
            }
          else
            {
              ipuz_cell_set_cell_type (cell, IPUZ_CELL_NORMAL);
              ipuz_cell_set_label (cell, str);
            }
        }
     }
  else
    {
      /* Not sure what to do with booleans, floats, etc */
    }
}

void
ipuz_cell_parse_puzzle (IpuzCell       *cell,
                        JsonNode       *node,
                        IpuzPuzzleKind  kind,
                        const gchar    *block,
                        const gchar    *empty)
{
  JsonNodeType node_type;

  g_return_if_fail (cell != NULL);
  g_return_if_fail (node != NULL);

  node_type = json_node_get_node_type (node);

  if (node_type == JSON_NODE_NULL)
    {
      ipuz_cell_set_cell_type (cell, IPUZ_CELL_NULL);
      return;
    }
  else if (node_type == JSON_NODE_VALUE)
    {
      ipuz_cell_parse_puzzle_value (cell, node, kind, block, empty);
      return;

    }
  else if (node_type == JSON_NODE_OBJECT)
    {
      JsonObject *obj;
      JsonNode *element;

      obj = json_node_get_object (node);

      element = json_object_get_member (obj, "cell");
      if (element)
        ipuz_cell_parse_puzzle_value (cell, element, kind, block, empty);

      element = json_object_get_member (obj, "style");
      if (element)
        {
          if (JSON_NODE_HOLDS_VALUE (element))
            cell->style_name = g_strdup (json_node_get_string (element));
          else if (JSON_NODE_HOLDS_OBJECT (element))
            cell->style = ipuz_style_new_from_json (element);
        }

      element = json_object_get_member (obj, "value");
      if (element)
        {
          const char *initial_value = json_node_get_string (element);
          ipuz_cell_set_initial_val (cell, initial_value);
        }
    }
  /* We don't do anything with arrays. */
}

/* Validate the solution is in the charset, if it exists. Note, the
 * solution could be a rebus in which case we need to go through all
 * the characters in solution. Also, the charset can be null, in which
 * case we allow all solutions to be valid
 */
static gboolean
check_solution_in_charset (const gchar *solution,
                           const gchar *charset)
{
  const gchar *ptr = solution;

  /* Fail open */
  if (solution == NULL || charset == NULL)
    return TRUE;

  while (*ptr)
    {
      if (g_utf8_strchr (solution, -1, g_utf8_get_char (ptr)) == NULL)
        return FALSE;
      ptr = g_utf8_next_char (ptr);
    }
  return TRUE;
}
/*
 * Ignores all BLOCK / NULL characters. The shape of the board is set
 * by the "puzzle" element.
 */
void
ipuz_cell_parse_solution (IpuzCell       *cell,
                          JsonNode       *node,
                          IpuzPuzzleKind  kind,
                          const gchar    *block,
                          const gchar    *charset)
{
  JsonNodeType node_type;
  const gchar *solution = NULL;

  g_return_if_fail (cell != NULL);
  g_return_if_fail (node != NULL);

  node_type = json_node_get_node_type (node);

  if (node_type == JSON_NODE_NULL)
    {
      return;
    }
  else if (node_type == JSON_NODE_VALUE)
    {
      solution = json_node_get_string (node);

      if (! check_solution_in_charset (solution, charset))
        return;

      if (g_strcmp0 (solution, block) == 0)
        return;

      ipuz_cell_set_solution (cell, solution);
    }
  else if (node_type == JSON_NODE_OBJECT)
    {
      g_autoptr(JsonReader) reader = json_reader_new (node);

      if (json_reader_read_member (reader, "value"))
        {
          solution = json_reader_get_string_value (reader);

          if (charset != NULL && strstr (charset, solution) == NULL)
            return;
          ipuz_cell_set_solution (cell, solution);
        }
      json_reader_end_member (reader);

      /* Some puzzles in the wild have a "cell" attribute that links it back
       * to the cell. I don't think we need to parse this though, and its
       * not in the spec.
       */
    }
  else
    {
      /* Arrays aren't valid solution values AFAICT */
    }
}


/* Private Functions */
IpuzCell *
_ipuz_cell_new (void)
{
  IpuzCell *cell = g_new0 (IpuzCell, 1);

  g_ref_count_init (&cell->ref_count);
  cell->cell_type = IPUZ_CELL_NORMAL;

  return cell;
}

/*
 * Private function to return an array of all the clues. This is
 * because we don't really need an iterator API for IpuzCell just to
 * clone it.
 */
GArray *
_ipuz_cell_get_clues (IpuzCell *cell)
{
  g_assert (cell != NULL);

  return cell->clues;
}
