/* ipuz-cryptic.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */


#include "libipuz-config.h"
#include "ipuz-cryptic.h"
#include "ipuz-magic.h"

static void                ipuz_cryptic_init         (IpuzCryptic      *self);
static void                ipuz_cryptic_class_init   (IpuzCrypticClass *klass);
static const gchar *const *ipuz_cryptic_get_kind_str (IpuzPuzzle       *puzzle);


G_DEFINE_TYPE (IpuzCryptic, ipuz_cryptic, IPUZ_TYPE_CROSSWORD);


static void
ipuz_cryptic_init (IpuzCryptic *self)
{
  /* Pass */
}

static void
ipuz_cryptic_class_init (IpuzCrypticClass *klass)
{
  IpuzPuzzleClass *puzzle_class = IPUZ_PUZZLE_CLASS (klass);

  puzzle_class->get_kind_str = ipuz_cryptic_get_kind_str;
}

static const gchar *const *
ipuz_cryptic_get_kind_str (IpuzPuzzle *puzzle)
{
  static const char *kind_str[] =
    {
      "http://ipuz.org/crossword#1",
      "http://ipuz.org/crossword/crypticcrossword#1",
      NULL
    };

  return kind_str;
}

/*
 * Public Methods
 */

IpuzPuzzle *
ipuz_cryptic_new (void)
{
  IpuzPuzzle *cryptic;

  cryptic = g_object_new (IPUZ_TYPE_CRYPTIC,
                          "version", _IPUZ_VERSION_2,
                          "show-enumerations", TRUE,
                          NULL);

  return cryptic;
}
