/* ipuz-acrostic.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */


#include "libipuz-config.h"
#include <libipuz/ipuz-acrostic.h>
#include <libipuz/ipuz-charset.h>
#include <libipuz/ipuz-cell-coord-array.h>
#include <libipuz/ipuz-clue-sets.h>
#include <libipuz/ipuz-clues.h>
#include "ipuz-private.h"
#include "acrostic-board-dimensions.h"
#include "ipuz-magic.h"

enum
{
  PROP_0,
  PROP_QUOTE,
  PROP_SOURCE,
  N_PROPS
};

static GParamSpec *obj_props[N_PROPS] = { NULL, };

typedef struct _IpuzAcrosticPrivate
{
  gchar *quote;
  gchar *normalized_quote;
  gchar *source;
  gchar *normalized_source;
  IpuzClue *quote_clue;
} IpuzAcrosticPrivate;


static void                ipuz_acrostic_init         (IpuzAcrostic      *self);
static void                ipuz_acrostic_class_init   (IpuzAcrosticClass *klass);
static void                ipuz_acrostic_set_property (GObject           *object,
                                                       guint              prop_id,
                                                       const GValue      *value,
                                                       GParamSpec        *pspec);
static void                ipuz_acrostic_get_property (GObject           *object,
                                                       guint              prop_id,
                                                       GValue            *value,
                                                       GParamSpec        *pspec);
static void                ipuz_acrostic_load_node    (IpuzPuzzle        *puzzle,
                                                       const char        *member_name,
                                                       JsonNode          *node);
static void                ipuz_acrostic_finalize     (GObject           *object);
static void                ipuz_acrostic_clone        (IpuzPuzzle        *src,
                                                       IpuzPuzzle        *dest);
static gboolean            ipuz_acrostic_equal        (IpuzPuzzle        *puzzle_a,
                                                       IpuzPuzzle        *puzzle_b);
static void                ipuz_acrostic_fixup        (IpuzPuzzle        *puzzle);
static void                ipuz_acrostic_fix_clues    (IpuzCrossword     *puzzle);
static void                ipuz_acrostic_build        (IpuzPuzzle        *puzzle,
                                                       JsonBuilder       *builder);
static void                ipuz_acrostic_real_fix_all (IpuzCrossword     *self,
                                                       const gchar       *first_attribute_name,
                                                       va_list            var_args);
static const gchar *const *ipuz_acrostic_get_kind_str (IpuzPuzzle        *puzzle);


G_DEFINE_TYPE_WITH_CODE (IpuzAcrostic, ipuz_acrostic, IPUZ_TYPE_CROSSWORD,
                         G_ADD_PRIVATE (IpuzAcrostic));


static void
ipuz_acrostic_init (IpuzAcrostic *self)
{
  /* Pass */
}

static void
ipuz_acrostic_class_init (IpuzAcrosticClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  IpuzPuzzleClass *puzzle_class = IPUZ_PUZZLE_CLASS (klass);
  IpuzCrosswordClass *crossword_class = IPUZ_CROSSWORD_CLASS (klass);

  object_class->set_property = ipuz_acrostic_set_property;
  object_class->get_property = ipuz_acrostic_get_property;
  object_class->finalize = ipuz_acrostic_finalize;
  puzzle_class->load_node = ipuz_acrostic_load_node;
  puzzle_class->clone = ipuz_acrostic_clone;
  puzzle_class->equal = ipuz_acrostic_equal;
  puzzle_class->fixup = ipuz_acrostic_fixup;
  puzzle_class->build = ipuz_acrostic_build;
  puzzle_class->get_kind_str = ipuz_acrostic_get_kind_str;
  crossword_class->fix_all_valist = ipuz_acrostic_real_fix_all;
  crossword_class->fix_clues = ipuz_acrostic_fix_clues;

  /**
   * IpuzAcrostic:quote: (attributes org.gtk.Property.get=ipuz_acrostic_get_quote org.gtk.Property.set=ipuz_acrostic_set_quote)
   *
   * Human readable string representing the quote of the puzzle.
   **/
  obj_props[PROP_QUOTE] = g_param_spec_string ("quote",
                                               "Quote",
                                               "Human readable string representing the quote of the puzzle",
                                               NULL,
                                               G_PARAM_READWRITE);

  /**
   * IpuzAcrostic:source: (attributes org.gtk.Property.get=ipuz_acrostic_get_source org.gtk.Property.set=ipuz_acrostic_set_source)
   *
   * Human readable source representing the author and/or title of the quote.
   **/
  obj_props[PROP_SOURCE] = g_param_spec_string ("source",
                                                "Source",
                                                "Human readable source representing the author and/or title of the quote",
                                                NULL,
                                                G_PARAM_READWRITE);

  g_object_class_install_properties (object_class, N_PROPS, obj_props);
}

static void
ipuz_acrostic_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  g_return_if_fail (object != NULL);

  switch (prop_id)
    {
    case PROP_QUOTE:
      ipuz_acrostic_set_quote (IPUZ_ACROSTIC (object), g_value_get_string (value));
      break;
    case PROP_SOURCE:
      ipuz_acrostic_set_source (IPUZ_ACROSTIC (object), g_value_get_string (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
ipuz_acrostic_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  IpuzAcrosticPrivate *priv;

  g_return_if_fail (object != NULL);

  priv = ipuz_acrostic_get_instance_private (IPUZ_ACROSTIC (object));

  switch (prop_id)
    {
    case PROP_QUOTE:
      g_value_set_string (value, priv->quote);
      break;
    case PROP_SOURCE:
      g_value_set_string (value, priv->source);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
ipuz_acrostic_load_node (IpuzPuzzle *puzzle,
                         const char *member_name,
                         JsonNode   *node)
{
  GValue value = G_VALUE_INIT;

  g_return_if_fail (member_name != NULL);
  g_return_if_fail (node != NULL);

  if (strcmp (member_name, _IPUZ_X_ACROSTIC_SOURCE_TAG) == 0)
    {
      json_node_get_value (node, &value);
      g_object_set_property (G_OBJECT (puzzle), "source", &value);
      g_value_unset (&value);
      return;
    }

  IPUZ_PUZZLE_CLASS (ipuz_acrostic_parent_class)->load_node (puzzle, member_name, node);
}

static void
ipuz_acrostic_finalize (GObject *object)
{
  IpuzAcrosticPrivate *priv;

  g_return_if_fail (object != NULL);

  priv = ipuz_acrostic_get_instance_private (IPUZ_ACROSTIC (object));

  g_clear_pointer (&priv->quote, g_free);
  g_clear_pointer (&priv->source, g_free);
  g_clear_pointer (&priv->normalized_quote, g_free);
  g_clear_pointer (&priv->normalized_source, g_free);
  g_clear_pointer (&priv->quote_clue, ipuz_clue_unref);

  G_OBJECT_CLASS (ipuz_acrostic_parent_class)->finalize (object);
}

static void
ipuz_acrostic_clone (IpuzPuzzle *src,
                     IpuzPuzzle *dest)
{
  IpuzAcrosticPrivate *src_priv, *dest_priv;

  g_assert (src != NULL);
  g_assert (dest != NULL);

  src_priv = ipuz_acrostic_get_instance_private (IPUZ_ACROSTIC (src));
  dest_priv = ipuz_acrostic_get_instance_private (IPUZ_ACROSTIC (dest));

  IPUZ_PUZZLE_CLASS (ipuz_acrostic_parent_class)->clone (src, dest);

  dest_priv->quote_clue = ipuz_clue_dup (src_priv->quote_clue);
  dest_priv->quote = g_strdup (src_priv->quote);
  dest_priv->normalized_quote = g_strdup (src_priv->normalized_quote);
  dest_priv->source = g_strdup (src_priv->source);
  dest_priv->normalized_source = g_strdup (src_priv->normalized_source);
}

static IpuzClue *
calculate_quote_clue (IpuzAcrostic *self)
{
  IpuzCrossword *xword = IPUZ_CROSSWORD (self);
  IpuzClue *quote_clue = ipuz_clue_new ();

  guint rows = ipuz_grid_get_height (IPUZ_GRID (xword));
  guint columns = ipuz_grid_get_width (IPUZ_GRID (xword));
  guint row, column;

  for (row = 0; row < rows; row++)
    {
      for (column = 0; column < columns; column++)
        {
          IpuzCell *cell;
          IpuzCellCoord coord = {
            .row = row,
            .column = column,
          };

          cell = ipuz_grid_get_cell (IPUZ_GRID (xword), &coord);

          if (IPUZ_CELL_IS_GUESSABLE (cell))
            {
              ipuz_clue_append_coord (quote_clue, &coord);
            }
        }
    }

  return quote_clue;
}

/* look through the puzzle for a clue labeled "[QUOTE]" */
static IpuzClue *
find_quote_clue_in_clues (IpuzAcrostic *self)
{
  for (guint n = 0; n < ipuz_clues_get_n_clue_sets (IPUZ_CLUES (self)); n++)
    {
      GArray *clues;

      clues = ipuz_clues_get_clues (IPUZ_CLUES (self),
                                    ipuz_clues_clue_set_get_dir (IPUZ_CLUES (self), n));
      g_assert (clues);
      for (guint i = 0; i < clues->len; i++)
        {
          IpuzClue *clue = g_array_index (clues, IpuzClue *, i);
          if (g_strcmp0 (ipuz_clue_get_clue_text (clue), _IPUZ_ACROSTIC_QUOTE_STR) == 0)
            {
              IpuzClue *quote_clue;
              quote_clue = ipuz_clue_dup (clue);
              ipuz_clues_remove_clue (IPUZ_CLUES (self), clue);

              ipuz_clue_set_direction (quote_clue, IPUZ_CLUE_DIRECTION_NONE);
              ipuz_clue_set_clue_text (quote_clue, NULL);
              return quote_clue;
            }
        }
    }

  return NULL;
}

static void
fix_quote_clue (IpuzAcrostic *self)
{
  IpuzAcrosticPrivate *priv;

  priv = ipuz_acrostic_get_instance_private (self);

  /* Defer to the puzzle-provided quote, and fallback if it's not
   * there */
  priv->quote_clue = find_quote_clue_in_clues (self);

  if (priv->quote_clue == NULL)
    priv->quote_clue = calculate_quote_clue (self);
}

static gboolean
ipuz_acrostic_equal (IpuzPuzzle *puzzle_a,
                     IpuzPuzzle *puzzle_b)
{
  IpuzAcrosticPrivate *priv_a, *priv_b;

  g_return_val_if_fail (IPUZ_IS_ACROSTIC (puzzle_b), FALSE);

  priv_a = ipuz_acrostic_get_instance_private (IPUZ_ACROSTIC (puzzle_a));
  priv_b = ipuz_acrostic_get_instance_private (IPUZ_ACROSTIC (puzzle_b));

  if (! ipuz_clue_equal (priv_a->quote_clue, priv_b->quote_clue))
    return FALSE;

  if (g_strcmp0 (priv_a->quote, priv_b->quote) ||
      g_strcmp0 (priv_a->source, priv_b->source))
    return FALSE;

  return IPUZ_PUZZLE_CLASS (ipuz_acrostic_parent_class)->equal (puzzle_a,
                                                                puzzle_b);
}

static void
ipuz_acrostic_fixup (IpuzPuzzle *puzzle)
{
  IPUZ_PUZZLE_CLASS (ipuz_acrostic_parent_class)->fixup (puzzle);

  fix_quote_clue (IPUZ_ACROSTIC (puzzle));
}

static void
ipuz_acrostic_build (IpuzPuzzle  *puzzle,
                     JsonBuilder *builder)
{
  IpuzAcrosticPrivate *priv;

  priv = ipuz_acrostic_get_instance_private (IPUZ_ACROSTIC (puzzle));

  IPUZ_PUZZLE_CLASS (ipuz_acrostic_parent_class)->build (puzzle, builder);

  if (priv->source != NULL)
    {
      json_builder_set_member_name (builder, _IPUZ_X_ACROSTIC_SOURCE_TAG);
      json_builder_add_string_value (builder, priv->source);
    }
}

static void
ipuz_acrostic_real_fix_all (IpuzCrossword *self,
                            const gchar   *first_attribute_name,
                            va_list        var_args)

{
  const gchar *attribute_name;
  IpuzAcrosticSyncDirection direction;
  va_list var_args_copy;

  va_copy (var_args_copy, var_args);
  attribute_name = first_attribute_name;

  while (attribute_name)
    {
      if (! g_strcmp0 (attribute_name, "sync-direction"))
        {
          direction = va_arg (var_args_copy, IpuzAcrosticSyncDirection);
          ipuz_acrostic_fix_quote (IPUZ_ACROSTIC (self), direction);
          ipuz_acrostic_fix_source (IPUZ_ACROSTIC (self), direction);
        }

      attribute_name = va_arg (var_args_copy, const gchar *);
    }

  va_end (var_args_copy);

  ipuz_acrostic_fix_labels (IPUZ_ACROSTIC (self));

  /* Don't chain up. Just fix styles from the parent class as its the
   * only one that makes sense. */
  ipuz_crossword_fix_styles (IPUZ_CROSSWORD (self));
}

static const gchar *const *
ipuz_acrostic_get_kind_str (IpuzPuzzle *puzzle)
{
  static const char *kind_str[] =
    {
      "http://ipuz.org/acrostic#1",
      NULL
    };

  return kind_str;
}

/*
 * Public Methods
 */

IpuzPuzzle *
ipuz_acrostic_new (void)
{
  IpuzPuzzle *acrostic;

  acrostic = g_object_new (IPUZ_TYPE_ACROSTIC,
                           "version", _IPUZ_VERSION_2,
                           NULL);

  return acrostic;
}

static gchar*
normalize_quote (IpuzAcrostic *self,
                 const gchar  *quote)
{
  const gchar *p;
  GString *string = NULL;
  gchar *sanitized = NULL;
  IpuzCharset *charset = NULL;
  const gchar *block;
  gboolean initial_word_found = FALSE;
  gboolean in_word = FALSE;

  charset = ipuz_puzzle_get_charset (IPUZ_PUZZLE (self));
  block = ipuz_puzzle_get_block (IPUZ_PUZZLE (self));
  string = g_string_new (NULL);

  for (p = quote; p[0]; p = g_utf8_next_char (p))
    {
      gunichar c;

      c = g_utf8_get_char (p);

      /* check if the character is valid */
      if (ipuz_charset_get_char_count (charset, g_unichar_toupper (c)))
        {
          initial_word_found = TRUE;
          in_word = TRUE;
          g_string_append_unichar (string, g_unichar_toupper (c));
        }
      else
        {
          /* skip over invalid chars at the start */
          if (!initial_word_found)
            continue;
          if (in_word)
            g_string_append (string, block);
          in_word = FALSE;
        }
    }

  sanitized = g_string_free (string, FALSE);

  /* remove trailing whitespaces */
  sanitized = g_strchomp (sanitized);

  /* Truncate the sanitized string if it's too long */
  if (g_utf8_strlen (sanitized, -1) > IPUZ_ACROSTIC_MAX_QUOTE_STR_LENGTH)
    g_utf8_offset_to_pointer (sanitized, IPUZ_ACROSTIC_MAX_QUOTE_STR_LENGTH)[0] = '\000';
  return sanitized;
}

void
ipuz_acrostic_set_quote (IpuzAcrostic *self,
                         const gchar  *quote)
{
  gchar *quote_copy;

  IpuzAcrosticPrivate *priv;

  g_return_if_fail (self != NULL);
  g_return_if_fail (quote != NULL);

  priv = ipuz_acrostic_get_instance_private (self);

  /* guard against (unlikely) calls of:
   * ipuz_acrostic_set_quote (a, ipuz_acrostic_get_quote (a)) */
  quote_copy = g_strdup (quote);

  g_clear_pointer (&priv->quote, g_free);
  g_clear_pointer (&priv->normalized_quote, g_free);

  priv->quote = quote_copy;
  priv->normalized_quote = normalize_quote (self, quote);
}

const gchar*
ipuz_acrostic_get_quote (IpuzAcrostic *self)
{
  IpuzAcrosticPrivate *priv;

  g_return_val_if_fail (self != NULL, NULL);

  priv = ipuz_acrostic_get_instance_private (self);

  return priv->quote;
}

/* Consider moving this to ipuz-charset */
static gchar*
normalize_source (IpuzAcrostic *self,
                  const gchar  *source)
{
  const gchar *p;
  GString *string = NULL;
  IpuzCharset *charset = NULL;

  charset = ipuz_puzzle_get_charset (IPUZ_PUZZLE (self));
  string = g_string_new (NULL);

  for (p = source; p[0]; p = g_utf8_next_char (p))
    {
      gunichar c;

      c = g_utf8_get_char (p);

      /* check if the character is valid */
      if (ipuz_charset_get_char_count (charset, g_unichar_toupper (c)))
        g_string_append_unichar (string, g_unichar_toupper (c));
    }

  return g_string_free (string, FALSE);
}

void
ipuz_acrostic_set_source (IpuzAcrostic *self,
                          const gchar  *source)
{
  gchar *source_copy;

  IpuzAcrosticPrivate *priv;

  g_return_if_fail (self != NULL);
  g_return_if_fail (source != NULL);

  priv = ipuz_acrostic_get_instance_private (self);

  /* guard against (unlikely) calls of:
   * ipuz_acrostic_set_source (a, ipuz_acrostic_get_source (a)) */
  source_copy = g_strdup (source);

  g_clear_pointer (&priv->source, g_free);
  g_clear_pointer (&priv->normalized_source, g_free);

  priv->source = source_copy;
  priv->normalized_source = normalize_source (self, source);
}

const gchar*
ipuz_acrostic_get_source (IpuzAcrostic *self)
{
  IpuzAcrosticPrivate *priv;

  g_return_val_if_fail (self != NULL, NULL);

  priv = ipuz_acrostic_get_instance_private (self);

  return priv->source;
}

IpuzClue *
ipuz_acrostic_get_quote_clue (IpuzAcrostic *self)
{
  IpuzAcrosticPrivate *priv;

  priv = ipuz_acrostic_get_instance_private (self);

  return priv->quote_clue;
}

static void
update_grid_from_quote (IpuzAcrostic *self)
{
  IpuzAcrosticPrivate *priv;
  const gchar *ptr;
  guint rows, columns;
  const gchar *block;

  rows = ipuz_grid_get_height (IPUZ_GRID (self));
  columns = ipuz_grid_get_width (IPUZ_GRID (self));

  priv = ipuz_acrostic_get_instance_private (self);
  ptr = priv->normalized_quote;
  block = ipuz_puzzle_get_block (IPUZ_PUZZLE (self));

  for (guint row = 0; row < rows; row++)
    {
      for (guint column = 0; column < columns; column++)
        {
          IpuzCell *cell;
          IpuzCellCoord coord = {
            .row = row,
            .column = column,
          };

          cell = ipuz_grid_get_cell (IPUZ_GRID (self), &coord);

          /* Clear out the cell. As a reminder, this call will clear
           * out old values */
          ipuz_cell_set_cell_type (cell, IPUZ_CELL_BLOCK);

          if (ptr && ptr[0])
            {
              gchar cell_str[6];

              g_utf8_strncpy (cell_str, ptr, 1);

              /* It's a valid string that's not block. Update cell */
              if (cell_str[0] && g_strcmp0 (block, cell_str))
                {
                  ipuz_cell_set_cell_type (cell, IPUZ_CELL_NORMAL);
                  ipuz_cell_set_solution (cell, cell_str);
                }

              ptr = g_utf8_next_char (ptr);
            }
        }
    }
}

static void
ensure_board_fits_quote (IpuzAcrostic *self)
{
  IpuzAcrosticPrivate *priv;
  guint quote_length;
  AcrosticBoardDimension dimension;

  priv = ipuz_acrostic_get_instance_private (self);

  quote_length = g_utf8_strlen (priv->normalized_quote, -1);
  dimension = acrostic_board_dimension_from_quote_length (quote_length);

  ipuz_grid_resize (IPUZ_GRID (self), dimension.width, dimension.height);
}

static void
sync_quote_to_grid (IpuzAcrostic *self)
{
  IpuzAcrosticPrivate *priv;
  g_return_if_fail (IPUZ_IS_ACROSTIC (self));

  priv = ipuz_acrostic_get_instance_private (self);

  if (priv->quote != NULL)
    {
      ensure_board_fits_quote (self);
      update_grid_from_quote (self);

      /* regenerate this every time */
      g_clear_pointer (& priv->quote_clue, ipuz_clue_unref);
      priv->quote_clue = calculate_quote_clue (self);
    }
  else
    {
      ipuz_grid_resize (IPUZ_GRID (self), 0, 0);
    }
}

static void
sync_grid_to_quote (IpuzAcrostic *self)
{
  GString *quote = NULL;
  guint rows, columns;
  g_autofree gchar *quote_str = NULL;

  rows = ipuz_grid_get_height (IPUZ_GRID (self));
  columns = ipuz_grid_get_width (IPUZ_GRID (self));

  quote = g_string_new (NULL);

  for (guint row = 0; row < rows; row++)
    {
      for (guint column = 0; column < columns; column++)
        {
          IpuzCell *cell;
          IpuzCellCoord coord = {
            .row = row,
            .column = column,
          };

          cell = ipuz_grid_get_cell (IPUZ_GRID (self), &coord);

          if (IPUZ_CELL_IS_NORMAL (cell))
            {
              const gchar *solution;

              solution = ipuz_cell_get_solution (cell);
              g_string_append (quote, solution);
            }
          else
            {
              g_string_append_unichar (quote, ' ');
            }
        }
    }

  quote_str = g_string_free (quote, FALSE);
  g_strchomp (quote_str);

  ipuz_acrostic_set_quote (self, quote_str);
}

void
ipuz_acrostic_fix_quote (IpuzAcrostic              *self,
                         IpuzAcrosticSyncDirection  sync_direction)
{
  g_return_if_fail (self != NULL);

  if (sync_direction == IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE)
    sync_quote_to_grid (self);
  else if (sync_direction == IPUZ_ACROSTIC_SYNC_PUZZLE_TO_STRING)
    sync_grid_to_quote (self);
  else
    g_assert_not_reached ();
}

/*
 * n: nth clue starting from 0
 */
static gchar *
generate_label (const gchar *alphabet,
                guint        n)
{
  GString *string = NULL;
  guint n_letters = g_utf8_strlen (alphabet, -1);

  string = g_string_new (NULL);

  /* clue indices start from  0 */
  n++;

  do {
    n--;
    g_string_prepend_c (string, alphabet[n % n_letters]);

    n = n / n_letters;
  } while (n > 0);

  return g_string_free (string, FALSE);
}

static void
sync_source_to_clues (IpuzAcrostic *self)
{
  IpuzAcrosticPrivate *priv;
  IpuzClueSets *clue_sets;
  IpuzCharset *charset;
  g_autofree gchar *alphabet = NULL;

  priv = ipuz_acrostic_get_instance_private (self);
  clue_sets = _ipuz_puzzle_get_clue_sets (IPUZ_PUZZLE (self));
  charset = ipuz_puzzle_get_charset (IPUZ_PUZZLE (self));

  alphabet = ipuz_charset_serialize (charset);
  g_return_if_fail (alphabet != NULL);

  /* FIXME: Clear existing clues for now. In the future, we should try
   * to preserve this and just change the length. */
  ipuz_clue_sets_remove_set (clue_sets, IPUZ_CLUE_DIRECTION_CLUES);
  if (priv->normalized_source == NULL)
    return;

  for (guint i = 0; i < g_utf8_strlen (priv->normalized_source, -1); i++)
    {
      gchar *label;
      IpuzClue *clue = ipuz_clue_new ();
      label = generate_label (alphabet, i);

      ipuz_clue_set_direction (clue, IPUZ_CLUE_DIRECTION_CLUES);
      ipuz_clue_set_label (clue, label);

      ipuz_clue_sets_append_clue (clue_sets, IPUZ_CLUE_DIRECTION_CLUES, clue);

      g_free (label);
    }
}

static void
sync_clues_to_source (IpuzAcrostic *self)
{
  GString *source = NULL;

  source = g_string_new (NULL);

  for (guint n = 0; n < ipuz_clues_get_n_clue_sets (IPUZ_CLUES (self)); n++)
    {
      GArray *clues;

      clues = ipuz_clues_get_clues (IPUZ_CLUES (self),
                                    ipuz_clues_clue_set_get_dir (IPUZ_CLUES (self), n));

      g_assert (clues);
      for (guint i = 0; i < clues->len; i++)
        {
          IpuzClue *clue;

          clue = g_array_index (clues, IpuzClue *, i);
          g_assert (clue);

          /* Only the first letter */
          for (guint j = 0; j < 1; j++)
            {
              IpuzCellCoord coord;
              IpuzCell *cell;

              ipuz_clue_get_coord (clue, j, &coord);
              cell = ipuz_grid_get_cell (IPUZ_GRID (self), &coord);

              g_string_append_unichar (source,
                                       g_utf8_get_char (ipuz_cell_get_solution (cell)));
            }
        }
    }

  ipuz_acrostic_set_source (self, source->str);

  g_string_free (source, TRUE);
}

void
ipuz_acrostic_fix_source (IpuzAcrostic              *self,
                          IpuzAcrosticSyncDirection  sync_direction)
{
  g_return_if_fail (self != NULL);

  if (sync_direction == IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE)
    sync_source_to_clues (self);
  else if (sync_direction == IPUZ_ACROSTIC_SYNC_PUZZLE_TO_STRING)
    sync_clues_to_source (self);
  else
    g_assert_not_reached ();
}

static void
ipuz_acrostic_fix_clues (IpuzCrossword *self)
{
  ipuz_acrostic_fix_source (IPUZ_ACROSTIC (self),
                            IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE);
}

static void
shuffle_array_foreach (gpointer key,
                       gpointer value,
                       gpointer user_data)
{
  IpuzCellCoordArray *array = value;

  ipuz_cell_coord_array_shuffle (array);
}

static IpuzCharsetBuilder *
build_answers_charset (GArray *answers)
{
  IpuzCharsetBuilder *builder = ipuz_charset_builder_new ();

  for (guint i = 0; i < answers->len; i++)
    {
      const gchar *answer;

      answer = g_array_index (answers, gchar *, i);

      ipuz_charset_builder_add_text (builder, answer);
    }

  return builder;
}

static GHashTable *
validate_and_build_cell_hash (IpuzAcrostic *self,
                              GArray       *answers)
{
  g_autoptr (IpuzCharsetBuilder) builder;
  GHashTable *hash_table;
  guint rows, columns;

  rows = ipuz_grid_get_height (IPUZ_GRID (self));
  columns = ipuz_grid_get_width (IPUZ_GRID (self));

  hash_table = g_hash_table_new_full (g_direct_hash, g_direct_equal, NULL,
                                      (GDestroyNotify) ipuz_cell_coord_array_unref);

  /* For validation */
  builder = build_answers_charset (answers);

  for (guint row = 0; row < rows; row++)
    {
      for (guint column = 0; column < columns; column++)
        {
          const gchar *sol;
          IpuzCell *cell;
          gunichar c;
          IpuzCellCoord coord = {
            .row = row,
            .column = column
          };

          cell = ipuz_grid_get_cell (IPUZ_GRID (self), &coord);

          if (IPUZ_CELL_IS_NORMAL (cell))
            {
              IpuzCellCoordArray *array;

              sol = ipuz_cell_get_solution (cell);
              c = g_utf8_get_char (sol);

              /* Validation */
              if (!ipuz_charset_builder_remove_character (builder, c))
                {
                  g_hash_table_unref (hash_table);
                  return NULL;
                }

              array = g_hash_table_lookup (hash_table, GUINT_TO_POINTER (c));
              if (array == NULL)
                {
                  array = ipuz_cell_coord_array_new ();
                  g_hash_table_insert (hash_table, GUINT_TO_POINTER (c), array);
                }

              ipuz_cell_coord_array_append (array, &coord);
            }
        }
    }

  /* Shuffle the order of cells for each letter */
  g_hash_table_foreach (hash_table, shuffle_array_foreach, NULL);

  return hash_table;
}

gboolean
ipuz_acrostic_set_answers (IpuzAcrostic *self,
                           GArray       *answers)
{
  GArray *clues;
  GHashTable *hash_table;

  g_return_val_if_fail (IPUZ_IS_ACROSTIC (self), FALSE);

  clues = ipuz_clues_get_clues (IPUZ_CLUES (self), IPUZ_CLUE_DIRECTION_CLUES);

  if (answers->len != clues->len)
    {
      g_warning ("Number of passed answers does not equal the number of clues");
      return FALSE;
    }

  hash_table = validate_and_build_cell_hash (self, answers);

  if (hash_table == NULL)
    {
      g_warning ("Invalid answers");
      return FALSE;
    }

  for (guint i = 0; i < answers->len; i++)
    {
      const gchar *p;
      const gchar *answer;
      IpuzClue *clue;

      clue = g_array_index (clues, IpuzClue *, i);
      answer = g_array_index (answers, gchar *, i);

      /* clear old coords */
      ipuz_clue_clear_coords (clue);

      for (p = answer; p[0]; p = g_utf8_next_char (p))
        {
          IpuzCellCoord coord;
          IpuzCellCoordArray *array;

          gunichar c = g_utf8_get_char (p);

          array = g_hash_table_lookup (hash_table, GUINT_TO_POINTER (c));

          if (ipuz_cell_coord_array_pop_front (array, &coord))
            {
              IpuzCell *cell;

              /* clue -> cell */
              ipuz_clue_append_coord (clue, &coord);

              /* cell -> clue */
              cell = ipuz_grid_get_cell (IPUZ_GRID (self), &coord);
              ipuz_cell_set_clue (cell, clue);
            }
        }
    }

  g_hash_table_unref (hash_table);

  return TRUE;
}

static void
set_cell_label (IpuzCell    *cell,
                guint        cell_label,
                const gchar *clue_label)
{
  g_autofree gchar *num_label = NULL;
  g_autofree gchar *label = NULL;

  num_label = g_strdup_printf ("%u", cell_label);
  label = g_strconcat (num_label, clue_label, NULL);

  ipuz_cell_set_label (cell, label);
}

void
ipuz_acrostic_fix_labels (IpuzAcrostic *self)
{
  guint num_label = 1;
  guint rows, columns;

  rows = ipuz_grid_get_height (IPUZ_GRID (self));
  columns = ipuz_grid_get_width (IPUZ_GRID (self));

  for (guint row = 0; row < rows; row++)
    {
      for (guint column = 0; column < columns; column++)
        {
          IpuzCell *cell;
          IpuzCellCoord coord = {
            .row = row,
            .column = column
          };

          cell = ipuz_grid_get_cell (IPUZ_GRID (self), &coord);

          if (IPUZ_CELL_IS_NORMAL (cell))
            {
              IpuzClue *clue;

              clue = (IpuzClue *)ipuz_cell_get_clue (cell, IPUZ_CLUE_DIRECTION_CLUES);

              if (clue != NULL)
                {
                  const gchar *label;

                  /* Ideal if set_answers returned TRUE */
                  if ((label = ipuz_clue_get_label (clue)))
                    {
                      set_cell_label (cell, num_label, label);
                      num_label++;
                    }
                }
            }
        }
    }
}
