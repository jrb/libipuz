/* ipuz-cell.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <json-glib/json-glib.h>
#include <libipuz/ipuz-clue.h>
#include <libipuz/ipuz-puzzle.h>
#include <libipuz/ipuz-style.h>

G_BEGIN_DECLS


#define IPUZ_TYPE_CELL (ipuz_cell_get_type ())
#define IPUZ_CELL_IS_NORMAL(cell)      ((cell) && ipuz_cell_get_cell_type(cell)==IPUZ_CELL_NORMAL)
#define IPUZ_CELL_IS_BLOCK(cell)       ((cell) && ipuz_cell_get_cell_type(cell)==IPUZ_CELL_BLOCK)
#define IPUZ_CELL_IS_NULL(cell)        ((cell) && ipuz_cell_get_cell_type(cell)==IPUZ_CELL_NULL)
#define IPUZ_CELL_IS_INITIAL_VAL(cell) ((cell) && ipuz_cell_get_initial_val(cell)!=NULL)
#define IPUZ_CELL_IS_GUESSABLE(cell)   (IPUZ_CELL_IS_NORMAL(cell)&&!IPUZ_CELL_IS_INITIAL_VAL(cell))

/**
 * IpuzCellType:
 * @IPUZ_CELL_NORMAL: A regular light cell
 * @IPUZ_CELL_BLOCK: A block
 * @IPUZ_CELL_NULL: An omitted cell
 *
 * Indicates the type and behavior of a given #IpuzCell.
 */
typedef enum
{
  IPUZ_CELL_NORMAL = 0,
  IPUZ_CELL_BLOCK,
  IPUZ_CELL_NULL,
} IpuzCellType;

/* Keep in sync with IpuzCell in libipuz/rust/src/guesses.rs */
typedef struct _IpuzCell IpuzCell;


GType            ipuz_cell_get_type             (void) G_GNUC_CONST;
IpuzCell        *ipuz_cell_ref                  (IpuzCell          *cell);
void             ipuz_cell_unref                (IpuzCell          *cell);
gboolean         ipuz_cell_equal                (const IpuzCell    *cell1,
                                                 const IpuzCell    *cell2);
void             ipuz_cell_build                (IpuzCell          *cell,
                                                 JsonBuilder       *builder,
                                                 gboolean           solution,
                                                 const char        *block,
                                                 const char        *empty);

IpuzCellType     ipuz_cell_get_cell_type        (IpuzCell          *cell);
void             ipuz_cell_set_cell_type        (IpuzCell          *cell,
                                                 IpuzCellType       cell_type);
gint             ipuz_cell_get_number           (IpuzCell          *cell);
void             ipuz_cell_set_number           (IpuzCell          *cell,
                                                 gint               number);
const gchar     *ipuz_cell_get_label            (IpuzCell          *cell);
void             ipuz_cell_set_label            (IpuzCell          *cell,
                                                 const gchar       *label);
const gchar     *ipuz_cell_get_solution         (IpuzCell          *cell);
void             ipuz_cell_set_solution         (IpuzCell          *cell,
                                                 const gchar       *solution);
const gchar     *ipuz_cell_get_saved_guess      (IpuzCell          *cell);
void             ipuz_cell_set_saved_guess      (IpuzCell          *cell,
                                                 const gchar       *saved_guess);
const gchar     *ipuz_cell_get_initial_val      (IpuzCell          *cell);
void             ipuz_cell_set_initial_val      (IpuzCell          *cell,
                                                 const gchar       *initial_val);
IpuzStyle       *ipuz_cell_get_style            (IpuzCell          *cell);
void             ipuz_cell_set_style            (IpuzCell          *cell,
                                                 IpuzStyle         *style,
                                                 const gchar       *style_name);
const gchar     *ipuz_cell_get_style_name       (IpuzCell          *cell);
void             ipuz_cell_set_style_name       (IpuzCell          *cell,
                                                 const gchar       *style_name);
const IpuzClue  *ipuz_cell_get_clue             (IpuzCell          *cell,
                                                 IpuzClueDirection  direction);
void             ipuz_cell_set_clue             (IpuzCell          *cell,
                                                 IpuzClue          *clue);
void             ipuz_cell_clear_clue_direction (IpuzCell          *cell,
                                                 IpuzClueDirection  direction);
void             ipuz_cell_clear_clues          (IpuzCell          *cell);

void             ipuz_cell_parse_puzzle         (IpuzCell          *cell,
                                                 JsonNode          *node,
                                                 IpuzPuzzleKind     kind,
                                                 const gchar       *block,
                                                 const gchar       *empty);
void             ipuz_cell_parse_solution       (IpuzCell          *cell,
                                                 JsonNode          *node,
                                                 IpuzPuzzleKind     kind,
                                                 const gchar       *block,
                                                 const gchar       *charset);


G_END_DECLS
