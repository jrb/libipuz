/* ipuz-guesses.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <json-glib/json-glib.h>
#include <libipuz/ipuz-cell.h>

G_BEGIN_DECLS


typedef struct _IpuzGuesses IpuzGuesses;


#define IPUZ_TYPE_GUESSES (ipuz_guesses_get_type ())


GType         ipuz_guesses_get_type         (void) G_GNUC_CONST;
IpuzGuesses  *ipuz_guesses_new              (void);
IpuzGuesses  *ipuz_guesses_new_from_file    (const gchar          *filename,
                                             GError              **error);
IpuzGuesses  *ipuz_guesses_new_from_stream  (GInputStream         *stream,
                                             GCancellable         *cancellable,
                                             GError              **error);
gboolean      ipuz_guesses_save_to_file     (IpuzGuesses          *guesses,
                                             const gchar          *filename,
                                             GError              **error);
IpuzGuesses  *ipuz_guesses_ref              (IpuzGuesses          *guesses);
void          ipuz_guesses_unref            (IpuzGuesses          *guesses);
IpuzGuesses  *ipuz_guesses_copy             (IpuzGuesses          *src);
gboolean      ipuz_guesses_equal            (IpuzGuesses          *guesses1,
                                             IpuzGuesses          *guesses2);
void          ipuz_guesses_resize           (IpuzGuesses          *guesses,
                                             guint                 new_width,
                                             guint                 new_height);
guint         ipuz_guesses_get_width        (IpuzGuesses          *guesses);
guint         ipuz_guesses_get_height       (IpuzGuesses          *guesses);
const gchar  *ipuz_guesses_get_guess        (IpuzGuesses          *guesses,
                                             const IpuzCellCoord  *coord);
void          ipuz_guesses_set_guess        (IpuzGuesses          *guesses,
                                             const IpuzCellCoord  *coord,
                                             const gchar          *guess);
IpuzCellType  ipuz_guesses_get_cell_type    (IpuzGuesses          *guesses,
                                             const IpuzCellCoord  *coord);
void          ipuz_guesses_set_cell_type    (IpuzGuesses          *guesses,
                                             const IpuzCellCoord  *coord,
                                             IpuzCellType          cell_type);
gfloat        ipuz_guesses_get_percent      (IpuzGuesses          *guesses);
gchar        *ipuz_guesses_get_checksum     (IpuzGuesses          *guesses,
                                             const gchar          *salt);
void          ipuz_guesses_set_stride_guess (IpuzGuesses          *guesses,
                                             IpuzClueDirection     direction,
                                             guint                 index,
                                             const gchar          *data);
const gchar  *ipuz_guesses_get_stride_guess (IpuzGuesses          *guesses,
                                             IpuzClueDirection     direction,
                                             guint                 index);

/* Helper functions */
void          ipuz_guesses_print            (IpuzGuesses          *guesses);


G_DEFINE_AUTOPTR_CLEANUP_FUNC(IpuzGuesses, ipuz_guesses_unref)


G_END_DECLS
