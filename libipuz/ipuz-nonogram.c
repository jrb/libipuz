/* ipuz-nonogram.c
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */


#include "libipuz-config.h"
#include <glib/gi18n-lib.h>

#include "ipuz-nonogram.h"
#include "ipuz-nonogram-clue.h"
#include "ipuz-nonogram-color.h"
#include "ipuz-magic.h"


enum
{
  PROP_0,
  PROP_SPACE,
  N_PROPS
};
static GParamSpec *obj_props[N_PROPS] = { NULL, };


typedef struct _IpuzNonogramPrivate
{
  GArray *across_clues;
  GArray *down_clues;
  GHashTable *group_cells;
  gchar **groups;
  guint n_groups;
  gchar *space;
} IpuzNonogramPrivate;


static void                ipuz_nonogram_init           (IpuzNonogram        *self);
static void                ipuz_nonogram_class_init     (IpuzNonogramClass   *klass);
static void                ipuz_nonogram_set_property   (GObject             *object,
                                                         guint                prop_id,
                                                         const GValue        *value,
                                                         GParamSpec          *pspec);
static void                ipuz_nonogram_get_property   (GObject             *object,
                                                         guint                prop_id,
                                                         GValue              *value,
                                                         GParamSpec          *pspec);
static void                ipuz_nonogram_dispose        (GObject             *object);
static void                ipuz_nonogram_fixup          (IpuzPuzzle          *puzzle);
static void                ipuz_nonogram_build          (IpuzPuzzle          *puzzle,
                                                         JsonBuilder         *builder);
static IpuzPuzzleFlags     ipuz_nonogram_get_flags      (IpuzPuzzle          *puzzle);
static const gchar *const *ipuz_nonogram_get_kind_str   (IpuzPuzzle          *puzzle);
static void                ipuz_nonogram_calculate_info (IpuzPuzzle          *puzzle,
                                                         IpuzPuzzleInfo      *info);
static gboolean            ipuz_nonogram_check_cell     (IpuzGrid            *grid,
                                                         IpuzCell            *cell,
                                                         const IpuzCellCoord *coord,
                                                         IpuzGuesses         *guesses,
                                                         IpuzGridCheckType    check_type);
static gboolean            ipuz_nonogram_check_stride   (IpuzGrid            *grid,
                                                         IpuzClueDirection    direction,
                                                         guint                index,
                                                         IpuzGuesses         *guesses,
                                                         IpuzGridCheckType    check_type);



G_DEFINE_TYPE_WITH_CODE (IpuzNonogram, ipuz_nonogram, IPUZ_TYPE_GRID,
                         G_ADD_PRIVATE (IpuzNonogram));


static void
clue_array_clear_func (IpuzNonogramClue *clue)
{
  g_clear_pointer (&clue->group, g_free);
}

static void
cell_array_clear_func (GArray **cell_row)
{
  g_array_unref (*cell_row);
  *cell_row = NULL;
}

static void
ipuz_nonogram_init (IpuzNonogram *self)
{
  IpuzNonogramPrivate *priv;

  priv = ipuz_nonogram_get_instance_private (self);

  priv->space = g_strdup (_IPUZ_DEFAULT_SPACE);

  priv->across_clues = g_array_new (FALSE, TRUE, sizeof (GArray *));
  g_array_set_clear_func (priv->across_clues, (GDestroyNotify) cell_array_clear_func);

  priv->down_clues = g_array_new (FALSE, TRUE, sizeof (GArray *));
  g_array_set_clear_func (priv->down_clues, (GDestroyNotify) cell_array_clear_func);

  priv->group_cells = g_hash_table_new_full (g_str_hash, g_str_equal,
                                             g_free,
                                             (GDestroyNotify) ipuz_cell_coord_array_unref);
}

static void
ipuz_nonogram_class_init (IpuzNonogramClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  IpuzPuzzleClass *puzzle_class = IPUZ_PUZZLE_CLASS (klass);
  IpuzGridClass *grid_class = IPUZ_GRID_CLASS (klass);

  object_class->dispose = ipuz_nonogram_dispose;
  object_class->set_property = ipuz_nonogram_set_property;
  object_class->get_property = ipuz_nonogram_get_property;
  puzzle_class->fixup = ipuz_nonogram_fixup;
  puzzle_class->build = ipuz_nonogram_build;
  puzzle_class->get_flags = ipuz_nonogram_get_flags;
  puzzle_class->get_kind_str = ipuz_nonogram_get_kind_str;
  puzzle_class->calculate_info = ipuz_nonogram_calculate_info;
  grid_class->check_cell = ipuz_nonogram_check_cell;
  grid_class->check_stride = ipuz_nonogram_check_stride;

  /**
   * IpuzNonogram:space: (attributes org.gtk.Property.get=ipuz_nonogram_get_space org.gtk.Property.set=ipuz_nonogram_set_space org.gtk.Property.default=".")
   *
   * The text value that represents a space in the saved file
   *
   * This value is used to indicate a the user eliminated the
   * possibility of a block in a cell. It is not recommended to change
   * this value, as it is written to the save file.
   *
   * This string must be only be one unicode character long, and defaults to `"."`
   **/
  obj_props[PROP_SPACE] = g_param_spec_string ("space",
                                                "Space",
                                                "Text value which represents a space in a guess",
                                                _IPUZ_DEFAULT_SPACE,
                                                G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

}

static void
ipuz_nonogram_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  IpuzNonogramPrivate *priv;

  priv = ipuz_nonogram_get_instance_private (IPUZ_NONOGRAM (object));

  switch (prop_id)
    {
    case PROP_SPACE:
      g_free (priv->space);
      priv->space = g_value_dup_string (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
ipuz_nonogram_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  IpuzNonogramPrivate *priv;

  priv = ipuz_nonogram_get_instance_private (IPUZ_NONOGRAM (object));

  switch (prop_id)
    {
    case PROP_SPACE:
      g_value_set_string (value, priv->space);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
ipuz_nonogram_dispose (GObject *object)
{
  IpuzNonogramPrivate *priv;

  priv = ipuz_nonogram_get_instance_private (IPUZ_NONOGRAM (object));

  g_clear_pointer (&priv->space, g_free);
  g_clear_pointer (&priv->across_clues, g_array_unref);
  g_clear_pointer (&priv->down_clues, g_array_unref);
  g_clear_pointer (&priv->group_cells, g_hash_table_unref);
  g_clear_pointer (&priv->groups, g_free); /* not g_strfreev() as the strings are owned by the hashtable*/
  priv->n_groups = 0;

  G_OBJECT_CLASS (ipuz_nonogram_parent_class)->dispose (object);
}

static void
ipuz_nonogram_fixup (IpuzPuzzle *puzzle)
{
  IPUZ_PUZZLE_CLASS (ipuz_nonogram_parent_class)->fixup (puzzle);

  ipuz_nonogram_fix_clues (IPUZ_NONOGRAM (puzzle));
}

static void
ipuz_nonogram_build (IpuzPuzzle  *puzzle,
                     JsonBuilder *builder)
{
  IPUZ_PUZZLE_CLASS (ipuz_nonogram_parent_class)->build (puzzle, builder);
}

static IpuzPuzzleFlags
ipuz_nonogram_get_flags (IpuzPuzzle *puzzle)
{
  guint flags;

  flags = IPUZ_PUZZLE_CLASS (ipuz_nonogram_parent_class)->get_flags (puzzle);

  /* all nonograms are an extension! */
  flags |= IPUZ_PUZZLE_FLAG_USES_EXTENSIONS;

  return flags;
}

static const gchar *const *
ipuz_nonogram_get_kind_str (IpuzPuzzle *puzzle)
{
  static const char *kind_str[] =
    {
      "http://libipuz.org/nonogram#1",
      NULL
    };

  return kind_str;
}

static void
ipuz_nonogram_calculate_info (IpuzPuzzle     *puzzle,
                              IpuzPuzzleInfo *info)
{
  /* FIXME: Write once we have the interface */
  g_assert_not_reached ();
}

static gboolean
ipuz_nonogram_check_cell (IpuzGrid            *grid,
                          IpuzCell            *cell,
                          const IpuzCellCoord *coord,
                          IpuzGuesses         *guesses,
                          IpuzGridCheckType    check_type)
{
  IpuzCellType cell_type, guesses_cell_type;

  /* All operations so far need guesses */
  if (guesses == NULL)
    return FALSE;

  cell_type = ipuz_cell_get_cell_type (cell);
  guesses_cell_type = ipuz_guesses_get_cell_type (guesses, coord);

  switch (check_type)
    {
    case IPUZ_GRID_CHECK_INITIALIZE_GUESS:
      if (cell_type == IPUZ_CELL_NULL)
        ipuz_guesses_set_cell_type (guesses, coord, IPUZ_CELL_NULL);
      else
        ipuz_guesses_set_cell_type (guesses, coord, IPUZ_CELL_NORMAL);
      break;
    case IPUZ_GRID_CHECK_GUESSABLE:
      return cell_type != IPUZ_CELL_NULL;
    case IPUZ_GRID_CHECK_GUESSES_VALID:
      if (cell_type == IPUZ_CELL_NULL)
        return (guesses_cell_type == IPUZ_CELL_NULL);
      return TRUE;
    case IPUZ_GRID_CHECK_GUESS_MADE:
        return (cell_type == IPUZ_CELL_BLOCK);
    case IPUZ_GRID_CHECK_GUESS_CORRECT:
        return (cell_type == guesses_cell_type);
    default:
      break;
    }

  return FALSE;
}

static gboolean
ipuz_nonogram_check_stride (IpuzGrid          *grid,
                            IpuzClueDirection  direction,
                            guint              index,
                            IpuzGuesses       *guesses,
                            IpuzGridCheckType  check_type)
{
  if (check_type == IPUZ_GRID_CHECK_INITIALIZE_GUESS)
    {
      GArray *clues;

      clues = ipuz_nonogram_get_clues (IPUZ_NONOGRAM (grid),
                                       direction, index);

      if (clues && clues->len)
        {
          const gchar *empty;
          g_autoptr (GString) new_stride = NULL;

          empty = ipuz_puzzle_get_empty (IPUZ_PUZZLE (grid));
          new_stride = g_string_new (NULL);
          for (guint i = 0; i < clues->len; i++)
            g_string_append (new_stride, empty);
          ipuz_guesses_set_stride_guess (guesses, direction, index, new_stride->str);
        }
      else
        ipuz_guesses_set_stride_guess (guesses, direction, index, NULL);

      return TRUE;
    }
  return FALSE;
}

/*
 * Public Methods
 */

IpuzPuzzle *
ipuz_nonogram_new (void)
{
  IpuzPuzzle *nonogram;

  nonogram = g_object_new (IPUZ_TYPE_NONOGRAM,
                           "version", _IPUZ_VERSION_2,
                           NULL);

  return nonogram;
}

const gchar *
ipuz_nonogram_get_space (IpuzNonogram *self)
{
  IpuzNonogramPrivate *priv;

  g_return_val_if_fail (IPUZ_IS_NONOGRAM (self), NULL);

  priv = ipuz_nonogram_get_instance_private (IPUZ_NONOGRAM (self));

  return priv->space;
}

void
ipuz_nonogram_set_space (IpuzNonogram *self,
                         const gchar  *space)
{
  IpuzNonogramPrivate *priv;
  gchar *new_space;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_nonogram_get_instance_private (self);

  /* Make sure we only include the first character of space. we do not
   * support multi-char space strings */
  new_space = g_utf8_substring (space, 0, 1);
  g_clear_pointer (&priv->space, g_free);

  if (new_space == NULL)
    priv->space = g_strdup (_IPUZ_DEFAULT_SPACE);
  else
    priv->space = new_space;

  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_SPACE]);
}


GArray *
ipuz_nonogram_get_clues(IpuzNonogram      *self,
                        IpuzClueDirection  direction,
                        guint              index)
{
  IpuzNonogramPrivate *priv;
  GArray *arr;

  g_return_val_if_fail (IPUZ_IS_NONOGRAM (self), NULL);
  g_return_val_if_fail (direction == IPUZ_CLUE_DIRECTION_ACROSS || direction == IPUZ_CLUE_DIRECTION_DOWN, NULL);

  priv = ipuz_nonogram_get_instance_private (IPUZ_NONOGRAM (self));

  if (direction == IPUZ_CLUE_DIRECTION_ACROSS)
    arr = priv->across_clues;
  else
    arr = priv->down_clues;

  g_return_val_if_fail (index < arr->len, NULL);

  return g_array_index (arr, GArray *, index);
}

/* This will guarantee that we always have valid arrays for our
 * horizontal and vertical clues. It doesn't update the contents of
 * the arrays — that's a different pass. */
static void
fix_clue_arrays (IpuzNonogram *self)
{
  IpuzNonogramPrivate *priv;
  guint new_width, new_height;
  guint old_width, old_height;

  priv = ipuz_nonogram_get_instance_private (self);

  new_width = ipuz_grid_get_width (IPUZ_GRID (self));
  new_height = ipuz_grid_get_height (IPUZ_GRID (self));

  old_width = priv->down_clues->len;
  old_height = priv->across_clues->len;

  if (new_width == old_width && new_height == old_height)
    return;

  if (new_height > old_height)
    {
      for (guint i = 0; i < new_height - old_height; i++)
        {
          GArray *new_row;
          new_row = g_array_new (FALSE, TRUE, sizeof (IpuzNonogramClue));
          g_array_set_clear_func (new_row, (GDestroyNotify) clue_array_clear_func);
          g_array_append_val (priv->across_clues, new_row);
        }
    }
  else
    {
      g_array_set_size (priv->across_clues, new_height);
    }

  if (new_width > old_width)
    {
      for (guint i = 0; i < new_width - old_width; i++)
        {
          GArray *new_row;
          new_row = g_array_new (FALSE, TRUE, sizeof (IpuzNonogramClue));
          g_array_set_clear_func (new_row, (GDestroyNotify) clue_array_clear_func);
          g_array_append_val (priv->down_clues, new_row);
        }
    }
  else
    {
      g_array_set_size (priv->down_clues, new_width);
    }
}


static void
ensure_block_style (IpuzNonogram *self)
{
  IpuzStyle *block_style;

  /* Make sure there's a block style. Either check it exists, or set it. */
  block_style = ipuz_puzzle_get_style (IPUZ_PUZZLE (self), ipuz_puzzle_get_block (IPUZ_PUZZLE (self)));
  if (block_style == NULL)
    {
      block_style = ipuz_style_new ();
      ipuz_style_set_bg_color (block_style, "#000000");
      ipuz_puzzle_set_style (IPUZ_PUZZLE (self), ipuz_puzzle_get_block (IPUZ_PUZZLE (self)), block_style);
      ipuz_style_unref (block_style);
    }
}

/* This function's name is a little misleading.  It wil calculate the
 * clues for the direction, but while we're iterating it will also fix
 * up the styles and build out the color hashtable.
 */
static void
calculate_clues (IpuzNonogram      *self,
                 IpuzClueDirection  direction)
{
  IpuzNonogramPrivate *priv;
  guint target, alt_target;
  GArray *target_array;
  gboolean calculate_color;

  priv = ipuz_nonogram_get_instance_private (self);
  calculate_color = IPUZ_IS_NONOGRAM_COLOR (self);

  /* Pick our dimensions and array for the direction. Doing this means
   * we avoid writing two almost-identical functions for each
   * direction. */
  if (direction == IPUZ_CLUE_DIRECTION_ACROSS)
    {
      if (!calculate_color)
        ensure_block_style (self);
      target = ipuz_grid_get_height (IPUZ_GRID (self));
      alt_target = ipuz_grid_get_width (IPUZ_GRID (self));
      target_array = priv->across_clues;
    }
  else /* direction == DOWN */
    {
      target = ipuz_grid_get_width (IPUZ_GRID (self));
      alt_target = ipuz_grid_get_height (IPUZ_GRID (self));
      target_array = priv->down_clues;
    }

  for (guint i = 0; i < target; i++)
    {
      GArray *segment_array;
      const gchar *current_solution = NULL;
      IpuzNonogramClue clue_segment = {
        .count = 0,
      };

      /* clear the array every time */
      segment_array = g_array_index (target_array, GArray *, i);
      g_assert (segment_array);
      g_array_set_size (segment_array, 0);

      for (guint j = 0; j < alt_target; j++)
        {
          IpuzCell *cell;
          IpuzCellCoord coord;
          const gchar *solution;

          if (direction == IPUZ_CLUE_DIRECTION_ACROSS)
            {
              coord.row = i;
              coord.column = j;
            }
          else
            {
              coord.row = j;
              coord.column = i;
            }

          cell = ipuz_grid_get_cell (IPUZ_GRID (self), &coord);
          solution = ipuz_cell_get_solution (cell);

          /* Fix up the cell style for color nonograms */
          /* Since this function goes through all the cells and we
           * call it once for each direction, we only need to do
           * this if direction is across. */
          if (calculate_color &&
              direction == IPUZ_CLUE_DIRECTION_ACROSS)
            {
              if (ipuz_cell_get_cell_type (cell) == IPUZ_CELL_BLOCK)
                {
                  IpuzStyle *style;

                  style = ipuz_puzzle_get_style (IPUZ_PUZZLE (self), solution);
                  if (!calculate_color && style == NULL)
                    style = ipuz_puzzle_get_style (IPUZ_PUZZLE (self), ipuz_puzzle_get_block (IPUZ_PUZZLE (self)));

                  /* This will set a style if it matches solution, or
                   * in the case of of a monochrome nonogram, will wet
                   * it to the block style. */
                  ipuz_cell_set_style (cell,
                                       style,
                                       solution);
                }
              else
                {
                  ipuz_cell_set_style (cell, NULL, NULL);
                }
            }

          if (ipuz_cell_get_cell_type (cell) == IPUZ_CELL_BLOCK)
            {
              if (!calculate_color)
                solution = ipuz_puzzle_get_block (IPUZ_PUZZLE (self));

              /* Add the cell to group_cells. */
              /* Since this function goes through all the cells and we
               * call it once for each direction, we only need to do
               * this if direction is across. */
              if (direction == IPUZ_CLUE_DIRECTION_ACROSS)
                {
                  IpuzCellCoordArray *arr;

                  arr = g_hash_table_lookup (priv->group_cells, solution);
                  if (arr == NULL)
                    {
                      arr = ipuz_cell_coord_array_new ();
                      g_hash_table_insert (priv->group_cells,
                                           (gpointer) g_strdup (solution),
                                           arr);
                    }
                  ipuz_cell_coord_array_append (arr, &coord);
                }

              if (current_solution != NULL)
                {
                  if (g_strcmp0 (current_solution, solution) == 0)
                    {
                      clue_segment.count++;
                    }
                  else
                    {
                      clue_segment.group = g_strdup (current_solution);
                      g_array_append_val (segment_array, clue_segment);
                      clue_segment.count = 1;
                      current_solution = solution;
                    }
                }
              else
                {
                  current_solution = solution;
                  clue_segment.count++;
                }
            }
          else if (current_solution != NULL)
            {
              clue_segment.group = g_strdup (current_solution);
              g_array_append_val (segment_array, clue_segment);
              clue_segment.count = 0;
              current_solution = NULL;
            }
        }

      if (current_solution != NULL)
        {
          clue_segment.group = g_strdup (current_solution);
          g_array_append_val (segment_array, clue_segment);
        }
    }
}

/* from the qsort man page */
static int
cmpstringp (const void *p1, const void *p2)
{
  return g_strcmp0(*(const char **) p1, *(const char **) p2);
}

void
ipuz_nonogram_fix_clues (IpuzNonogram *self)
{
  IpuzNonogramPrivate *priv;

  g_return_if_fail (IPUZ_IS_NONOGRAM (self));

  priv = ipuz_nonogram_get_instance_private (self);

  fix_clue_arrays (self);
  g_hash_table_remove_all (priv->group_cells);
  g_clear_pointer (&priv->groups, g_free);
  priv->n_groups = 0;

  calculate_clues (self, IPUZ_CLUE_DIRECTION_ACROSS);
  calculate_clues (self, IPUZ_CLUE_DIRECTION_DOWN);

  /* cache the keys as an array */
  priv->groups = (gchar **) g_hash_table_get_keys_as_array (priv->group_cells,
                                                            &priv->n_groups);
  /* sort the keys. This isn't strictly necessary, but having these
   * stable and sorted makes it easier to test */
  qsort (priv->groups, priv->n_groups, sizeof (gchar *),
         cmpstringp);
}

guint
ipuz_nonogram_get_n_groups (IpuzNonogram *self)
{
  IpuzNonogramPrivate *priv;

  g_return_val_if_fail (IPUZ_IS_NONOGRAM (self), 0);

  priv = ipuz_nonogram_get_instance_private (self);

  return priv->n_groups;
}

const gchar *
ipuz_nonogram_get_group (IpuzNonogram *self,
                         guint         index)
{
  IpuzNonogramPrivate *priv;

  g_return_val_if_fail (IPUZ_IS_NONOGRAM (self), NULL);

  priv = ipuz_nonogram_get_instance_private (self);

  g_return_val_if_fail (index < priv->n_groups, NULL);

  return priv->groups[index];

}

IpuzCellCoordArray *
ipuz_nonogram_get_cells_by_group (IpuzNonogram *self,
                                  const gchar  *group)
{
  IpuzNonogramPrivate *priv;

  g_return_val_if_fail (IPUZ_IS_NONOGRAM (self), NULL);
  g_return_val_if_fail (group != NULL, NULL);

  priv = ipuz_nonogram_get_instance_private (self);

  return g_hash_table_lookup (priv->group_cells, group);

}

void
ipuz_nonogram_set_count_crossed_off (IpuzNonogram      *self,
                                     IpuzClueDirection  direction,
                                     guint              index,
                                     guint              count_index,
                                     gboolean           crossed_off)
{
  IpuzGuesses *guesses;
  const gchar *block, *empty;
  GArray *clues;
  const gchar *old_stride;
  g_autoptr (GString) new_stride = NULL;
  const gchar *ptr;

  g_return_if_fail (IPUZ_IS_NONOGRAM (self));

  guesses = ipuz_grid_get_guesses (IPUZ_GRID (self));
  clues = ipuz_nonogram_get_clues (self, direction, index);
  block = ipuz_puzzle_get_block (IPUZ_PUZZLE (self));
  empty = ipuz_puzzle_get_empty (IPUZ_PUZZLE (self));

  /* sanity check things */
  if (guesses == NULL)
    return;
  if (clues == NULL || clues->len == 0)
    return;

  /* We can't set a count higher than the number of clues */
  g_return_if_fail (count_index < clues->len);

  old_stride = ipuz_guesses_get_stride_guess (guesses, direction, index);
  ptr = old_stride;
  new_stride = g_string_new (NULL);

  /* copy the old stride over to the new one. We can't make any
   * guarantees about the input so this is a very conservative
   * implementation. This is necessary really only if something weird
   * got saved to disk or if we're combining editing nonograms with
   * guesses. But better safe than sorry.*/
  for (guint i = 0; i < clues->len; i++)
    {
      const gchar *append_str = empty;

      /* Walk the old_stride as well and keep it*/
      if (ptr && *ptr)
        {

          if(i == count_index)
            {
              if (crossed_off)
                {
                  append_str = block;
                }
            }
          else if (g_utf8_get_char (ptr) == g_utf8_get_char (block))
            append_str = block;

          ptr = g_utf8_next_char (ptr);
        }
      else
        {
          if (i == count_index &&crossed_off)
            append_str = block;
        }

      g_string_append (new_stride, append_str);
    }

  ipuz_guesses_set_stride_guess (guesses, direction, index, new_stride->str);
}

gboolean
ipuz_nonogram_get_count_crossed_off (IpuzNonogram      *self,
                                     IpuzClueDirection  direction,
                                     guint              index,
                                     guint              count_index)
{
  IpuzGuesses *guesses;
  const gchar *block;
  const gchar *stride;

  g_return_val_if_fail (IPUZ_IS_NONOGRAM (self), FALSE);

  guesses = ipuz_grid_get_guesses (IPUZ_GRID (self));
  block = ipuz_puzzle_get_block (IPUZ_PUZZLE (self));

  /* sanity check */
  if (guesses == NULL)
    return FALSE;

  stride = ipuz_guesses_get_stride_guess (guesses, direction, index);
  if (stride == NULL)
    return FALSE;

  for (guint i = 0; i < count_index; i++)
    {
      if (stride[0] == '\0')
        return FALSE;
      stride = g_utf8_next_char (stride);
    }

  return g_utf8_get_char (stride) == g_utf8_get_char (block);
}

void
ipuz_nonogram_print (IpuzNonogram *self)
{
  IpuzNonogramPrivate *priv;

  g_return_if_fail (IPUZ_IS_NONOGRAM (self));

  priv = ipuz_nonogram_get_instance_private (self);

  for (guint row = 0; row < ipuz_grid_get_height (IPUZ_GRID (self)); row++)
    {
      for (guint column = 0; column < ipuz_grid_get_width (IPUZ_GRID (self)); column++)
        {
          IpuzCell *cell;
          IpuzCellCoord coord = {
            .row = row,
            .column = column
          };

          cell = ipuz_grid_get_cell (IPUZ_GRID (self), &coord);
          if (ipuz_cell_get_cell_type (cell) == IPUZ_CELL_BLOCK)
            {
              const gchar *solution = ipuz_cell_get_solution (cell);
              if (solution && *solution)
                g_print ("%c", solution[0]);
              else
                g_print ("▓");
            }
          else
            g_print (" ");
        }
      g_print ("\n");
    }

  g_print ("\n");

  g_print ("across:\n");
  for (guint i = 0; i < priv->across_clues->len; i++)
    {
      GArray *row_array;

      row_array = g_array_index (priv->across_clues, GArray *, i);

      if (row_array->len == 0)
        g_print ("\n");
      else
        {
          for (guint j = 0; j < row_array->len; j++)
            {
              IpuzNonogramClue clue;

              clue = g_array_index (row_array, IpuzNonogramClue, j);
              if (IPUZ_IS_NONOGRAM_COLOR (self))
                g_print ("%u%s ", clue.count, clue.group);
              else
                g_print ("%u ", clue.count);
            }
          g_print ("\n");
        }
    }

  g_print ("down:\n");
  for (guint i = 0; i < priv->down_clues->len; i++)
    {
      GArray *row_array;

      row_array = g_array_index (priv->down_clues, GArray *, i);

      if (row_array->len == 0)
        g_print ("\n");
      else
        {
          for (guint j = 0; j < row_array->len; j++)
            {
              IpuzNonogramClue clue;

              clue = g_array_index (row_array, IpuzNonogramClue, j);
              g_print ("%u ", clue.count);
            }
          g_print ("\n");
        }
    }
}
