/* ipuz-cell-coord.
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

/**
 * IpuzCellCoord:
 * @row: the row offset of the cell
 * @column: the column offset of the cell
 *
 * A data type representing the coordinates of a single [struct@Ipuz.Cell].
 */


#include <libipuz/ipuz-cell-coord.h>


G_DEFINE_BOXED_TYPE (IpuzCellCoord, ipuz_cell_coord, ipuz_cell_coord_copy, ipuz_cell_coord_free);


/**
 * ipuz_cell_coord_copy:
 *
 * Returns a newly allocated copy of @coord.
 *
 * Returns: (transfer full): A newly allocated copy of @coord
 */
IpuzCellCoord *
ipuz_cell_coord_copy (const IpuzCellCoord *coord)
{
  IpuzCellCoord *copy = g_new (IpuzCellCoord, 1);
  *copy = *coord;

  return copy;
}

/**
 * ipuz_cell_coord_equal:
 * @coord1:  (nullable): An [struct@Ipuz.CellCoord]
 * @coord2:  (nullable): An [struct@Ipuz.CellCoord]
 *
 * Returns %TRUE if @coord1 and @coord2 are identical.
 *
 * Returns: %TRUE, if @coord1 and @coord2 are identical
 */
gboolean
ipuz_cell_coord_equal (const IpuzCellCoord *coord1,
                       const IpuzCellCoord *coord2)
{
  if (coord1 == NULL)
    return (coord2 == NULL);
  if (coord2 == NULL)
    return FALSE;

  return (coord1->row == coord2->row && coord1->column == coord2->column);
}

/**
 * ipuz_cell_coord_free:
 *
 * Frees @coord.
 **/
void
ipuz_cell_coord_free (IpuzCellCoord *coord)
{
  g_free (coord);
}
