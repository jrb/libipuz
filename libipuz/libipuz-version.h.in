/* libipuz-version.h.in
 *
 * Copyright 2022 Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#pragma once

#if !defined(LIBIPUZ_INSIDE) && !defined(LIBIPUZ_COMPILATION)
# error "Only <libipuz.h> can be included directly."
#endif

/**
 * SECTION:libipuzversion
 * @short_description: libipuz version checking
 *
 * libipuz provides macros to check the version of the library
 * at compile-time
 */

/**
 * LIBIPUZ_MAJOR_VERSION:
 *
 * libipuz major version component (e.g. 1 if %LIBIPUZ_VERSION is 1.2.3)
 */
#define LIBIPUZ_MAJOR_VERSION (@MAJOR_VERSION@)

/**
 * LIBIPUZ_MINOR_VERSION:
 *
 * libipuz minor version component (e.g. 2 if %LIBIPUZ_VERSION is 1.2.3)
 */
#define LIBIPUZ_MINOR_VERSION (@MINOR_VERSION@)

/**
 * LIBIPUZ_MICRO_VERSION:
 *
 * libipuz micro version component (e.g. 3 if %LIBIPUZ_VERSION is 1.2.3)
 */
#define LIBIPUZ_MICRO_VERSION (@MICRO_VERSION@)

/**
 * LIBIPUZ_VERSION
 *
 * libipuz version.
 */
#define LIBIPUZ_VERSION (@VERSION@)

/**
 * LIBIPUZ_VERSION_S:
 *
 * libipuz version, encoded as a string, useful for printing and
 * concatenation.
 */
#define LIBIPUZ_VERSION_S "@VERSION@"

#define LIBIPUZ_ENCODE_VERSION(major,minor,micro) \
        ((major) << 24 | (minor) << 16 | (micro) << 8)

/**
 * LIBIPUZ_VERSION_HEX:
 *
 * libipuz version, encoded as an hexadecimal number, useful for
 * integer comparisons.
 */
#define LIBIPUZ_VERSION_HEX \
        (LIBIPUZ_ENCODE_VERSION (LIBIPUZ_MAJOR_VERSION, LIBIPUZ_MINOR_VERSION, LIBIPUZ_MICRO_VERSION))

/**
 * LIBIPUZ_CHECK_VERSION:
 * @major: required major version
 * @minor: required minor version
 * @micro: required micro version
 *
 * Compile-time version checking. Evaluates to %TRUE if the version
 * of libipuz is greater than the required one.
 */
#define LIBIPUZ_CHECK_VERSION(major,minor,micro)   \
        (LIBIPUZ_MAJOR_VERSION > (major) || \
         (LIBIPUZ_MAJOR_VERSION == (major) && LIBIPUZ_MINOR_VERSION > (minor)) || \
         (LIBIPUZ_MAJOR_VERSION == (major) && LIBIPUZ_MINOR_VERSION == (minor) && \
          LIBIPUZ_MICRO_VERSION >= (micro)))
