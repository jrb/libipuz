/* ipuz-grid.h
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <json-glib/json-glib.h>
#include <libipuz/ipuz-cell.h>
#include <libipuz/ipuz-guesses.h>
#include <libipuz/ipuz-puzzle.h>

G_BEGIN_DECLS


#define IPUZ_TYPE_GRID   (ipuz_grid_get_type ())
G_DECLARE_DERIVABLE_TYPE (IpuzGrid, ipuz_grid, IPUZ, GRID, IpuzPuzzle);

/**
 * IpuzGridCheckType:
 * @IPUZ_GRID_CHECK_INITIALIZE_GUESS: Changes guesses at coord to be a good blank initial value based on cell
 * @IPUZ_GRID_CHECK_GUESSABLE: The cell at coord can take a user guess
 * @IPUZ_GRID_CHECK_GUESSES_VALID: Indicates that the cell in guesses is appropriate to the puzzle type. For example, in crosswords it indicates they have the same cell_type
 * @IPUZ_GRID_CHECK_GUESS_MADE: The player attempted a guess at coord
 * @IPUZ_GRID_CHECK_GUESS_CORRECT: Guesses is in a winning state at coord
 *
 * Operation passed to [method@Ipuz.Grid.check_cell].
 */
typedef enum
{
  IPUZ_GRID_CHECK_INITIALIZE_GUESS,
  IPUZ_GRID_CHECK_GUESSABLE,
  IPUZ_GRID_CHECK_GUESSES_VALID,
  IPUZ_GRID_CHECK_GUESS_MADE,
  IPUZ_GRID_CHECK_GUESS_CORRECT,
} IpuzGridCheckType;

/**
 * IpuzGridForeachCellFunc:
 * @grid: The iterated [class@Ipuz.Grid]
 * @cell: The [struct@Ipuz.Cell]
 * @coord: The [struct@Ipuz.CellCoord] of @cell
 * @user_data: data passed to the function
 *
 * The function to be passed to [method@Ipuz.Grid.foreach_cell].
 *
 * You should not change the size of @grid from within this
 * function. It is safe to change properties of @cell.
 */
typedef void (*IpuzGridForeachCellFunc) (IpuzGrid            *grid,
                                         IpuzCell            *cell,
                                         const IpuzCellCoord *coord,
                                         IpuzGuesses         *guesses,
                                         gpointer             user_data);

typedef struct _IpuzGridClass
{
  IpuzPuzzleClass parent_class;

  gboolean (*check_cell)   (IpuzGrid            *grid,
                            IpuzCell            *cell,
                            const IpuzCellCoord *coord,
                            IpuzGuesses         *guesses,
                            IpuzGridCheckType    check_type);
  gboolean (*check_stride) (IpuzGrid            *grid,
                            IpuzClueDirection    direction,
                            guint                index,
                            IpuzGuesses         *guesses,
                            IpuzGridCheckType    check_type);
} IpuzGridClass;


guint        ipuz_grid_get_width      (IpuzGrid                *self);
guint        ipuz_grid_get_height     (IpuzGrid                *self);
IpuzCell    *ipuz_grid_get_cell       (IpuzGrid                *self,
                                       const IpuzCellCoord     *coord);
void         ipuz_grid_foreach_cell   (IpuzGrid                *self,
                                       IpuzGridForeachCellFunc  func,
                                       gpointer                 user_data);
void         ipuz_grid_resize         (IpuzGrid                *self,
                                       guint                    new_width,
                                       guint                    new_height);
IpuzGuesses *ipuz_grid_create_guesses (IpuzGrid                *self);
gboolean     ipuz_grid_set_guesses    (IpuzGrid                *self,
                                       IpuzGuesses             *guesses);
IpuzGuesses *ipuz_grid_get_guesses    (IpuzGrid                *self);
void         ipuz_grid_fix_guesses    (IpuzGrid                *self);
gboolean     ipuz_grid_check_cell     (IpuzGrid                *self,
                                       IpuzCell                *cell,
                                       const IpuzCellCoord     *coord,
                                       IpuzGuesses             *guesses,
                                       IpuzGridCheckType        check_type);
gboolean     ipuz_grid_check_stride   (IpuzGrid                *self,
                                       IpuzClueDirection        direction,
                                       guint                    index,
                                       IpuzGuesses             *guesses,
                                       IpuzGridCheckType        check_type);


G_END_DECLS
