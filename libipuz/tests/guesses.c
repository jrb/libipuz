#include <glib.h>
#include <libipuz/libipuz.h>
#include <locale.h>

static void
test_guesses_create_from_grid (void)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  g_autoptr (IpuzGuesses) guesses = NULL;
  g_autofree gchar *path = NULL;

  path = g_test_build_filename (G_TEST_DIST, "filippine-mini.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);

  if (error != NULL)
    {
      g_print ("Error: %s\n", error->message);
    }
  g_assert (puzzle != NULL);

  guesses = ipuz_grid_create_guesses (IPUZ_GRID (puzzle));

  g_assert_cmpint (ipuz_grid_get_width (IPUZ_GRID (puzzle)),
                   ==,
                   ipuz_guesses_get_width (guesses));
}

static void
test_guesses_resize (void)
{
  g_autoptr (IpuzGuesses) guesses;
  IpuzCellCoord coord;

  guesses = ipuz_guesses_new ();
  ipuz_guesses_resize (guesses, 10, 12);
  g_assert (ipuz_guesses_get_width (guesses) == 10);
  g_assert (ipuz_guesses_get_height (guesses) == 12);

  for (guint row = 0; row < ipuz_guesses_get_height (guesses); row++)
    {
      for (guint column = 0; column < ipuz_guesses_get_width (guesses); column++)
        {
          coord.row = row;
          coord.column = column;

          ipuz_guesses_set_cell_type (guesses,
                                      &coord,
                                      (IpuzCellType) (row+column)%3);
        }
    }
  ipuz_guesses_print (guesses);
  coord.row = 7;
  coord.column = 3;
  g_assert (ipuz_guesses_get_cell_type (guesses, &coord) == IPUZ_CELL_BLOCK);
  ipuz_guesses_resize (guesses, 3, 3);
  ipuz_guesses_resize (guesses, 10, 12);
  g_assert_cmpint (ipuz_guesses_get_width (guesses), ==, 10);
  g_assert_cmpint (ipuz_guesses_get_height (guesses), ==, 12);
  g_assert (ipuz_guesses_get_cell_type (guesses, &coord) == IPUZ_CELL_NULL);
}

static void
test_guesses_new (void)
{
  g_autoptr (IpuzGuesses) guesses;
  g_autofree gchar *checksum = NULL;

  /* create a 0x0 guesses */
  guesses = ipuz_guesses_new ();

  g_assert (ipuz_guesses_get_width (guesses) == 0);
  g_assert (ipuz_guesses_get_height (guesses) == 0);
  g_assert (ipuz_guesses_get_percent (guesses) == 0.0);
  checksum = ipuz_guesses_get_checksum (guesses, NULL);

  /* The checksum of an empty string is this: */
  g_assert_cmpstr (checksum, ==, "da39a3ee5e6b4b0d3255bfef95601890afd80709");
}

static void
test_guesses_empty_stride_guess (void)
{
  /* Test that both operations cause critical warnings */

  if (g_test_subprocess ())
    {
      /* create a 0x0 guesses */
      g_autoptr (IpuzGuesses) guesses = ipuz_guesses_new ();

      g_assert_null (ipuz_guesses_get_stride_guess (guesses, IPUZ_CLUE_DIRECTION_ACROSS, 0));
    }

  g_test_trap_subprocess (NULL, 0, 0);
  g_test_trap_assert_failed ();
  g_test_trap_assert_stderr ("*CRITICAL*index*");

  if (g_test_subprocess ())
    {
      /* create a 0x0 guesses */
      g_autoptr (IpuzGuesses) guesses = ipuz_guesses_new ();

      ipuz_guesses_set_stride_guess (guesses, IPUZ_CLUE_DIRECTION_DOWN, 0, "foo");
    }

  g_test_trap_subprocess (NULL, 0, 0);
  g_test_trap_assert_failed ();
  g_test_trap_assert_stderr ("*CRITICAL*index*");
}

static void
test_guesses_get_set_stride_guess (void)
{
  g_autoptr (IpuzGuesses) guesses = ipuz_guesses_new ();
  ipuz_guesses_resize (guesses, 10, 10);

  ipuz_guesses_set_stride_guess (guesses, IPUZ_CLUE_DIRECTION_ACROSS, 0, "foo");
  const gchar *data1 = ipuz_guesses_get_stride_guess (guesses, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  g_assert_cmpstr (data1, ==, "foo");

  g_assert_null (ipuz_guesses_get_stride_guess (guesses, IPUZ_CLUE_DIRECTION_DOWN, 0));

  ipuz_guesses_set_stride_guess (guesses, IPUZ_CLUE_DIRECTION_ACROSS, 0, NULL);
  const gchar *data2 = ipuz_guesses_get_stride_guess (guesses, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  g_assert_null (data2);
}

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/guesses/new", test_guesses_new);
  g_test_add_func ("/guesses/resize", test_guesses_resize);
  g_test_add_func ("/guesses/create_from_grid", test_guesses_create_from_grid);
  g_test_add_func ("/guesses/empty", test_guesses_empty_stride_guess);
  g_test_add_func ("/guesses/get_set_stride_guess", test_guesses_get_set_stride_guess);
  /* FIXME: load, save */

  return g_test_run ();
}

