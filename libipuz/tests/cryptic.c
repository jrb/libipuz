#include <glib.h>
#include <libipuz/libipuz.h>
#include <locale.h>


static void
test_cryptic_load (void)
{
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  g_autofree gchar *path = NULL;
  g_autoptr (GError) error = NULL;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "4962.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);

  if (error != NULL)
    {
      g_print ("Error: %s\n", error->message);
      g_assert_not_reached();
    }

  g_assert_nonnull (puzzle);
  g_assert_true (IPUZ_IS_CRYPTIC (puzzle));
  g_assert_true (ipuz_puzzle_get_puzzle_kind (puzzle) == IPUZ_PUZZLE_CRYPTIC);
  g_assert_true (ipuz_crossword_get_show_enumerations (IPUZ_CROSSWORD (puzzle)));
}

static void
test_cryptic_enumeration (void)
{
  IpuzPuzzle *puzzle;

  /* check that a crossword doesn't show_enumerations by default. */
  puzzle = ipuz_crossword_new ();
  g_assert_false (ipuz_crossword_get_show_enumerations (IPUZ_CROSSWORD (puzzle)));
  g_object_unref (puzzle);

  /* check that a cryptic shows_enumerations by default. */
  puzzle = ipuz_cryptic_new ();
  g_assert_true (ipuz_crossword_get_show_enumerations (IPUZ_CROSSWORD (puzzle)));
  g_object_unref (puzzle);
}

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/cryptic/load", test_cryptic_load);
  g_test_add_func ("/cryptic/enumeration", test_cryptic_enumeration);
  

  return g_test_run ();
}
