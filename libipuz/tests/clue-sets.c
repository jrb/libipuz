#include <glib.h>
#include <libipuz/libipuz.h>
#include <locale.h>


/* FIXME(tests): This one should actually fail. See Issue #29 */
#if 0
static void
test_clue_sets_duplicates (void)
{
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "clue-sets-fail.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);
  g_free (path);

  g_assert (puzzle == NULL);
}
#endif

static void
test_clue_sets_one_custom (void)
{
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "clue-sets1.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);
  g_free (path);

  if (error != NULL)
    {
      g_print ("Error: %s\n", error->message);
    }
  g_assert (puzzle != NULL);

  g_assert (ipuz_clues_get_n_clue_sets (IPUZ_CLUES (puzzle)) == 2);

}

static void
test_clue_sets_two_customs (void)
{
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "clue-sets2.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);
  g_free (path);

  if (error != NULL)
    {
      g_print ("Error: %s\n", error->message);
    }
  g_assert (puzzle != NULL);

  ipuz_crossword_print (IPUZ_CROSSWORD (puzzle));
}

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/clue-sets/one-custom", test_clue_sets_one_custom);
  g_test_add_func ("/clue-sets/two-customs", test_clue_sets_two_customs);

  return g_test_run ();
}

