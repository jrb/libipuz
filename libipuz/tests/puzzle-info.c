#include <glib.h>
#include <libipuz/libipuz.h>
#include <locale.h>


static void
test_cell_stats (void)
{
  IpuzCellStats stats;
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  g_autoptr (IpuzPuzzleInfo) info = NULL;
  GError *error = NULL;
  g_autofree gchar *path = NULL;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "quad-pangram.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);

  info = ipuz_puzzle_get_puzzle_info (puzzle);
  g_assert (IPUZ_IS_PUZZLE_INFO (info));

  stats = ipuz_puzzle_info_get_cell_stats (info);

  g_assert (stats.block_count == 64);
  g_assert (stats.normal_count == (225-64));
  g_assert (stats.null_count == 0);
}

static void
test_quad_pangram (void)
{
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  g_autoptr (IpuzPuzzleInfo) info = NULL;
  GError *error = NULL;
  g_autofree gchar *path = NULL;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "quad-pangram.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);

  info = ipuz_puzzle_get_puzzle_info (puzzle);
  g_assert (IPUZ_IS_PUZZLE_INFO (info));

  g_assert (ipuz_puzzle_info_get_pangram_count (info) == 4);
}

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/puzzle-info/quad-pangram", test_quad_pangram);
  g_test_add_func ("/puzzle-info/cell-stats", test_cell_stats);

  return g_test_run ();
}

