/* ipuz-cell-coord-array.h - A list of IpuzCellCoords
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <libipuz/ipuz-cell-coord.h>

G_BEGIN_DECLS

#define IPUZ_TYPE_CELL_COORD_ARRAY (ipuz_cell_coord_array_get_type ())
#define IPUZ_CELL_COORD_ARRAY(array) ((IpuzCellCoordArray *)array)

typedef struct _IpuzCellCoordArray IpuzCellCoordArray;

GType                ipuz_cell_coord_array_get_type     (void);
IpuzCellCoordArray  *ipuz_cell_coord_array_new          (void);
IpuzCellCoordArray  *ipuz_cell_coord_array_ref          (IpuzCellCoordArray       *self);
void                 ipuz_cell_coord_array_unref        (IpuzCellCoordArray       *self);
IpuzCellCoordArray  *ipuz_cell_coord_array_dup          (IpuzCellCoordArray       *self);
gboolean             ipuz_cell_coord_array_equal        (const IpuzCellCoordArray *array1,
                                                         const IpuzCellCoordArray *array2);
void                 ipuz_cell_coord_array_clear        (IpuzCellCoordArray       *self);
void                 ipuz_cell_coord_array_append       (IpuzCellCoordArray       *self,
                                                         const IpuzCellCoord      *coord);
void                 ipuz_cell_coord_array_remove_coord (IpuzCellCoordArray       *self,
                                                         const IpuzCellCoord      *coord);
gint                 ipuz_cell_coord_array_coord_index  (IpuzCellCoordArray       *self,
                                                         const IpuzCellCoord      *coord);
size_t               ipuz_cell_coord_array_len          (const IpuzCellCoordArray *self);
gboolean             ipuz_cell_coord_array_index        (const IpuzCellCoordArray *self,
                                                         size_t                    index,
                                                         IpuzCellCoord            *out_value);
gboolean             ipuz_cell_coord_array_pop_front    (IpuzCellCoordArray       *self,
                                                         IpuzCellCoord            *out_front);
void                 ipuz_cell_coord_array_shuffle      (IpuzCellCoordArray       *self);
void                 ipuz_cell_coord_array_print        (const IpuzCellCoordArray *self);


G_DEFINE_AUTOPTR_CLEANUP_FUNC (IpuzCellCoordArray, ipuz_cell_coord_array_unref);


G_END_DECLS
