/* ipuz-magic.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include "ipuz-puzzle.h"

#define _IPUZ_DEFAULT_BLOCK       "#"
#define _IPUZ_DEFAULT_EMPTY       "0"
#define _IPUZ_DEFAULT_SPACE       "." /* Used by nonograms */

#define _IPUZ_DEFAULT_LOCALE      "C"

#define _IPUZ_X_GNOME_LICENSE_TAG "org.gnome.libipuz:license" /* Backwards compatible */
#define _IPUZ_X_GNOME_LOCALE_TAG  "org.gnome.libipuz:locale"  /* Backwards compatible */
#define _IPUZ_X_LICENSE_TAG       "org.libipuz:license"
#define _IPUZ_X_LOCALE_TAG        "org.libipuz:locale"

/* Ipuz versions */
#define _IPUZ_VERSION_1           "http://ipuz.org/v1"
#define _IPUZ_VERSION_2           "http://ipuz.org/v2"

/* Puzzle Types */
#define _IPUZ_ACROSTIC_PREFIX        "http://ipuz.org/acrostic"
#define _IPUZ_CROSSWORD_PREFIX       "http://ipuz.org/crossword"
#define _IPUZ_ARROWWORD_PREFIX       "http://ipuz.org/crossword/arrowword"
#define _IPUZ_CRYPTIC_PREFIX         "http://ipuz.org/crossword/crypticcrossword"
#define _IPUZ_BARRED_PREFIX          "https://libipuz.org/barred"
#define _IPUZ_FILIPPINE_PREFIX       "https://libipuz.org/filippine"
#define _IPUZ_NONOGRAM_PREFIX        "https://libipuz.org/nonogram"
#define _IPUZ_NONOGRAM_COLOR_PREFIX  "https://libipuz.org/nonogram/colornonogram"

#define _IPUZ_ACROSTIC_VERSION       1
#define _IPUZ_CROSSWORD_VERSION      1
#define _IPUZ_ARROWWORD_VERSION      1
#define _IPUZ_CRYPTIC_VERSION        1
#define _IPUZ_BARRED_VERSION         1
#define _IPUZ_FILIPPINE_VERSION      1
#define _IPUZ_NONOGRAM_VERSION       1
#define _IPUZ_NONOGRAM_COLOR_VERSION 1

#define _IPUZ_ACROSTIC_QUOTE_STR    "[QUOTE]"
#define _IPUZ_X_ACROSTIC_SOURCE_TAG "org.libipuz:source"
