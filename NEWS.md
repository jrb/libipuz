=============
Version 0.5.1
=============

Release date: Jan 7, 2024

## Changes
* Fix the api_version tag

=============
Version 0.5.0
=============

Release date: Jan 6, 2024

## Changes
* Huge API renaming. We now are prefixed by Ipuz and not IPuz.
* Enable GObject Introspection support. Many API cleanups to make it
  compatible with best practices:
    * Box all structs and register them with GType. Pass them in by
    reference. API calls no longer accepts a straight IpuzCellCoord
    anywhere, but require a pointer
    * Make const structs in API calls more consistent
    * Register all enums as GTypes
    * Annotate APIs with lifecycle annotations
    * Change foreach function names to be consistent with each other
* Implemented gi-docgen. API documentation is 80% done.
* IpuzAcrostic now has `fix()` functions and keeps the source/quote
  strings around as properties
* Rust fixes:
    * Move to SharedBoxed to add ref counting. This works better with
      GObject introspection. It means we can refcount
      eg. IpuzEnumerations without making a copy all the time. It also
      means that we can make mutators for some structs.
    * IpuzCellCoordArray and IpuzGuesses are ported to rust
* Abstract away CellCoord behavior and make it a standalone boxed type
* New puzzle kinds: Nonograms and color Nonograms. These are not part
  of the Ipuz spec, and are a libipuz extension
* New base classes and interfaces: IpuzGrid and IpuzClues. This
  removes most of the code that's not crossword specific from
  IpuzCrossword. This means that Acrostic can inherit from IpuzGrid in
  the future without being a crossword. Nonograms are also not
  crosswords.
    * IpuzGrid is a grid of IpuzCells. It provides a way to access
      them, and virtualizes common operations that subclasses can use
      to provide behavior hints.
    * IpuzClues is an interface that exports crossword clues.
* New libipuz.org landing page

## Special thanks for this release

* Federico for substantial Rust support and cargo fixes
* Tanmay for acrostic work
* Pranjal for Rust porting work
* Philip for API design and testing of IpuzNonogram
* Emmanuele for help and advice with gi-docgen and gobject introspection
* Luciana for the artwork

===============
Version 0.4.6.3
===============

Release date: May 21, 2024

Build fixes

## Changes
* change rust code to work with the cargo dependency
  pre-downloaded. This will let it be built in a flatpak with network
  access off

===============
Version 0.4.6.2
===============

Release date: May 18, 2024

Paper bag release.

## Changes
* Only ref the enumeration when it's !NULL

===============
Version 0.4.6.1
===============

Release date: May 18, 2024

Bug fix release to fix two crashers

## Changes
* Fix an unhandled enumeration that would crash the library
* Ref enumerations when copying clue

=============
Version 0.4.6
=============

Release date: May 12, 2024

This release adds IpuzAcrostic, and marks the beginning of
reimplementation in Rust.

## Changes
* Started the migration to rust
* Initial Acrostic work
* Better support for manipulating styles

## Special thanks for this release

* Tanmay for IpuzAcrostic
* Federico for initial rust support with IpuzCharset
* Pranjal for porting enumerations to rust
* Shruti for making CellArray in rust

=============
Version 0.4.5
=============

Release date: December 27, 2023

This release adds IpuzCharset and IpuzPuzzleInfo. The former is useful
for indicating (and measuring) the characters in a puzzle. The latter
includes information and statistics about a puzzle.

This version has also had its license changed to LGPL-2.1/MIT.

## Changes
* Moved Charset from GNOME Crosswords to IpuzCharset
* Added IpuzPuzzleInfo

## Special thanks for this release

* Davide for docs fixes and Italian insight
* Federico for substantial CI fixes
* Tanmay for charset fixes and tests
* All prior contributors for permitting the license cleanup

=============
Version 0.4.4
=============

Release date: August 30, 2023

Paper bag release to match double-bumped version in crosswords.

=============
Version 0.4.3
=============

Release date: August 28, 2023

First release with an editable API. This lets us handle a wide-variety
of mutations to the puzzle. This version also adds acrostics, as well
as supports IpuzPuzzle::equal().

## Changes
* Add an editable API to IpuzCrossword:
    * ipuz_crossword_fix_numbering()
    * ipuz_crossword_fix_symmetry()
    * ipuz_crossword_fix_enumeration()
    * ipuz_crossword_fix_clues()
    * ipuz_crossword_fix_all()
* Add acrostic puzzle type
* Support clue_sets with the same direction but different labels
* New ipuz_puzzle_equal () function, and associated tests
* devel-docs available at libipuz.org. A copy of the ipuz spec is
  available as well.

## Special thanks for this release:

* Tanmay for acrostic support
* Pranjal for ipuz_puzzle_equal() support
* Federico, for massive advice on implementing edit support, and for
  help keeping our CI tests running.

=============
Version 0.4.2
=============

Release date: March 29, 2023

## Changes
* Add an Barred type crossword to make that style explicit
* build kind string into puzzles so that we can save different puzzle
  types.
* New function: ipuz_puzzle_kind_to/from_gtype()

=============
Version 0.4.1
=============

## Changes
* Update api_version to match release

=============
Version 0.4.0
=============

## Changes

* Big API refactor
    * Make Puzzle kinds their own subtype. Including Arrowword,
      Cryptic, and Filippine.
    * Move ClueId into libipuz from Crosswords as
      IpuzClueId. IpuzCrossword now has by_id variations for it's clue
      operations, where appropriate.
    * Add a concept of ClueSets to Crosswords, so that we no longer
      hardcode puzzles to Across/Down. We support puzzles with other
      column types (such as Clue/Hidden, for alphabetical crosswords
* Update copyright information
* Support Kind as per the spec. The old code wasn't sophisticated
  enough to handle what we would otherwise see.
* Initial devel website


=============
Version 0.3.0
=============

## Changes
* Make html handling spec-compliant, and work with the subset GMarkup
  can handle
* Change enumerations handling from a regex to a state machine to
  handle more edge cases. Add testing
* Make spec compliance more explicit
