Title: Puzzle Gallery
Slug: puzzle-gallery

A visual gallery of all supported puzzle types.

## Acrostic

[<picture>
  <source srcset="acrostic-dark.png" media="(prefers-color-scheme: dark)">
  <img src="acrostic.png" alt="acrostic">
</picture>](class.Acrostic.html)

**Acrostic** — _A quote-based word puzzle_

Acrostics consists of two parts: a grid containing a quote, and a list
of clues. Each cell in the grid has a label and corresponds to one of
the cells in a clue. The puzzle can be solved by a combination of
guessing words in the quote and solving the clues. The first letter of
each answer will typically spell out the author of the clue.

## Arrowword

[<picture>
  <source srcset="arrowword-dark.png" media="(prefers-color-scheme: dark)">
  <img src="arrowword.png" alt="arrowword">
</picture>](class.Arrowword.html)

**Arrowword** — _A crossword-like puzzle with clues in blocks_

Arrowwords eschew numbers in favor of arrows extending from the blocks
to indicate the direction of the answer. Traditionally, arrowwords are
asymmetric and have relatively short clues. They are more common in
Europe.

This example is in Dutch.

## Barred

[<picture>
  <source srcset="barred-dark.png" media="(prefers-color-scheme: dark)">
  <img src="barred.png" alt="barred">
</picture>](class.Barred.html)

**Barred Crosswords** — _A crossword-like puzzle with lines between
words instead of blocks_

Barred puzzles are a variant of crossword, with lines instead of
blocks. Their clues tend to be cryptic in nature and often exhibit
symmetry.

## Crossword

[<picture> <source srcset="crossword-dark.png"
  media="(prefers-color-scheme: dark)"> <img src="crossword.png"
  alt="crossword"> </picture>](class.Crossword.html)

**Crossword** — _A gridded puzzle with blocks to indicate answers_

These are also known as **Traditional Crosswords**, **Standard
Crosswords**, or **American Crosswords**. These have clues that are
numbered by row and column. They frequently exhibit symmetry, and have
relatively simple clues.

This example also demonstrates hidden answers found within circles.

See also: [struct@Ipuz.Clue]

## Cryptic

[<picture>
  <source srcset="cryptic-dark.png" media="(prefers-color-scheme: dark)">
  <img src="cryptic.png" alt="cryptic">
</picture>](class.Cryptic.html)

**Cryptic Crossword** — _A crossword variant featuring cryptic clues_

These are also known as **British Crosswords**. Cryptic crosswords
traditionally have sparser grids with fewer — but longer —
answers. Cryptic clues use wordplay to give additional information
when solving clues.

Similar to [class@Ipuz.Crossword]s, Cryptics are frequently symmetric
and use numbered clues.

## Filippine

[<picture>
  <source srcset="filippine-dark.png" media="(prefers-color-scheme: dark)">
  <img src="filippine.png" alt="filippine">
</picture>](class.Filippine.html)

**Filippine** — _A puzzle consisting only of horizontal clues with an acrostic down the middle_

These puzzles are related to [class@Ipuz.Acrostic] puzzles. They
feature a bonus answer down the middle of the puzzle. They are
assymetric and only use across clues. Filippine puzzles are
historically found in the Netherlands.

This example is in Dutch.

## Nonogram

[<picture>
  <source srcset="nonogram-dark.png" media="(prefers-color-scheme: dark)">
  <img src="nonogram.png" alt="nonogram">
</picture>](class.Nonogram.html)

**Nonogram** — A picture-based logic puzzle.

Nonograms are visual-only puzzles and don't involve words. Players
uses the number clues on the edge of the puzzle to reveal an image.

See also: [struct@Ipuz.NonogramClue]

## Nonogram Color

[<picture>
  <source srcset="nonogram-color-dark.png" media="(prefers-color-scheme: dark)">
  <img src="nonogram-color.png" alt="nonogram-color">
</picture>](class.NonogramColor.html)

**Nonogram Color** — _A color variant of a nonogram puzzle_

Like nonograms, players uses the number hints on the edge of the
puzzle to reveal an image. However, the numbers also indicate the
color to be used, leading to a richer image.
